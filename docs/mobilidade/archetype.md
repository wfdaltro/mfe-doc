
## Guia Básico para construção de aplicativos com o Mobile First Platform 8 

### Construção e uso de adapters (server-side) 

1. Todo resourceAdapter deve estender a classe ResourceBusinessAdapter, encapsulada no arquivo ResourceBusinessAdapter.jar) e responsável pela implementação da chamada segura ao API Manager, propagação do token SISET associado ao usuário logado, propagação do API_key associado ao aplicativo– bem como dos logs básicos de infra a serem coletados (aplicativo, usuário, device ID, api chamada e retorno da requisição) e demais ações básicas a serem executadas pelo adapter. O resource adapter gerado a partir do archetype caixa.businessadapterarchetype (http://fontes.des.caixa/arqmobilidade/caixa.businessadapterarchetype) e a instancia já disponível no repositório (http://fontes.des.caixa/arqmobilidade/caixa.businessadapter)  já comtemplam a implementação acima especificada.

2. O resource adapter deve obedecer ao seguinte padrão de nomeclatura: "nomeDoAplicativo_nomeDoRecurso_<vesrsão>_ra", onde "nomeDoAplicativo" faz referência ao nome do aplicativo que fará uso do adapter, "nomeDoRecurso" faz referência ao recurso sendo acessado, versão faz referência a versão no padrão de versionamento semântico (MAjor.MInor.PAth - versioa) e "_ra" é a abreviação para "resource adapter". Exemplo: Appcartoes_cartaodecredito_v3_2_0_ra, Appbankingbeta_contacorrente_v1_0_0_ra, etc..

3. Os recursos disponibilizados pelo resource adapter devem obedecer ao padrão de nomenclatura do API Manager já adotado pela CAIXA publicado no ppds.caixa (API RESTful – Orientações Gerais – v1.1)

4. Todo resource adapter deve fazer requisições somente ao API Manager. Não são permitidas requisições diretas ao barramento, ou ao provedor do serviço, seja de baixa ou alta plataforma.

5. Não é permitida a implementação de orquestrações/transformações nos resource adapters.

6. Todo processo de login e provisionamento de dispositivos deve ser feito exclusivamente por meio do SecurityAdapterCaixa. Cada runtime do Mobile First possuirá uma única instancia instalada deste adapter. Entretanto instancias deste adapter podem ser replicadas e vinculadas a aplicativos específicos registrados no Mobile First server. Desta forma é possível customizar as propriedades deste adapter para atender características específicas de cada aplicativo. Exemplo (nível do aplicativo e chave pública para validação do token SISET entre outras). A vinculação de uma instancia especifica do SecurityAdapterCaixa a um aplicativo registrado no Mobile First server é feita por meio da console administrativa do Mobile First, conforme espeficado na documentação do produto. Ref: Item Mobile First Operation Console – Aplication em https://mobilefirstplatform.ibmcloud.com/tutorials/en/foundation/8.0/authentication-and-security/creating-a-security-check/

7. Todo recurso(url) exposto por meio de um resource adapter deve estar protegido exclusivamente por um dos seguintes escopos: "acesso_logado" ou "app_autentico". Para recurso que acessam serviços do API manager protegidos por token SISET, o escopo a ser utilizado é o "acesso_logado". Para recursos acessiveis somente por aplicativos autênticos o escopo a ser utilizado é o "app_autentico".

      7.1 - O escopo “acesso_logado” deve estar obrigatoriamente vinculado somente ao security check “SecurityAdapterCaixa” para TODOS OS APLICATIVOS em que for usado. Esta configuração é feita na console do MFP. 
 
      7.2 - O escopo “app_autentico” deve estar obrigatoriamente vinculado somente ao security check “appAuthenticity” para TODOS OS APLICATIVOS em que for usado. Esta configuração é feita na console do MFP; 
 
      7.3 - Cada recurso de adapter deve estar protegido (minimamente com app_autentico) e declarar individualmente cada escopo exigido para seu acesso (no código fonte do adapter), conforme o exemplo abaixo:

      ```java
      // ESCOPOS SEPARADOS POR ESPAÇO – Exemplo de recurso protegido com os security check SecurityAdapterCaixa e appAuthenticity; 
      @OAuthSecurity(scope = "acesso_logado app_autentico")  

      // Exemplo de recurso protegido somente com appAuthenticity; 
      @OAuthSecurity(scope = "app_autentico")    
      ```

       7.4 - A anotation @OAuthSecurity(enabled = "false") não pode estar presente na definição do método a ser exposto como um recurso em um adapter. 

8. Verificar se a versão indicada na tag &lt;description&gt; presente no arquivo de configuração adapter.xml  do projeto do adapter esta seguindo o versionamento deste artefato no repositório de executáveis (Nexus).  

9. Verificar o uso de qualquer escopo diferente de acesso_logado e app_autentico setado na annotation @OAuthSecurity. Exemplo @OAuthSecurity(escope = “meu_escopo”). O escopo acesso_logado deve estar vinculado ao securityCheck SecurityAdapterCaixa. O escopo app_autentico deve estar vinculado ao securtiyCheck AppAuthenticity.  

### Construção de aplicativos (client-side) 
1. O login do usuário no aplicativo deve ser feita exclusivamente pelo SISET – tela de login padrão. Não são permitidas a implementação de telas de login especificas no aplicativo. Utilizar a biblioteca keycloak.js para aplicativos híbridos ou AppAuth para alicativos nativos. Chamadas diretas ao serviço de obtenção de tokens do SISET (https://login.caixa.gov.br/auth/realms/demo/protocol/openid-connect/token) não são permitidas.

2. Toda chamada de serviços disponibilizados pela CAIXA deve ser feita por meio da SDK disponibilizada pelo IBM Mobile First. Ref: https://mobilefirstplatform.ibmcloud.com/tutorials/en/foundation/8.0/application-development/#applications

3. Somente será permitido o uso do plugin/bliblioteca CAIXA para geração de coordenadas georeferenciadas vinculadas a eventos cliente-side (BIBLIOTECAS EM DESENVOLVIMENTO).

4. Projetos que utilizem qualquer tecnologia de desenvolvimento diferente das citadas abaixo, devem ser submetidos para análise da SUART:

    - Framework de front-end ionic para aplicativos híbridos (versão suportada pela versão vigente do Mobile First);
    - Java para aplicativos nativos android  (versão suportada pela versão vigente do Mobile First); 
    - Swift para aplicativos nativos iOS (versão suportada pela versão vigente do Mobile First); 


## Archetype de Adapter de Negócio Genérico para Mobile First 8 Genérico 

Cdigo de exemplo para um projeto de adapter de negcio na plataforma Mobile First 8 integrado  soluo de SSO Corporativa Red Hat SSO Keycloak e  soluo de gesto de APIs: CAAPIM - CA API Management. 
#### Para baixar o projeto: 
```sh
 git clone http://fontes.des.caixa/arqmobilidade/caixa.businessadapter 
```

#### Para instalar o archetype em seu repositrio local: 
```sh
cd caixa.businessadapterarchetype 
mvn install 
```

#### Criando novo projeto a partir do archetype: 
```sh
mvn archetype:generate -DarchetypeCatalog=local 
```

ou 

```sh
mvn archetype:generate         
  -DarchetypeGroupId=br.gov.caixa.mobilidade.adapters     
  -DarchetypeArtifactId=GenericBusinessAdapter-archetype     
  -DarchetypeVersion=1.2.0     
  -DgroupId=new.project.id     
  -DartifactId=sample     
  -Dversion=1.0-SNAPSHOT     
  -Dpackage=com.company.project 
``` 

#### Para gerar build: 
```sh
mfpdev adapter build 

mvn adapter:build
```
#### Para fazer deploy no ambiente:

```sh
mfpdev adapter deploy {server_definition} Dvidas: gearq01@caixa.gov.br ou cedesbr050@caixa.gov.br
```

## Histórico da Revisão

 * **Versão 1**
    * Data: 31/10/2017
    * Descrição: Publicação
    * Autor: GEARQ01

 
