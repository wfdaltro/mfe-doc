# Mobilidade

O objetivo deste documento é definir a arquitetura referência de
mobilidade para a CAIXA, bem como padrões e tecnologias a serem
utilizados. O documento também deve servir como um guia para direcionar
a tomada de decisões sobre quando utilizar cada arquétipo arquitetural e
avaliar a viabilidade de novos APPs conforme as estratégias de negócios
e premissas estabelecidas pela área responsável - SUART.

## Arquitetura de Mobilidade

![Diagrama da arquitetura de referência de mobilidade](../img/mobilidade/arquitetura_referencia_mobile.png)

É importante entender que no universo da mobilidade, a estrutura de
front-end (cliente) é em geral independente da estrutura do back-end
(servidor). Vale ressaltar que genericamente, um APP mobile, nada mais é
que uma “casca” que tem por finalidade consumir serviços que estão
expostos na rede e de uma maneira geral estes serviços são consumidos
através de requisições HTTP onde os dados repassados são uma
representação de estado, e o padrão utilizado para acesso é o REST,
conhecido como “Transferência de Estado Representacional – REST”. Os
dados trafegados são representados no formato JSON (_Java Script Object
Notation_).

Analisando pura e simplesmente o atendimento à regra de negócio (consumo
do serviço), podemos fazer um paralelo de um APP com uma aplicação SPA
(_Single Page Application_) em que todo o conteúdo visual encontra-se do
lado cliente (navegador) e os conteúdos informacionais são consumidos
através de requisições HTTP. Diferenciam-se essas abordagens pelo fato
de o navegador ser passivo (o usuário precisa informar o endereço para
ativar a navegação) e o APP ter uma característica ativa (ao abrir o APP
uma requisição inicial pode ser disparada automaticamente para o
servidor em busca dos primeiros dados).

A comunicação entre o cliente e o servidor, por padrão, é sempre feita
com um protocolo sem estado (_stateless_), que considera cada requisição
como uma transação independente e que não está relacionada a qualquer
requisição anterior.

Sendo assim, pode-se entender que mais importante do que definir uma
estrutura de construção/funcionamento do APP, é identificar como e onde
os serviços que serão consumidos estão disponibilizados. Nos itens a
seguir detalharemos cada uma das camadas dispostas na representação
arquitetural.

### Frontend

![Camada de frontend](../img/mobilidade/camada_front_end.png)

Esta camada é basicamente constituída pelo aplicativo em si, o qual traz
embarcado em seu código componentes de software (SDKs e/ou APIs),
responsáveis por prover sua interação com os hardwares integrados ao
dispositivo (câmera, GPS, acelerômetro, NFC etc.), recursos específicos
da plataforma (armazenamento seguro de _tokens_), bem como acesso a
funcionalidades providas de forma segura pelo _middleware_ de mobilidade
adotado pela CAIXA.

No diagrama abaixo estão representados alguns dos conceitos e
componentes chaves que constituem a camada de front-end de mobilidade,
abstraindo-se neste momento questões relacionadas à tecnologia de
construção do aplicativo (hibrido, nativo, nativo-hibrido, _webview_
etc.).

![Elementos conceituais da camada frontend](../img/mobilidade/camada_front_end_elementos_conceituais.png)

#### Tipos de Aplicativo - App

Há atualmente uma grande variedade de estratégias e tecnologias
disponíveis para o desenvolvimento de aplicativos móveis. As opções vão
desde o desenvolvimento em plataformas nativas viabilizadas por SDKs
específicas de cada sistema operacional, até o desenvolvimento
totalmente desvinculado de qualquer sistema operacional e que emprega o
uso de tecnologias de desenvolvimento _web_ (HTML, CSS e Javascript) no
seu core funcional e _frameworks_ específicos para garantir a
portabilidade entre várias plataformas.

Para fins de convenção, consideraremos aplicativos móveis na CAIXA
somente as aplicações instaláveis no dispositivo por meio de pacotes
específicos a cada plataforma de mobilidade (APK para Android e IPA para
iOS). Sendo assim, aplicações desenvolvidas totalmente com tecnologias
web, acessíveis somente por meio do browser não são consideradas
aplicativos móveis e sim _webapps_ ou simplesmente sites responsivos.

Segue abaixo uma linha evolutiva das tecnologias de construção de
aplicativos, que contempla desde aplicações web responsivas até
aplicativos nativos.

![Diagrama dos tipos de aplicações](../img/mobilidade/tipos_de_aplicacoes.png)

##### Aplicação Web Responsiva

É um site que tem o layout preparado para se adaptar ao formato de
diversos dispositivos dentre os quais estão o tablet e/ou smartphone,
porém não é um App. Quando o visitante visualiza o site no computador,
o formato se expande e aproveita toda a tela. No dispositivo, as
informações mudam de posição, mas preserva-se o conteúdo.

###### Vantagens

 * Renderização em qualquer dispositivo com acesso ao server;
 * Possibilidade de classificar em mecanismos de busca (SEO);
 * Reduz o esforço de atualização nos clientes pois o conteúdo passa a
   estar no server;
 * A atualização é centralizada;
 * Não é necessário instalação a partir de lojas virtuais;

###### Desvantagens

 * O desenvolvimento do design é mais custoso, pois é necessário adaptar
   a diversos tamanhos de dispositivos;
 * Acesso restrito a alguns recursos de hardware integrados aos
   dispositivos;
 * Interface gráfica dos componentes divergente dos padrões da
   plataforma;
 * Experiência do usuário pode ficar comprometida em alguns casos;
 * Tempo de implementação aumenta de acordo com a quantidade e
   complexidade dos elementos em tela;
 * Necessita de alta experiência de web design;
 * Desempenho dependente da largura de banda do usuário;

###### Recomendações

Recomenda-se a utilização de um cliente web Responsivo se:

 * Não se necessita acessar muitos recursos de hardware integrado aos
   dispositivos;
 * A solução envolve a construção de portal web para exibir as mesmas
   informações através de um navegador na intranet/internet;
 * Utilizado para aplicações informativas, ou quando a solução já
   corresponde originalmente a uma aplicação web;
    * Ex: https://sifec.caixa.gov.br/

##### Aplicação Web App

É uma aplicação web acessada via _browser_, projetada exclusivamente
para dispositivos móveis. Reconhece automaticamente o acesso via
_smartphone_ e se adapta às características do dispositivo. Utilizada
tipicamente quando pretende-se apenas apresentar conteúdos, não consome
espaço de armazenamento do dispositivo, armazena conteúdo _off-line_ e
não está disponível nas lojas.

###### Vantagens

 * Não consome espaço de armazenamento do dispositivo;
 * Executável satisfatoriamente em várias plataformas;
 * Menor custo de desenvolvimento;
 * Roda em vários navegadores.

###### Desvantagens

 * Não é um aplicativo de fato;
 * Não permite controle/acesso a todos os recursos de hardware
   integrados ao dispositivo;
 * A usabilidade não é a melhor possível;
 * Necessário conhecer a URL;

###### Recomendações

 * É recomendada para usuário eventual, uma vez que não precisa de
   instalação.
 * Solução meramente informacional, com utilização restrita a alguns
   recursos de hardware integrados ao dispositivo
    * Ex: [Facebook](https://m.facebook.com) e
      [Twitter](https://mobile.twitter.com)

![Estrutura de web app](../img/mobilidade/estrutura_de_web_app.png)

##### Aplicação Híbrida

Um aplicativo híbrido e/ou _Cross-platform_ é aquele que possui parte
considerável (ou todo ele) feita em tecnologia não específica da
plataforma. O Apache Cordova é um _framework_ de desenvolvimento móvel
de código aberto. Ele permite que você use tecnologias _web_ padrão -
HTML5, CSS3 e _Javascript_ para desenvolvimento multiplataforma. Os
aplicativos são executados em _wrappers_ segmentados para cada
plataforma e dependem de APIs compatíveis com padrões para acessar as
capacidades de cada dispositivo, como sensores, dados, status da rede
etc.

###### Vantagens

 * Desenvolvimento único com compilação para cada plataforma;
 * Tendência a gastar um tempo menor com desenvolvimento;
 * Desenvolvimento baseado em estrutura de web e compilação em um APP
   para diversas plataformas com acesso a recursos do dispositivo;
 * Aproveitamento do conhecimento da equipe em desenvolvimento web;
 * Grande diversidade de plug-ins disponíveis, incluindo acesso a vários
   recursos dos dispositivos;
 * Ferramental open-source para desenvolvimento;

###### Desvantagens

 * Performance um pouco mais lenta por existir uma camada de tradução;
 * Compatibilidade depende do motor de renderização do navegador da
   plataforma;
 * Maior esforço de testes para validar a estrutura em todos os
   dispositivos e versões de sistemas operacionais desejados;
 * Acesso a recursos do dispositivo dependem de _plug-ins_;
 * Interface gráfica padronizada pelo CSS, geralmente divergindo dos
   modelos específicos das plataformas;

###### Recomendações

Recomenda-se a construção de um cliente híbrido se:

 * A solução não necessitar de características muito específicas da
   plataforma e/ou recursos de hardware específicos;
 * Todos os recursos de hardware necessários possuírem _plug-ins_
   disponíveis e estiverem previamente validados através de POCs;
 * A equipe de desenvolvimento possuir domínio nas linguagens de
   programação para _WEB_ (HTML5, CSS e _Javascript/Typescript_) de
   forma a reduzir a curva de aprendizado e tempo de desenvolvimento;
 * A padronização de interface gráfica entre as plataformas não for um
   problema;
    * Ex: Netflix

![Diagrama da arquitetura conceitual do Cordova.](../img/mobilidade/arquitetura_conceitual_do_cordova.png)

![Ilustração da estrutura de aplicativo híbrido.](../img/mobilidade/estrutura_de_aplicativo_hibrido.png)

##### Aplicação Nativo Híbrido

App Nativo Híbrido tem parte do código personalizado, compilada
nativamente e executado diretamente no sistema operacional do
dispositivo móvel. Adicionalmente tem parte do código personalizado
executado dentro do controle nativo do _Webview_ da plataforma móvel ou
como um aplicativo habilitado para _Javascript_ que funciona no sistema
ou no “navegador seguro”. A parte nativa pode acessar recursos nativos
diretamente através das APIs, enquanto o container _Web_ executado
dentro do controle _Webview_ usa API de ponte _Javascript_.

###### Vantagens

 * Suporte a todas as funcionalidades oferecidas pela plataforma, tanto
   em software quanto em hardware;
 * Compatibilidade com componentes de interface gráfica;
 * Melhor desempenho da solução;
 * Compartilhamento de partes da aplicação entre projetos de plataformas
   distintas;

###### Desvantagens

 * Necessidade de maior experiência na gestão de configuração e mudança
   do projeto;
 * Projetos independentes para as plataformas que se desejar ofertar a
   solução;
 * Necessidade de conhecimento aprofundado nas linguagens de cada uma
   das plataformas;
 * Riscos de evoluir funcionalidades de forma desordenada nas
   plataformas;
 * Custo de desenvolvimento para construção e inclusão de novas
   funcionalidades cresce proporcionalmente ao número de plataformas
   suportadas, além do desenvolvimento com o uso do _framework_;

###### Recomendações

Recomenda-se a construção de um cliente nativo híbrido se:

 * Soluções com ciclos longos de evolução (pois a manutenção e
   sincronização dos códigos de cada parte considerada é mais complexa).
 * A solução necessite primordialmente de alto desempenho e com
   restrições orçamentárias de projeto;
 * Deseje que algumas partes essenciais da interface visual possa ser
   adequada a cada plataforma de forma mais fluída;
 * Necessidade de utilização de recursos não acessíveis na plataforma
   híbrida. Ex: Integração com o assistente pessoal.
 * Projetos que estão passando por uma transição da plataforma
   tecnológica utilizada no seu desenvolvimento (Ex: projeto
   originalmente híbrido, sendo migrado para Nativo e vice-versa)
    * Ex: Agência Virtual CAIXA

![Ilustração da estrutura de aplicativo nativo híbrido.](../img/mobilidade/estrutura_de_app_nativo_hibrido.png)

##### Aplicação Nativa

É um aplicativo desenvolvido para plataforma específica, utilizando todo
o potencial de funcionalidades nativas para a qual ele foi feito. É
geralmente programado em Java ou Kotlin para Android e Swift para iOS.

###### Vantagens

 * Suporte total a todas as funcionalidades oferecidas pela plataforma,
   tanto em software quanto em hardware;
 * Total compatibilidade com componentes de interface gráfica;
 * Melhor desempenho da solução;
 * Maior facilidade para evoluir funcionalidades específicas da
   plataforma;

###### Desvantagens

 * Necessidade de maior experiência na Gestão de Configurações e
   Mudança;
 * Projetos independentes para todas as plataformas que se desejar
   ofertar a solução;
 * Necessidade de conhecimento aprofundado nas linguagens de cada uma
   das plataformas;
 * Riscos de evoluir funcionalidades de forma desordenada nas
   plataformas;
 * Custo de desenvolvimento para construção e inclusão de novas
   funcionalidades multiplicado por tantas quantas plataformas forem
   suportadas;
 * Ferramental específico de cada plataforma para desenvolvimento;

###### Recomendações

Recomenda-se a construção de um cliente nativo se:

 * A solução necessite primordialmente de alto desempenho;
 * Deseje garantir que seja possível utilizar os recursos mais novos dos
   dispositivos, logo que lançados (independente da característica
   existir em apenas uma das plataformas)
 * Deseje que a interface visual possa ser adequada a cada plataforma de
   forma mais fluída;
 * A aplicação utilize recursos muito específicos de cada plataforma.
   Ex: Integração com o assistente pessoal, etc.
    * Ex: WhatsApp, Instagram, Waze e Uber.

![Ilustração da estrutura de aplicativo nativo.](../img/mobilidade/estrutura_de_app_nativo.png)

##### Tabela comparativa de arquitetura de _app_

Perfil Arquitetural             | Nativo                           | Nativo Híbrido                                                                       | Híbrido
------------------------------- | -------------------------------- | ------------------------------------------------------------------------------------ | --------------------------
**Linguagem**                   | Nativo (Swift e Java)            | _Javascript_ e Nativo (Swift e Java)                                                 | _Javascript_, HTML e CSS
**Acesso a API Nativa**         | API de Acesso Direto             | API JS _Bridge_ e API de Acesso Direto                                               | API JS _Bridge_
**Portabilidade**               | Não portável                     | *_Código JS e Híbrido:_* Portável<br>*_Código Nativo:_* Não                          | Portável
**Complexidade**                | Média                            | Alta                                                                                 | Média
**Interface de Usuário Nativa** | Sim                              | *_Código JS e Híbrido:_* Não<br>*_Código Nativo:_* Sim                               | Não
**Características Nativas**     | Sim, todas acessíveis            | *_JS e Híbrido:_* Sim, várias acessíveis<br>*_Código Nativo:_* Sim, todas acessíveis | *_JS e Híbrido:_* Sim, várias acessíveis
**Opções de Deploy**            | Publicável nas lojas ou estático | Publicável nas lojas ou estático                                                     | Publicável nas lojas ou estático

Conforme apresentado na figura acima o _framework_ e as ferramentas a
serem utilizadas na construção de um App podem variar de acordo com a
arquitetura adotada.

*Na Arquitetura de App Nativo* utiliza-se SDKs nativos para a
construção, teste, compilação e publicação. Adicionalmente pode-se
utilizar uma ferramenta _cross-compiler_ (Xamarin) para se construir o
código nativo, porém o SDK ainda será necessário para testar e publicar
a aplicação.

*Na Arquitetura de App Nativo Híbrido* deve-se utilizar a ferramenta
_cross-compiler_ ou SDKs nativos para construir as funcionalidades
nativas do app, porém será necessária a utilização de _framework_
híbrido baseado em HTML5, CSS e _Javascript_ para geração do código
_Web_ que rodará no _Webview_ gerenciado pelo aplicativo.

*Na Arquitetura de App Híbrido* recomenda-se a utilização de _framework_
híbrido para gerar o _wrap_ do App, como Cordova, visto este prover a
_Javascript_ API _bridge_ para acesso aos recursos nativos por parte do
código _Web_ (HTML, CSS e Javascript) gerado. Nessa arquitetura não se
lança mão dos SDKs nativos para fase de construção, sendo que não se
fará uso de APIs nativas. Essa arquitetura é utilizada quando se pensa
em um modelo de negócio que necessita de desenvolvimento otimizado do
aplicativo, pretende-se entregar ao cliente boa experiência de usuário
(UX), os Apps demandam celeridade na construção, além de rodarem em
várias plataformas. Outra relevância é a padronização da arquitetura,
ferramentas e _frameworks_ de desenvolvimento dos Apps, dessa forma
pretende-se consolidar as melhores práticas na criação destes.

#### Critérios para Definições de Desenvolvimento

Existe uma grande discussão a respeito de qual estratégia é a mais
adequada para se desenvolver a camada de _front-end_ de um aplicativo
móvel. Ter ciência das características, vantagens e desvantagens
inerentes de cada uma delas é essencial na escolha da opção mais
adequada a um determinado contexto.

O processo decisório envolverá basicamente o confronto destas
características com o seu nível de relevância para cada um dos seguintes
fatores, essenciais na concepção do aplicativo a ser construído:

 * Interface Visual;
 * Abordagem Orientada a Plataforma;
 * Desempenho;
 * Recursos utilizados do dispositivo;
 * Características dos usuários alvo;
 * Complexidade das funcionalidades;
 * Tempo disponível para desenvolvimento;
 * Custo para construção/manutenção da solução.

Para auxiliar nesta definição foi desenvolvida a matriz de decisão que
orienta os idealizadores/desenvolvedores do aplicativo na identificação
e avaliação de cada um dos requisitos, conceitos e estratégias de uso
acima discutidos, buscando promover desta forma uma convergência para a
opção mais indicada a ser aplicada em um dado contexto.

Estão mapeados resultados que vão desde a não viabilidade de construção
do aplicativo, até as arquiteturas de implementação mais indicadas:
Nativo, nativo-hibrido ou hibrido.

É importante ressaltar que a matriz proposta não tem o objetivo de
contemplar todas as possibilidades a serem avaliadas no momento de se
decidir qual a estratégia a ser adotada no desenvolvimento do
aplicativo. Trata-se de um guia simplificado e de alto nível, construído
com as principais questões a serem respondidas durante esta análise. Não
há a intenção de excluir quaisquer outros questionamentos, haja vista a
complexidade e as inúmeras situações que podem estar envolvidas na
concepção e construção de aplicativos móveis.

Esta matriz deve servir de ponto de partida para análise a ser feita, ou
até mesmo a base de evolução para uma análise mais detalhada, caso
necessário.

![Matriz de Decisão (fonte Consultoria BCG).](../img/mobilidade/matriz_decisao_consultoria_bcg.png)

Seguem abaixo um case ilustrativo da aplicação da matriz na definição da
melhor estratégia de desenvolvimento de um aplicativo da CAIXA:

!!!Example

    Cartão Virtual do Cidadão: Proposta inicial de um aplicativo
    especifico para consulta de saldos e pagamentos de benefícios. Após
    análise do critério de reuso, optou-se pela unificação das
    funcionalidades da consulta de saldos (já implementada em um outro
    aplicativo) e a funcionalidade de pagamento em um único
    aplicativo.

Outra abordagem a ser adotada para construção de aplicativos é a
evolutiva. O aplicativo em um contexto inicial de testes e maturação é
desenvolvido de forma hibrida, e à medida que a sua interatividade
evolui, novas funcionalidades dos dispositivos são agregadas à
experiência do usuário. Tecnologias de desenvolvimento nativas passam a
compor uma parte cada vez mais significante do aplicativo até que
finalmente o aplicativo atinja o ápice de importância estratégia e
exigência de uma experiência de uso específica da plataforma que
justifique os custos e complexidades inerentes a um desenvolvimento
puramente nativo.

#### Plataformas e Ferramentas de Desenvolvimento

Cada estratégia de desenvolvimento discutida anteriormente é suportada
por um conjunto de ferramentas (IDEs, SDKs, APIs, linguagens, etc) as
quais terão os seus principais conceitos aqui relacionados.

O objetivo não é delimitar o uso de uma tecnologia ou outra dentro
CAIXA, mas sim estabelecer uma linha de entendimento acerca dos
princípios de uso vinculados a cada uma delas, mesmo porque todas seguem
evoluindo e novas surgem a todo momento, as vezes como uma extensão das
existentes, como é o caso do _framework_ Apache Cordova, base para o
Ionic.

##### Android

O sistema operacional criado em 2005 compartilha o mesmo _kernel_ do
Linux e é atualmente propriedade da Open Handset Alliance, um consórcio
de empresas criado pelo Google e com participação das principais
fabricantes de celulares do mundo como a Motorola, Samsung e LG.

O desenvolvimento nesta plataforma prevê a instalação do JDK (Java
Development Kit) e o Android SDK (Software Development Kit), disponíveis
publicamente aos desenvolvedores desde setembro de 2008. O SDK contém
todas as bibliotecas e APIs para manipular os aplicativos nativos da
plataforma e os recursos de hardware do dispositivo, como GPS,
acelerômetros, tela sensível ao toque, rede de dados, etc.

Como ambiente de desenvolvimento não há uma limitação. Pode se utilizar
o Android Studio, Eclipse, Netbeans ou IntelliJ entre outras.

Cabe por fim salientar que a máquina virtual Java (JVM) que roda nos
dispositivos Android é uma versão da tradicional que roda em _desktops_
e contém o seu próprio conjunto de instruções. Não há portanto
compatibilidade entre os binários de ambas plataformas e mesmo através
de recompilação, nem todas as bibliotecas Java tradicionais funcionam no
Android. A plataforma suporta o desenvolvimento com as linguagens Java,
Kotlin, C, C++, HTML+CSS+JSS entre outras, com diferente níveis de
desempenho, compatibilidade e conjunto de recursos.

Mais informações sobre a plataforma podem ser obtidas em
[https://developer.android.com/index.html](https://developer.android.com/index.html).

##### iOS

O sistema operacional da Apple lançado em 2007 (inicialmente com o nome
de iPhone OS) é baseado no OS X, sistema operacional da Apple para
_desktops_ e notebooks. Para instalar o SDK e programar para o iOS é
necessário um computador que rode o OS X.

Um ponto a se destacar aqui é que ao contrário de outros sistemas
_mobile_ como o Android, detalhes no iOS são mais padronizados,
existindo uma quantidade muito menor de dispositivos e resolução de
tela, o que simplifica muito a vida na hora de criar aplicativos.

A Apple disponibiliza gratuitamente todas as ferramentas e documentação
para desenvolver para iOS, incluindo simuladores iPhone e iPad, além da
sua IDE de desenvolvimento, o XCode.

As linguagens utilizadas para o desenvolvimento nesta plataforma são o
Objective-C e o Swift. Mais informações podem ser obtidas em:
[https://developer.apple.com/documentation/](https://developer.apple.com/documentation/).

##### Tecnologia Cross-platform

###### Cordova

O projeto Apache Cordova é o principal representante desta estratégia de
desenvolvimento. Trata-se da versão _open source_ mantida pela Apache
Foundation. A versão original que serviu de base para o Cordova é o
Phone Gap, de propriedade da Adobe.

A partir deste _framework_ vários outros foram implementados melhorando
ou otimizando algumas características/funcionalidades pouco exploradas
nele, tal como o Ionic (desenvolvido com o objetivo de agregar uma
melhor experiência com o usuário por meio do provimento de componentes
de interface customizados e com bastante similaridade de
aparência/comportamento das suas respectivas versões nativas), entre
outras.

O ponto forte do projeto Apache Cordova é intermediar o acesso a
funcionalidades nativas dos dispositivos, como: acelerômetro, câmera,
conexões, contatos, eventos diversos, arquivos, geolocalização, entre
outras. Para isso mantém um conjunto de _plug-ins_ chamados “core
_plug-ins_”. Esse _framework_ precisa de outra ferramenta ou APIs
especializadas na criação visual do aplicativo, que se responsabilize
pela interface com o usuário. Para auxiliar nesta tarefa são utilizados
vários outros _frameworks_ como por exemplo: Ionic, Sencha Touch, Dojo
_Mobile_, jQuery _Mobile_ entre outras.

Mais informações acerca dos _plug-ins_ Cordova assim como das
especificidades do framework podem ser encontradas em:
[https://cordova.apache.org/docs/en/latest/guide/overview/index.html](https://cordova.apache.org/docs/en/latest/guide/overview/index.html).

##### Padrão de geração de ID de dispositivo

Para uma gestão efetiva dos dispositivos dos usuários é mandatório a
geração de um código identificador único para cada dispositivo, com
regra de geração padronizada para toda a empresa.

Há algumas especificações de geração de identificadores disponíveis no
mercado e a grande maioria utiliza algoritmos baseados em componentes
físicos do dispositivo, IMEI entre outros dados.

Para os aplicativos da CAIXA, o padrão a ser utilizado na identificação
única de dispositivos é o implementado pelo IBM Mobile First, onde são
usados como insumos informações do dispositivo como MAC-ADDRESS, UUID de
boot, IMEI e APIs proprietárias da plataforma entre outros.

Conforme a documentação do fabricante, o identificador gerado é único
para todos os aplicativos de um mesmo publicador.

O identificador é gerado somente uma vez, durante a
autenticação/registro do aplicativo junto ao Mobile First Server e em
seguida persistido na keystore do aplicativo. Ele não é regerado pelo
aplicativo nas interações posteriores, salvo em ocasiões em que os dados
do aplicativo na keystore são apagados (deleção do cache do aplicativo
ou desinstalação completa do aplicativo por exemplo).

Entretanto o fabricante salienta que os parâmetros usados na geração do
identificador podem mudar a qualquer momento, por conta de mudanças
implementadas na plataforma, como por exemplo uma atualização do sistema
operacional do dispositivo.

O middleware de mobilidade também permite a utilização de um
identificador único de dispositivo gerado por uma ferramenta de
terceiros. O Mobile First disponibiliza APIs que permitem a integração
automática destes identificadores ao framework de segurança da
ferramenta e demais funcionalidades que dependam deste dado.

Entretanto, conforme orientação da GECMI, esta funcionalidade ainda será
usada até que a CAIXA adquira ou desenvolva uma solução para geração
segura de identificador único de dispositivos.

#### Criticidade de Segurança dos Aplicativos

Os Aplicativos deverão passar, anteriormente ao início do
desenvolvimento, por uma consultoria das áreas de segurança (GECMI e
GEIPF) para verificação dos níveis de segurança exigidos para cada
produto/funcionalidade.

De acordo com critérios de risco definidos por estas áreas, os
aplicativos/funcionalidades serão enquadrados em uma classificação
também definida pelas mesmas áreas.

As definições se diferem para o público de clientes CAIXA (externo) e
para os empregados (interno).

#### SDK CaixaMobilidade

Com o objetivo de padronizar a implementação de funcionalidades de uso
comum a vários aplicativos da CAIXA uma SDK _cliente-side_ foi
desenvolvida e disponibilizada no repositório de código fontes
corporativo:
[http://fontes.des.caixa/arqmobilidade/CaixaMobilidade](http://fontes.des.caixa/arqmobilidade/CaixaMobilidade).
A primeira versão de deste componente contempla as plataformas de
desenvolvimento iOS, Android e Ionic, e implementa a captura e envio de
informações de geolocalização vinculadas a eventos específicos para a
base elasticsearch do Mobile First.

Toda coleta de coordenadas geográficas vinculadas à eventos do usuário
no aplicativo devem portanto ser realizadas por meio deste componente.

Novas funcionalidades (coleta de eventos sem geolocalização entre
outros) serão contempladas em versões posteriores.

#### Diretrizes para o _Front-end_

As plataformas móveis abarcadas pelos projetos de mobilidade da CAIXA
são:

 * iOS;
 * Android.

As IDEs de desenvolvimento utilizadas pela CAIXA nos projetos de
mobilidade são:

 * XCode;
 * Android Studio;
 * Eclipse;
 * Netbeans.

As linguagens de programação utilizada para desenvolvimento nativo são:

 * Java (Android);
 * Kotlin (Android)
 * Swift(IOS).

Os FRAMEWORKS de desenvolvimento híbrido adotados são:

 * Cordova;
 * Ionic.

O padrão a ser utilizado na identificação única de dispositivos é o UUID
gerado pelo IBM Mobile First.

O fluxo do padrão _OAuth_ a ser usado para autenticação de usuários de
aplicativos móveis junto ao SISET (SSO) é o *Authorization Code*.

O adapter *cliente* a ser usado pelos aplicativos móveis nas interações
junto ao SISET para autenticação e autorização de usuários é o
*AppAuth*.

A estratégia de tombamento dos Ids de dispositivos atualmente
registrados no SIPER - gerados pela ferramenta da GAS - para os
identificadores gerados pelo middleware de mobilidade deverá ser
definida pela GECMI.

A definição da criticidade do aplicativo deve ser feita com a
consultoria das áreas de segurança (GECMI e GEIPF).

!!! Important

    Quaisquer outras tecnologias devem ser validadas pela SUART antes de
    serem utilizadas.

### Middleware

![Diagrama da arquitetura da camada middleware.](../img/mobilidade/middleware_da_arquitetura_de_referencia.png)

Em linhas gerais está camada detém as seguintes responsabilidades

 * Recepção segura das requisições oriundas do front end;
 * Execução de todo o processamento relacionado à segurança de acesso às
   funcionalidades disponibilizadas (tais como a identificação/validação
   do dispositivo, aplicativo, usuário);
 * Registros das requisições/interações do usuário com o _front-end_
   (_analytics_ e monitoring);
 * Direcionamento destas solicitações à camada de _back-end_,
   responsável pela sua execução do serviço de fato.

As ferramentas responsáveis por tais atividades e que compõem esta
camada na arquitetura de mobilidade da CAIXA estão detalhados na
sequência.

#### Proxy _Mobile_

O proxy _mobile_ corresponde à borda de contato da CAIXA com o
aplicativo na internet e tem por finalidade recepcionar a requisição
baseada em tunelamento SSL em porta padronizada do protocolo e
redirecionar este pacote para os componentes internos da arquitetura
(middleware de mobilidade ou API manager) através de IP interno e porta
específica exercendo a função de gateway dentro de uma DMZ.

Esses componentes internos são protegidos por firewall de forma a
garantir que todo a acesso realizado possa ser registrado e
identificado, tais como origem de rede, porta e serviço desejado.

Existem opções a cumprir esse papel como o IHS - IBM HTTP Server, Apache
HTTP Server ou NGINX. Nestes servidores são instalados os certificados
SSL para permitir a comunicação com os dispositivos.

A função de proxy _mobile_ pode ser, em alguns casos, assumida pelo API
Manager externo (onde APIs para consumo de desenvolvedores e parceiros
estão publicadas), sendo a requisição direcionada diretamente para ele
que protegerá o acesso aos recursos internos. Essa configuração é
possível na ferramenta, mas será avaliada pela SUART quando a
necessidade se apresentar.

Quando o APP utilizar o _Mobile_ First a função de proxy _mobile_ é
desempenhada pelo IHS - IBM HTTP Server, que redireciona a requisição
para o IBM _Mobile_ First acessar os serviços publicados exclusivamente
no API manager interno (onde APIs para consumo das aplicações internas
da CAIXA).

#### Servidor de Autenticação Corporativo

O Sistema Corporativo CAIXA para autenticação de usuários e autorização
de acesso a recursos é o SISET – Sistema de Segurança Tecnológica -
internamente denominado Login.caixa. Este sistema está baseado na
solução Red Hat SSO. Portanto registre-se, todas essas nomenclaturas
fazem referência à mesma solução, sendo esta gerida pela
GECMI - GN Monitoramento Integrado e Segurança TI.

O SISET é uma solução de mercado para single sign-on e federação de
identidades baseada em especificações de mercado (SAML 2.0, OpenID
Connect e OAuth 2.0) adquirida pela CAIXA para centralizar todo o
processo de autenticação de usuários na organização. Atualmente cada
linha de negócio da empresa praticamente possui a sua própria base
autenticadora, o que traz sérias dificuldades e limitações ao projeto,
desenvolvimento e disponibilização aos clientes de uma plataforma de
aplicativos CAIXA ágil e de fácil utilização. Em linhas gerais esta
ferramenta fornece serviços de autenticação, fornecimento de tokens,
sementes geradoras de senhas OTP e validação de senhas OTP entre
outras funcionalidades. Todas as funcionalidades implementadas pelo
SISET estão disponíveis para uso das aplicações por meio de serviços
REST.

O SISET é a solução base para viabilizar o acesso único dos usuários aos
aplicativos Caixa. Todos os aplicativos devem portanto implementar o
acesso via SISET, o qual já fornece tela de login padrão, além de
serviço de cadastramento, esqueci a senha, login social, reCAPTCHA,
proteção contra ataques de força bruta, logs de auditoria, entre outros.

Sistemas alternativos de provimento de autenticação não devem ser
utilizados.

##### RedHat Single Sign-On

###### Descrição

Red Hat Single Sign-On (RH-SSO) baseado no projeto Keycloak permite
proteger as aplicações por meio de _Web_ SSO utilizando-se padrões como:
SAML 2.0, OpenID Connect e OAuth 2.0. O servidor pode interagir SAML ou
OpenID Connect based identity provider (Idp), mediando acessos dos
usuários corporativos através de informações de credenciais do usuário e
de aplicação baseado em padrões de tokens de acesso. Objetiva modernizar
a arquitetura de provisionamento e autenticação, proporcionar a gestão
de acessos com múltiplos fatores de autenticação.

###### Características

 * Authentication Server
    * Trabalha autonomamente como Security Assertion Markup Language
      (SAML) ou OpenID Connect baseado em provedor de identidade;
 * Identity Brokering
    * Integração com provedores de identificação de terceiros, incluindo
      redes sociais como fonte identidade, ou seja, login social;
 * User Federation
    * Certificação via servidor LDAP e Microsoft Active Directory com
      fonte de informações de usuários;
 * REST APIs and Administration GUI
    * Especificação de federação de usuário, mapeamento de perfil de
      acesso, e aplicação clientes com administração via interface de
      usuário e REST APIs;

A figura abaixo representa o processo básico de realização de login do
usuário junto ao SISET para aplicações/aplicativos da CAIXA, bem como os
atores e ações envolvidas:

![Processo do login no SISET (Gestão de Acesso Corporativo).](../img/mobilidade/processo_de_login_no_siset.png)

#### Vinculação de Dispositivo

Este processo é de fundamental importância para o atendimento de
exigências de segurança tais como rastreabilidade e monitoramento de
dispositivos móveis e usuários a eles vinculados. Ele tem fundamental
importância também na garantia de acesso seguro de usuários a
funcionalidades críticas providas pelos aplicativos da CAIXA.

O SISET é o componente da arquitetura responsável pela gestão de todo o
processo de vinculação dos dispositivos aos seus respectivos usuários,
bem como a persistência destas vinculações e dos níveis de criticidades
obtidos pelos dispositivos.

O middleware de mobilidade (IBM Mobile First) armazena de forma
automática algumas informações básicas acerca dos aplicativos que em
algum momento se conectaram e os respectivos usuários, mas não faz
gestão nenhuma de tais informações. Tal responsabilidade é exclusiva do
SISET.

A credenciamento de um determinado dispositivo a um nível de criticidade
especifico pressupõe a execução de um processo de autenticação por meio
de um canal alternativo que varia conforme a criticidade do
serviço/aplicativo em questão.

Os níveis de criticidades, bem como os respectivos canais alternativos
que auxiliarão no processo de vinculação do dispositivo e o processo em
si são definidos pelas áreas de segurança (GECMI/GEIPF).

O fluxo de vinculação de dispositivos é um passo que ocorre após a
autenticação do usuário sendo padrão para todos os aplicativos da CAIXA.
A responsabilidade pela implementação do fluxo de vinculação é única e
exclusivamente do SISET. Este passo é autocontido e executado de forma
independente do aplicativo que a requisita, o que torna desta forma
desnecessário para o desenvolvedor do aplicativo conhecer os detalhes do
fluxo implementado, bastando para o mesmo apenas requisitar a sua
ativação.

Os níveis de criticidade para aplicativos/funcionalidades atualmente
definidos pelas áreas de segurança (GECMI/ GEIPF) são:

Nível                              | Definição
---------------------------------- | -----------------------------------
00 – Acesso a informações públicas | Este nível não requer autenticação do usuário nem registro do dispositivo em uso;
10 – Baixa criticidade             | Login do usuário é obrigatório. O dispositivo deve ser vinculado automaticamente mediante o login do usuário;
20 – Média criticidade             | Login do usuário é obrigatório. O dispositivo deve ser vinculado mediante a confirmação de informações do cliente por meio de um canal digital seguro previamente estabelecido com o cliente: SMS ou push notification;
30 – Alta criticidade              | Login do usuário é obrigatório. O dispositivo é vinculado mediante a confirmação de informações do cliente por meio de um segundo canal físico com a CAIXA: ATM ou Agência;

Durante o processo de autenticação, o aplicativo deve informar ao SISET
o deviceID do dispositivo, o nível de acesso necessário e o nome do app,
informações essas obtidas junto ao MobileFirst; além do parâmetro
`origem`, que deve ser igual a `mf` e o IP atribuído ao dispositivo.
Esses parâmetros devem ser passados ao SISET por meio do método
`setAdditionalParameters` da biblioteca de integração com o SISET
(appAuth), conforme exemplo abaixo:

```java
Map<String, String> parm = new HashMap<> ();
parm.put("deviceid","123");
parm.put("nivel","20");
parm.put("app","xxxxxx");
parm.put("origem","mf");
authRequestBuilder.setAdditionalParameters(parm);
```

Um dispositivo ativado para um determinado nível de segurança está
automaticamente ativado para os níveis inferiores, de forma que a
instalação de um aplicativo ou consumo de uma funcionalidade vinculada a
um nível de segurança inferior não demandará a realização do processo de
vinculação do dispositivo novamente.


#### Middleware de Mobilidade

O _middleware_ de mobilidade é o componente responsável por recepcionar
as requisições dos aplicativos, realizar alguns processamentos
específicos desta camada (em sua maior parte relacionadas à segurança) e
direcioná-las para o API Manager. Os processamentos nela executados
envolvem funções relacionadas a:

 * Garantia de autenticidade do aplicativo,
 * Proteção de acesso aos recursos corporativos,
 * Coleta de informações de _analytics_ e _monitoring_ relacionadas ao
   consumo destes recursos,
 * Gestão do envio de push notification;
 * Gestão de dispositivos, entre outras;

Para abstrair as chamadas a tais serviços a partir dos aplicativos a ele
conectados, o _middleware_ disponibiliza SDKs e APIs especificas para as
principais plataformas de desenvolvimento do mercado (iOS, Android e
_frameworks_ híbridos).

Toda a comunicação do aplicativo com o _middleware_ de mobilidade deve
ser baseada no padrão REST, seguindo as recomendações de uso dos verbos
HTTP para identificação da ação. E as informações trafegadas devem estar
codificadas em JSON de forma a melhorar a performance evitando operações
custosas de “parser” para analisar o corpo da requisição/retorno da
informação.

O _middleware_ atualmente utilizado na CAIXA é o IBM _Mobile_ First e
suas características estão detalhadas abaixo.

##### IBM _Mobile_ First

Plataforma da IBM que provê um _framework_ para o desenvolvimento,
integração, e gestão da segurança de aplicativos móveis sem a introdução
de uma linguagem de programação ou modelos proprietários de
desenvolvimento.

Aplicativos podem ser desenvolvidos nesta plataforma com o uso de HTML5,
CSS3 e _Javascript_ (Cordova) bem como com código nativo das plataformas
suportadas: Java e Kotlin para android e swift para iOS.

A ferramenta fornece SDK que incluem bibliotecas e chamadas acessíveis
pelas tecnologias de desenvolvimento suportadas para a criação de
aplicativos, bem como componentes programáveis no lado server em
_Javascript_ ou Java (adapters) usados para conectar com segurança os
aplicativos clientes ao _back-end_ corporativo.

A seguir temos uma breve descrição das principais funcionalidades
embarcadas na ferramenta:

 * Implementação de OAuth: Possibilita que os adapters disponíveis
   realizem autenticação utilizando padrão de mercado OAuth e com isso
   permitir a confiança em outros mecanismos de autenticação como SSO ou
   Login Social (Google+, Facebook, Twitter, etc);

 * Autenticidade de APP: Confirmação de “checksum” da APP no handshake
   garantindo que sejam atendidas apenas as requisições oriundas de
   clientes autênticos registrados no _Mobile_ First server;

 * Live Update: Torna possível colocar pacotes de conteúdo diferentes e
   selecionar alguns dispositivos previamente para que possam acessar as
   URLs (serviços disponibilizados pelos adapters) de novas
   funcionalidades e ir ampliando o conjunto de dispositivos/clientes
   até que a versão seja liberada totalmente ao público;

 * Gerenciamento de versões: A plataforma permite que uma mesma
   aplicação possa atender a duas ou mais versões diferentes de forma
   simultânea, dessa forma os clientes podem levar um tempo maior para
   atualizar suas aplicações junto a loja de aplicativos antes de que
   uma versão especifica deixe de ser atendida. Também é possível
   desabilitar remotamente essas versões mantendo o código ainda na
   plataforma;

 * Gerenciamento de Dispositivos: A plataforma gera uma identificação do
   dispositivo que gerou requisições de forma que o mesmo possa ser
   “desabilitado” para uma solução especifica ou para toda a plataforma
   de forma a que futuras requisições deste dispositivo não sejam mais
   atendidas;

 * Direct Update: Para aplicações não nativas, é possível distribuir os
   pacotes de recursos estáticos (HTML, JS, CSS, permitindo que a
   aplicação seja atualizada sem a necessidade de encaminhar a
   atualização para a loja atendendo a correções de forma imediata. Seu
   uso não é recomendado pela SUART, salvo em situações específicas
   passíveis de análise da SUART;

 * Estrutura de Analytics: Estrutura de logs baseado em ElasticSearch e
   customizações de relatórios baseado nos eventos de logs, captura de
   logs por dispositivos e informações de crash entre outros.
   Possibilidade de exportação dos dados armazenados (JSON) por meio de
   APIs nativas ou integração com a ferramenta Kibana para criação de
   painéis customizados;

#### API Manager

Todas os recursos disponibilizados para consumo dos aplicativos _mobile_
Caixa devem necessariamente ser acessados por meio de web APIs, sendo
estas necessariamente publicadas no padrão REST e no API Manager, que é
responsável por toda governança das APIs, tais como políticas de
utilização, acesso e _rate limit_.

#### _Analytics_ e Monitoring

Estas funcionalidades visam a captura, armazenamento e visão/exportação
de dados essencialmente relacionados à infraestrutura de mobilidade
(eventos específicos do canal, identificador do dispositivo, sistema
operacional, modelo do aparelho, versão do aplicativo entre outras).

A natureza dos dados coletados de forma nativa pela ferramenta, bem como
a categorização utilizada podem ser verificadas no manual do produto:
[https://mobilefirstplatform.ibmcloud.com/tutorials/en/foundation/8.0/analytics/analytics-api/](https://mobilefirstplatform.ibmcloud.com/tutorials/en/foundation/8.0/analytics/analytics-api/).

Os dados coletados de forma nativa pelo middleware de mobilidade fazem
referência portanto em uma trilha de auditoria ao canal utilizado no
consumo de determinado serviço corporativo. Dados vinculados ao negócio
(operação executada, usuário conectado, valores etc.) não devem utilizar
esta infraestrutura para captura e armazenamento. Tais dados devem ser
coletados pela própria aplicação e armazenados em repositório a serem
definidos pela SUART. A gravação neste repositório deve ser feita pelo
componente que efetiva ou orquestra a transação. Esta estrutura visa
garantir a disponibilidade de serviços reutilizáveis e desacoplados do
canal utilizado para o seu consumo.

Nesta camada da arquitetura recomenda-se que sejam coletadas e
armazenadas informações que permitam análises pontuais e momentâneas de
forma mais ágil e direta. Entretanto por conta de limitações de
infraestrutura de armazenamento do middleware, diariamente os dados
gerados devem ser repassados a uma estrutura de BigData para que possam
viabilizar análises comportamentais históricas utilizadas na geração e
oferta de novos negócios aos clientes.

O IBM Mobile First disponibiliza APIs para extração destes dados, cuja
especificação pode ser verificada no manual do produto:
[https://mobilefirstplatform.ibmcloud.com/tutorials/en/foundation/8.0/analytics/analytics-rest-api/](https://mobilefirstplatform.ibmcloud.com/tutorials/en/foundation/8.0/analytics/analytics-rest-api/).

As informações sobre o comportamento do cliente no dispositivo podem ser
obtidas através do “track” de eventos realizados por parte do usuário
como por exemplo, ao clicar num determinado item de menu, o aplicativo
dispara uma requisição _Web_ assíncrona (feita em batch pelo IBM
_Mobile_ First) que consome um serviço disponibilizado pelo Analytics
server do middleware para capturar a informação de que o item de menu X
foi selecionado. Neste envio de informação, pode ser identificado por
exemplo:

 * Plataforma do usuário;
 * Identificador do dispositivo utilizado;
 * Geolocalização (Caso habilitado e autorizado);
 * Evento ocorrido;
 * IP de acesso;
 * Data/Hora.

#### Push notification

O IBM Mobile First implementa nativamente toda a infraestrutura
necessária para a entrega do push notification às plataformas (Apple e
Google) responsáveis por sua distribuição aos aplicativos.

A ferramenta provê meios para que push notifications possam ser enviados
com base nos seguintes critérios: por usuário, por plataforma, por
dispositivo ou por tópicos de interesse assinados pelos usuários.

Os serviços para envio de push notification em ambiente de
desenvolvimento estão publicados no API Manager e sua documentação está
disponível em [http://portalapi.des.caixa](http://portalapi.des.caixa).

As APIs a serem utilizadas no registro, subscrição e tratamento de push
notifications recebidos pelo aplicativo estão documentadas em
[https://mobilefirstplatform.ibmcloud.com/tutorials/en/foundation/8.0/notifications/](https://mobilefirstplatform.ibmcloud.com/tutorials/en/foundation/8.0/notifications/).

#### Diretrizes para Middleware

O Sistema Corporativo Caixa para autenticação e autorização de acesso é
o SISET.

Sistemas alternativos de provimento de autenticação não devem ser
utilizados.

A vinculação de dispositivo se inicia no Mobile First, mas é
completamente gerenciada pelo SISET.

O fluxo do padrão _OAuth_ a ser usado para autenticação de usuários de
aplicativos móveis junto ao SISET (SSO) é o *Authorization Code*.

O *deviceID* informado pelo aplicativo bem como o *IP do dispositivo*
móvel serão automaticamente incluídos no access token gerado pelo SISET
e caso algum processo de backend necessite destas informações deve
obtê-las por meio do access token fornecido;

Nenhuma API implementada na camada API de mobilidade (hospedada no
WASND) deve ser acessada diretamente pelo middleware de mobilidade.

Todos os aplicativos, sejam eles nativos, nativos híbridos ou híbridos,
devem consumir APIs disponibilizadas no API Manager, no qual devem ser
configuradas as políticas de acesso e consumo.

As APIs disponibilizadas no _resource owner_ denominado API de
mobilidade devem migrar gradativamente para os respectivos backends,
barramento de serviços e disponibilizadas no API Manager. Não estão
autorizadas a construção de novas APIs nesta camada.

O API Manager deve autorizar as requisições com validação da chave de
acesso do aplicativo e, quando necessário, o _access token_ do usuário.

Quanto ao uso do middleware da IBM, deve ser utilizada a sua versão mais
recente - atualmente V8 - sendo vedado o desenvolvimento em versões
anteriores.

Deve ser habilitada a função autenticidade de aplicativos, que garante o
processamento somente de requisições oriundas de aplicativos autênticos.

Deve-se evitar atualizações do aplicativos via recursos _Direct Update_,
em detrimento do versionamento do APP, salvo em casos de necessidade
extrema.

Dentro do contexto de mobilidade, é importante salientar que os serviços
publicados pelo API manager devem ser síncronos, _stateless_ e
implementados sobre o protocolo REST.

Archetypes de adapters básicos de negócios (componente do lado server do
Mobile First) foram desenvolvidos e disponibilizados no repositório
corporativo para reutilização das equipes de desenvolvimento
([http://fontes.des.caixa/arqmobilidade/caixa.businessadapterarchetype](http://fontes.des.caixa/arqmobilidade/caixa.businessadapterarchetype));

Orientações acerca da construção/configuração dos adapters de negócios,
bem como a correta vinculação de escopos de segurança (acesso_logado,
app_autentico) aos seus recursos protegidos estão disponíveis em
[http://fontes.des.caixa/arqmobilidade/ORIENTACAO/arqmobilidade/blob/master/README.md](http://fontes.des.caixa/arqmobilidade/ORIENTACAO/arqmobilidade/blob/master/README.md);

Os adapters de segurança devem ser únicos para todas as aplicações. O
Mobile First permite que propriedades específicas de cada aplicativo
(chave pública SISET, nível, etc.) possam ser individualizadas via
console administrativo, durante a implantação do adapter.

Processos de integração e entrega contínua serão os responsáveis por
garantir os padrões de codificação de adapters e aplicativos definidos
pela SUART/CEDES e publicados no repositório corporativo
([http://fontes.des.caixa/arqmobilidade/arqmobilidade/wikis/home](http://fontes.des.caixa/arqmobilidade/arqmobilidade/wikis/home)).

Salientamos que é de suma importância que as equipes de desenvolvimento
já adequem os seus projetos às regras especificadas no repositório
corporativo pois já se encontra disponível para implantação pela GEOTI a
primeira versão do plugin SONAR que automatiza a validação as regras 1,
4 e 7 referente à construção e uso de adapters (_server side_). As
demais regras serão implementadas em versões posteriores do plugin.

Tal orientação visa evitar atrasos e retrabalhos nos projetos quando
submetidos ao processo de integração contínua, por conta da não
observância destas regras.

### Back-end

![Diagrama da arquitetura de referência para back-end.](../img/mobilidade/back_end_arquitetura_de_referencia.png)

Esta camada é a responsável por recepcionar as requisições oriundas do _front-end_ - já tratadas e validadas pela camada de middleware - e efetivamente prover a execução dos serviços requisitados (regras de negócios). Ela é composta basicamente por serviços disponibilizados na plataforma alta por meio do broker (IBM Integration BUS) e rotinas CICS bem como por serviços disponibilizados na plataforma baixa exclusivamente por meio do protocolo HTTP (REST).O consumo de serviços através de filas, corresponde ao pior cenário possível para o mundo da mobilidade pois todo fluxo ocorre em requisições síncronas, e quando chega nesta etapa o consumo necessariamente se torna assíncrono mantendo uma sessão aberta durante a espera e eventualmente causando um represamento de serviços com possibilidade inclusive de degradação e queda do ambiente.


#### Diretrizes para BACKEND

Para as APIs que suportam o canal mobilidade, o padrão de publicação a
ser utilizado é REST.

Não devem ser construídas soluções de integração com sistemas da
plataforma alta baseadas em filas MQ ou do componente JCICS Direct. As
APIs existentes nesse modelo, relacionadas com a camada de mobilidade,
devem ser migradas para o barramento ou WebService CICS.

Aplicativos que ainda fazem uso da camada de API mobilidade para fins de
transformação, orquestração, enriquecimento ou persistência de dados
devem ser ajustados pois tal camada não está prevista na arquitetura de
referência. Tais responsabilidades são estão distribuídas da seguinte
forma:

 * Orquestrações, transformações, redução e enriquecimento de dados
   devem ser implementadas na camada de _backend_ corporativo;

 * Persistência de dados de negócios devem ser feitos nos sistemas
   corporativos afins;

 * Persistência de dados de infraestrutura do canal mobilidade para
   compor trilha de auditoria são nativamente coletados e armazenados
   pelo Mobile First. Esta ferramenta expõe APIs para exportação dos
   dados coletados.

 * Persistência de dados de negócio para compor trilha de auditoria
   devem ser armazenados nos sistemas corporativos ou em repositório
   definido pela SUART, com escrita comandada pelo componente que
   efetiva ou orquestra a transação;

Os sistemas corporativos são os responsáveis pela exposição dos serviços
relacionados ao negócio sob sua gestão, sem a utilização de outros
sistemas como intermediários.

A criação de um novo sistema como back-end do aplicativo deve ser
previamente aprovada pela SUART.

As requisições oriundas do middleware de mobilidade não devem ser
direcionadas diretamente ao _back-end_ da aplicação. Todos os serviços
devem estar publicados exclusivamente no API Manager.

## Decomposição Arquitetural

Seguem abaixo os cenários arquiteturais previstos para a construção de
aplicativos móveis na CAIXA. Em cada cenário, basicamente é levado em
consideração a utilização ou não da ferramenta de middleware de
mobilidade, suas consequências, vantagens, desvantagens e critérios de
uso.

### Cenário 1: Sem middleware de mobilidade

![Diagrama da arquitetura sem middleware Mobile First.](../img/mobilidade/arquitetura_sem_mobile_first.png)

1. Aplicativo realiza login do usuário junto ao autenticador
   corporativo, o SISET. Ao final deste processo um access token e um
   refresh token (opcional) é gerado pelo RedHat SSO e encaminhado ao
   aplicativo para armazenamento.

2. Uma vez logado junto ao RedHat SSO e de posse dos tokens, o
   aplicativo está apto a realizar chamadas de serviços (REST) junto ao
   API Manager. O access token deve ser enviado na solicitação.

3. O API manager recepciona a requisição e valida o token repassado. Em
   caso de sucesso a requisição é encaminhada para a camada de backend.

4. O serviço é enfim executado pelo _backend_ e a resposta retornada ao
   aplicativo.

#### Vantagens

 * Independência de plataformas de middleware;

 * Número menor de camadas envolvidas na arquitetura – arquitetura com
   uma latência menor de processamento;

 * Maior fluidez na construção dos aplicativos tendo em vista a ausência
   do uso de SDKs especificas da plataforma de middleware.

 * Aderência às diretrizes definidas pela GECMI em relação à
   disponibilização de uma tela única padronizada de _logon_ ao usuário,
   fornecida pelo RedHat SSO;

#### Desvantagens

 * Aplicativos com um menor grau de segurança embarcado;

 * Ausência de funcionalidades providas pele middleware, tais como push
   notification, analytics e garantia de autenticidade do aplicativo,
   entre outras.

#### Indicação de USO

 * Aplicativos com requisitos de segurança mais flexíveis (ausência de
   comprovação de autenticidade para os aplicativos);

 * Aplicativos com baixa criticidade negocial para a CAIXA e baixo
   retorno financeiro. Entende-se por baixa criticidade negocial,
   aplicações meramente informativas, sem valor estratégico comercial
   para a CAIXA, sem necessidade de uso de notificações, monitoração
   e/ou analytics;

### Cenário 2: Com middleware de mobilidade – IBM _Mobile_ First

![Diagrama da arquitetura com middleware Mobile First.](../img/mobilidade/arquitetura_com_ibm_mobile_first.png)

1. Autenticidade do Aplicativo é realizada junto ao _Mobile First_ em
   sua primeira requisição. Esta funcionalidade é nativa do _Mobile
   First_ e sua implementação é transparente para o desenvolvedor do
   aplicativo;

2. Uma vez autenticado, o aplicativo realiza então o _login_ junto ao
   SISET, para obtenção do _access token_ e _refresh token_ (opcional).
   Neste momento a tela de logon padrão CAIXA gerada pelo SISET é
   exibida para o usuário no aplicativo;

3. RedHat SSO recebe a credencial do usuário informada na tela de logon
   e a valida. Em caso de sucesso o SISET gera o _access token_ e o
   _refresh token_ (opcional) e os encaminha para o aplicativo. Uma vez
   logado junto ao SISET e de posse dos tokens, o aplicativo está apto a
   realizar chamadas de serviços (REST) disponibilizadas pelos
   _adapters_ do _Mobile_ First. Em um processo intermediário de
   autenticação do usuário junto ao _Mobile_ First, o _access token_
   gerado pelo SISET é encaminhado ao _Mobile_ First que por sua vez
   faz sua validação, gera tokens próprios do seu contexto de segurança,
   encapsula dentro do seu token o _token_ gerado pelo SISET e o envia
   de volta para o aplicativo. Ao enviar uma requisição junto ao adapter
   funcional do _Mobile_ First, o aplicativo encaminha o token do
   _Mobile_ First já vinculado ao contexto de segurança do aplicativo
   criado pelo middleware anteriormente. Tal contexto será necessário às
   funcionalidades de gestão de dispositivo e _analytics_
   disponibilizadas pelo middleware. O adapter funcional, retira o token
   do SISET de dentro do token _Mobile_ First e o repassa ao API Manager
   juntamente com a requisição ao serviço de negócio;

4. O API manager recepciona a requisição e valida a token repassado.

5. O serviço é enfim executado pelo backend e a resposta retornada ao
   aplicativo.


#### Vantagens

 * Aderência às diretrizes definidas pela GECMI em relação à
   disponibilização de uma tela única padronizada de logon ao usuário,
   fornecida pelo RedHat SSO;

 * Abstrai do desenvolvedor o armazenamento e uso do token _Mobile_
   First (armazenado no dispositivo) no momento das chamadas aos
   _adapters_ de negócios providos pela SDK da ferramenta;

 * Integração do _Mobile_ First com o SISET feito de forma programática
   e com o uso das flexibilidades disponibilizadas pela ferramenta;

 * Funções de _push notification_ e gestão de dispositivos
   disponibilizados de forma nativa na ferramenta por meio de SDK/APIs;

 * Envio de dados de _analytics_/monitoring gerenciada pela SDK
   embarcada no aplicativo. Pode ser efetivada em background e
   postergada para ocasiões de alta disponibilidade de rede.

#### Desvantagens

 * Moderado acoplamento à plataforma de _middleware_ por conta do uso de
   suas funcionalidades (autenticidade de aplicativos, push, gestão de
   dispositivo) para atender aos requisitos do aplicativo;

 * Esforço adicional de aprendizado necessário às equipes de
   desenvolvimento para dominar a utilização dos recursos providos pela
   plataforma de middleware embarcados nos aplicativos por meio das SDKs

#### Indicação de USO

 * Aplicativos com altos requisitos de segurança;

 * Aplicativos com alta criticidade negocial e alta rentabilidade
   financeira.

## Verificação de aderência à Arquitetura de Referência

Abaixo estão listados as regras que devem ser verificadas a fim de
determinar que uma aplicação está aderente a essa Arquitetura de
Referência. As regras estão agrupadas por camada, back-end (servidor) e
front-end (cliente).


### Regras de validação para a camada do servidor

1. Todo _resource adapter_ deve estender a classe `ResourceBusinessAdapter` (encapsulada no arquivo `ResourceBusinessAdapter.jar`), responsável pela implementação da chamada segura ao API Manager – propagação do _token_ SISET associado ao usuário logado e a propagação da `API_key` associada ao aplicativo – bem como dos logs básicos de infra a serem coletados (aplicativo, usuário, device ID, api chamada e retorno da requisição) e demais ações básicas a serem executadas pelo adapter.

    1. Verificar importação e uso da classe `ResourceBusinessAdapter`.

    2. Verificar que a chamada aos serviços do API Manager deve ser feita por meio dos métodos herdados desta classe (`executeApi()` com os parâmetros necessários).

2. O _resource adapter_ deve obedecer ao padrão de nomenclatura `<Nome do aplicativo>_<nome do recurso>_<versão>_ra`, no qual:

    1. `<Nome do aplicativo>` faz referência ao nome do aplicativo que fará uso do _adapter_.

3. `<Nome do recurso>` faz referência ao recurso sendo acessado.

4. `<Versão>` faz referencia a versão do _adapter_, seguindo o padrão https://semver.org/lang/pt-BR/[Versionamento Semântico].

5. `ra` é abreviação para _resource adapter_.

    1. Exemplos:

        1. `Appcartoes_cartaodecredito_v3_2_0_ra`

        2. `Appbankingbeta_contacorrente_v1_0_0_ra`

6. Os recursos disponibilizados pelo _resource adapter_ devem obedecer ao padrão de nomenclatura do API Manager já adotado pela CAIXA, conforme https://portalapi.caixa/.

7. Todo _resource adapter_ deve fazer requisições somente ao API Manager. Não são permitidas requisições diretas ao barramento, ou ao provedor do serviço, seja de baixa ou alta plataforma.

    1. Verificar se há alguma requisição `HTTP` no _adapter_ cuja URL seja diferente da URL base do API Manager.

8. Não é permitida a implementação de orquestrações/transformações nos _resource adapters_.

    1. Verificar se há obrigatoriamente em cada recurso disponibilizado pelo _adapter_ uma única requisição `HTTP` ao API Manager.

9. Todo processo de _login_ e provisionamento de dispositivos deve ser feito exclusivamente por meio do `SecurityAdapterCaixa`.

    1. Verificar no arquivo `JSON` de configuração da aplicação, localizada na pasta  `[project-folder]\mobilefirst` do projeto do aplicativo, se o elemento de escopo `acesso_basico` está associado ao _security check_ `SecurityAdapterCaixa`.

10. Todo recurso (URL) exposto por meio de um _resource adapter_ deve estar protegido exclusivamente por um dos seguintes escopos: `acesso_logado` ou `app_autentico`

    1. Para recurso que acessam serviços do API Manager protegidos por _token_ SISET, o escopo a ser utilizado é o `acesso_logado`.

    2. O escopo `acesso_logado` deve estar obrigatoriamente associado ao _security check_ `SecurityAdapterCaixa`.

    3. O escopo `app_autentico` deve estar associado obrigatoriamente ao _security check_ `appAuthenticity`.

    4. A _annotation_ `@OAuthSecurity(enabled = "false")` não pode estar presente na definição do método a ser exposto como um recurso.

        1. _Annotations_ permitidas: `@OAuthSecurity(scope = "acesso_logado")`  e `@OAuthSecurity(scope = "app_autentico")`.

    5. Verificar no arquivo `JSON` de configuração da aplicação, localizada na pasta  `[project-folder]\mobilefirst` do projeto do aplicativo, se o elemento de escopo `acesso_logado` está associado ao _security check_ `SecurityAdapterCaixa` e se o escopo `app_autentico` está associado ao _security check_ `appAuthenticity`.

11. Verificar se a versão indicada na _tag_ `<description>` presente no arquivo de configuração `adapter.xml` está sincronizado com a última versão deste artefato no repositório de executáveis, ou seja, uma unidade à frente da última versão registrada.

12. Verificar o uso de qualquer escopo diferente de `acesso_logado` e `app_autentico` _setado_ na _annotation_ `@OAuthSecurity`. Exemplo: `@OAuthSecurity(escope = “meu_escopo”)`.

    1. O escopo `acesso_logado` deve estar vinculado ao _security check_ `SecurityAdapterCaixa`.

    2. O escopo `app_autentico` deve estar vinculado ao _security check_ `AppAuthenticity`.


### Regras de validação para a camada do cliente

1. O _login_ do usuário no aplicativo deve ser feito exclusivamente pelo SISET – tela de _login_ padrão.

    1. Não são permitidas a implementação de telas de _login_ especificas no aplicativo.

    2. Verificar a presença e uso da biblioteca `keycloak.js` para aplicativos híbridos ou `AppAuth` para aplicativos nativos.

        1. Chamadas diretas ao serviço de obtenção de _tokens_ do SISET não são permitidas. O serviço de obtenção de _tokens_ do SISET está disponível na URL https://login.caixa.gov.br/auth/realms/demo/protocol/openid-connect/token.

2. Toda chamada de serviços disponibilizados pela CAIXA deve ser feita por meio da SDK disponibilizada pelo IBM Mobile First.

    1. Verificar a versão da SDK utilizada é a suportada pela versão do MobileFirst.

    2. Verificar uso da REST API `WLResourceRequest` nas chamadas aos recursos tanto para aplicativos híbridos e nativos. Maiores informações no link https://mobilefirstplatform.ibmcloud.com/tutorials/en/foundation/8.0/application-development/#applications.

3. Somente será permitida o uso do plugin/biblioteca CAIXA _{nome do plugin/biblioteca para cada aplicativos híbridos e nativos}_ para geração de coordenadas georreferenciadas vinculadas a eventos client-side.

4. Projetos que utilizem qualquer tecnologia de desenvolvimento diferente das citadas abaixo devem ser submetidos para análise da SUART:

    1. _Framework_ de _front-end_ ionic para aplicativos híbridos (versão suportada pela versão vigente do Mobile First).

    2. Java ou Kotlin para aplicativos nativos Android (versão suportada pela versão vigente do Mobile First).

    3. Swift para aplicativos nativos iOS (versão suportada pela versão vigente do Mobile First).

## Glossário

Termo       | Definição
----------- | -----------------------------------------------------------
GCM         | Disciplina de Gerência de Configuração e Mudanças.
JSON        | Javascript Object Notation é uma formatação de troca de dados, amigável para leitura e escrita, além de ser fácil para conversão e geração.
POC         | Prova de Conceito – Do Inglês Prove Of Concept.
REST        | Representational State Transfer é a abstração da arquitetura WWW, que objetiva a definição de características fundamentais para construção de aplicações Web no contexto das melhores práticas.
SPA         | Single Page Application – Modelo de desenvolvimento onde todo o conteúdo visual está definido em uma página única e partes dessa página são ocultadas e exibidas de acordo com o conjunto de dados que se deseja exibir. Neste modelo, todo o recurso estático como os arquivos HTML, Javascript, CSS e imagens necessários são transferidos para o cliente no primeiro acesso e a partir daí, apenas dados são solicitados ao servidor.
_Stateless_ | Protocolo sem estado - É um protocolo de comunicação que considera cada requisição como uma transação independente que não está relacionada a qualquer requisição anterior, de forma que a comunicação consista de pares de requisição e resposta independentes.
_Wearable_  | É a palavra que resume o conceito das chamadas “tecnologias vestíveis”, que consistem em dispositivos tecnológicos que podem ser utilizados pelos usuários como peças do vestuário.
WLAPP       | Pacote denominado “Work Light Application” onde ficam armazenados os recursos estáticos como HTM, CSS e Javascript.
IAM         | Identity Access Management (Gerenciador de Identidade e Acesso)

## Histórico da Revisão

 * **Versão 1**
    * Data: 31/10/2017
    * Descrição: Publicação
    * Autor: GEARQ01

 * **Versão 2**
    * Data: 24/08/2018
    * Descrição:
        * _Item 2:_ Incluída a menção da SUARQ como área responsável pela
          definição da estratégia de negócios relacionadas à mobilidade
          na CAIXA;
        * _Item 3.1.3.3:_ Retirada das referências às ferramentas
          Xamarin e Appcelerator Titanium do tema tecnologias
          Cross-Platform;
        * _Item 3.1.3.4:_ Esclarecimento das características e
          limitações do identificador único de dispositivos fornecido
          pelo middleware de mobilidade, bem como uma breve descrição
          dos insumos necessários para a sua geração;
        * _Item 3.2.3:_ Incluída a menção ao SISET como sistema
          gerenciador do processo de vinculação de dispositivos e a
          existência de um fluxo padronizado para todos os aplicativos;
        * _Item 3.2.3:_ Incluído os níveis atuais de criticidade de
          segurança para aplicativos/funcionalidades;
        * _Item 3.2.6:_ Incluídas informações acerca da composição da
          trilha da auditoria com a coleta e armazenamento de dados em
          camadas específicas da arquitetura de referência;
        * _Item 3.2.7:_ Incluídas instruções sobre o envio de push
          notifications.
        * _Item 3.2.8:_ Incluídas as referências aos archetypes
          de adapters básicos no repositório corporativo;
        * _Item 3.2.8:_ Incluídas as instruções de uso de adapters de
          segurança;
        * _Item 3.2.8:_ Incluída a referência ao processo de integração
          e entrega contínua para mobilidade;
        * _Item 3.3:_ Retirada da menção aos sistemas auxiliares no
          ecossistema de mobilidade da CAIXA (SICPU e SISIT);
        * _Item 3.3.1:_ Diretrizes relacionadas a migração dos serviços
          atualmente implementados na camada de API Mobilidade (WASND);
        * _Item 4:_ Retirada do cenário arquitetural que contempla a
          solução CA MAG;
        * _Item 4.1.3:_ Esclarecimento do conceito "criticidade
          negocial"
        * _Item 3.2.7:_ Incluídas instruções sobre o envio de push
          notifications.
        * _Item 3.2.8:_ Incluídas as referências aos archetypes de
          adapters básicos no repositório corporativo;
        * _Item 3.2.8:_ Incluídas as instruções de uso de adapters de
          segurança;
        * _Item 3.2.8:_ Incluída a referência ao processo de integração
          e entrega contínua para mobilidade;
        * _Item 3.3:_ Retirada da menção aos sistemas auxiliares no
          ecossistema de mobilidade da CAIXA (SICPU e SISIT);
        * _Item 3.3.1:_ Diretrizes relacionadas a migração dos serviços
          atualmente implementados na camada de API Mobilidade (WASND);
        * _Item 4:_ Retirada do cenário arquitetural que contempla a
          solução CA MAG;
        * _Item 4.1.3:_ Esclarecimento do conceito "criticidade
          negocial"
    * Autor: GEARQ01

 * **Versão 3**
    * Data: 10/12/2018
    * Descrição:
        * _Item 3.1.5:_ Inclusão do SDK CaixaMobilidade;
        * _Item 3.1.6:_ Inclusão de informações acerca do fluxo padrão
          de autenticação a ser usado e a biblioteca de integração com o
          SISET;
        * _Item 3.2.3:_ Inclusão de informações acerca do processo de
          vinculação do dispositivo a ser encapsulado pelo SISET;
        * _Item 3.2.3:_ Inclusão de informações acerca dos dados a serem
          repassados pelo aplicativo ao SISET durante o processo de
          autenticação do usuário;
        * _Item 3.2.8:_ Inclusão de informações acerca do fluxo de
          autenticação padrão (_authorization code_) a ser usado pelos
          aplicativos móveis;
        * _Item 3.2.8:_ Inclusão de informações acerca das regras de
          construção de adapters e aplicativos a serem validadas de
          forma automatizada no processo de integração contínua;
    * Autor: GEARQ01

 * **Versão 4.0**
    * Data: 22/04/2019
    * Descrição:
        * _Item 3.2.8:_ Referência ao guia de construção de adapters e
          vinculação de escopo de segurança a recursos protegidos.
    * Autor: GEARQ02

 * **Versão 4.1**
    * Data: 23/07/2019
    * Descrição:
        * _Itens 3.1.4 , 3.1.6 e 3.2.3:_ Atualização da área responsável
          pela classificação de criticidade dos aplicativos.
    * Autor: GEARQ02

 * **Versão 4.1.1**
    * Data: 11/03/2020
    * Descrição: Inclusão do item "4. Verificação de aderência à
      Arquitetura de Referência".
    * Autor: GEARQ04

 * **Versão 4.1.2**
    * Data: 03/08/2020
    * Descrição: Atualização do item "2.1.6. Diretrizes para o Front
      End: Inclusão da linguagem Kotlin para plataforma Android".
    * Autor: SUART10

 * **Versão 4.1.3**
    * Data: 20/09/2021
    * Descrição: Atualização do item "Regras de validação para a camada do cliente - ítem 2"
      End: Substituição do AeroGear pelo AppAuth".
    * Autor: SUART10
