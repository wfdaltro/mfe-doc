

## Conceito e Cenário de Adoção

Microsserviço é uma abordagem arquitetural para o desenvolvimento de aplicações, as quais são decompostas em um conjunto de serviços colaborativos fracamente acoplados. Esses serviços podem ser desenvolvidos e implantados de forma independente, sem impactar e nem ser impactado por outros serviços.

Isso permite que times menores e mais fáceis de gerenciar possam entregar aplicações de forma mais rápida e com maior frequência, haja vista o escopo reduzido dos componentes. A independência dos serviços também diminui a quantidade de testes, bem como reduz o risco de regressão do sistema.

Do ponto de vista da operação, o desenvolvimento de microsserviços permite que o sistema seja escalado apenas nos componentes que forem necessários (escala no eixo Y), permitindo uma maior economia de recursos computacionais, além de isolar falhas da aplicação, aumentando a sua disponibilidade geral e facilitando a atuação em caso de incidente.

Sem entrar em discussões sobre arquitetura de microsserviços versus arquitetura monolítica, o cenário de utilização de uma arquitetura de microsserviços possui, dentre outras, as seguintes características:

*  A aplicação deve ser fácil de entender e modificar, de forma que os novos membros da equipe se tornem produtivos rapidamente.
*  A aplicação precisa de um processo de entrega contínua ou o custo do processo da implantação atual está muito alto.
*  A aplicação possui requisitos de escalabilidade de disponibilidade que demandam o aumento ou diminuição de instâncias de forma rápida e dinâmica.
*  A equipe deseja experimentar, de forma isolada, novas tecnologia ou até mesmo novos produtos.


## Arquitetura de Solução

Por se tratar de uma abordagem arquitetural, a arquitetura de microsserviços deve ser agnóstica em relação às tecnologias utilizadas para implementar os serviços.

No entanto, assim como acontece em aplicações corporativas de arquitetura monolítica, será definido um arcabouço inicial para o desenvolvimento de microsserviços, como forma de padronizar a entrega de soluções e preservar o conhecimento atual das equipes.

 


## Padrões para o Desenvolvimento

Além dos padrões e boas práticas para o desenvolvimento de APIs REST, bem como os demais processos e políticas de governança de APIs, existem também uma série de boas práticas para o desenvolvimento de microsserviços.

A seguir serão citados alguns indicadores de qualidade que serão exigidos nos projetos desenvolvidos utilizando a arquitetura de microsserviços.


### As 12 Boas Práticas para o Desenvolvimento de Aplicações em Nuvem.

Um importante compilado de boas práticas é o manifesto chamado “The Twelve Factor App” que descreve 12 práticas que auxiliam o desenvolvimento de aplicações em nuvem e que se aplicam também ao desenvolvimento de microsserviços. Segue abaixo a lista com as 12 práticas, cujos detalhes podem ser acessados em [https://12factor.net/pt_br/](https://12factor.net/pt_br/) ou no PPDS em [http://ppds.caixa/?page_id=4954](http://ppds.caixa/?page_id=4954).

1. Base de Código: a aplicação deve ter sua própria base de código, a qual deve ser rastreada em um sistema de controle de versão, como Git.
2. Dependências: a aplicação deve declarar e isolar todas as dependências, por meio de um manifesto de declaração de dependência e ferramentas de gerenciamento de pacotes, como npn ou maven.
3. Configurações: a aplicação deve manter suas configurações fora da sua base de código, utilizando, por exemplo, variáveis de ambiente.
4. Serviços de Apoio: todo os serviços de apoio utilizados pela aplicação devem ser tratados como recursos externos e devem permitir a configuração do destino desse serviço sem a necessidade de alteração em código.
5. Construa, Lance e Execute (Build, Release and Run): devemos separar os estágios de construção, lançamento e execução, preferencialmente utilizando ferramentas de Integração Contínua (CI) e Entrega Contínua (CD).
6. Processos: a aplicação deve ser "stateless" (não armazenam estado) e "share-nothing" (não compartilham dados). Quaisquer dados que precisem persistir devem ser armazenados em um serviço de apoio, tipicamente uma base de dados.
7. Vínculo de Porta: a aplicação deve expor seus serviços por meio de vínculos de portas e uma camada de roteamento manipula as requisições vindas de um hostname público para os processos web atrelados às portas.
8. Concorrência: a aplicação deve ser projetada de forma que seus processos possam ser escalados de forma independente, seja enquanto módulos rodando em máquinas separadas, seja em processos escalando dentro de uma mesma máquina.
9. Descartabilidade: a aplicação deve ser projetada visando uma inicialização rápida e um desligamento seguro, permitindo a automação da escalabilidade e facilitando tanto a implantação da aplicação, quanto a mudança de configurações.
10. Paridade entre DES e PRD: os ambientes de não-produtivos (desenvolvimento, teste, homologação etc.) devem ser o mais próximo possível do ambiente de produção. Essa equalização e proximidade deve ser a nível de código, pessoas e ambiente.
11. Logs: a aplicação deve tratar a escrita dos logs de eventos de forma padronizada, mas sem se preocupar com a centralização deles, que deve ser delegada para uma solução externa que dê a visão e o tratamento necessários.
12. Tarefas de Administração: os processos de administração da aplicação devem ser executados no mesmo ambiente que os processos de negócio. O código desses processos deve ser entregue junto com o código da aplicação, na mesma esteira.


## Migrando Aplicações Monolíticas para Microsserviços

Em alguns casos, pode ser necessária migração de uma aplicação tradicional monolítica para uma arquitetura de microsserviços.

Esse processo deve consumir bastante esforço das equipes de arquitetura, para que a jornada seja tranquila e a entrega tenha realmente os resultados de qualidade esperados.

Existem algumas metodologias para ajudar na migração de aplicações, tais como a abordagem de Chris Richardson para a extração de serviços de aplicações monolíticas, disponível em [https://microservices.io/refactoring/](https://microservices.io/refactoring/), ou a abordagem de 8 passos apresentada por Brent Frye, disponível em [https://insights.sei.cmu.edu/sei_blog/2020/09/8-steps-for-migrating-existing-applications-to-microservices.html](https://insights.sei.cmu.edu/sei_blog/2020/09/8-steps-for-migrating-existing-applications-to-microservices.html).

Ambas apresentam dicas importantes para o sucesso do projeto de migração no que tange a arquitetura de software.


## Considerações Finais

A arquitetura de microsserviços não deve ser encarada como uma evolução natural da arquitetura de aplicações web. Ela deve ser encarada como uma alternativa que pode ser aplicada em cenários bastante específicos e mesmo assim ela depende de uma série de fatores estruturais para que se alcance os resultados esperados.

As aplicações desenhadas e desenvolvidas utilizando a arquitetura de microsserviços acabam por serem divididas em pequenas aplicações, as quais precisam ser comunicar internamente e com outras aplicações além das suas fronteiras. Por conta disso, o ambiente dessas aplicações se torna muito mais complexo e, consequentemente, pode se tornar mais difícil de administrar e monitorar.

Da mesma forma, o processo de desenvolvimento também é muito mais complexo, pois agora teremos vários pequenos projetos com certo grau de independência que precisam, ao final, convergir em uma única solução que funcione de forma integrada.

Considerando um cenário de evolução do processo produtivo de TI, concomitantemente a qualquer iniciativa em microsserviços, é importante que sejam endereçadas outras iniciativas para tornar o ambiente mais propício a receber essas novas aplicações. Caso contrário, a implantação desse tipo de arquitetura pode trazer mais riscos do que benefícios.

Nesse sentido, a adoção da arquitetura de microsserviços deverá, preferencialmente, ser condicionada à pré-existência de outras iniciativas que darão condições para mitigar os riscos e extrair todos os benefícios esperados.

Essas iniciativas partem de uma reestruturação dos times e do investimento na cultura de DevOps, da automação e gestão do ambiente com uma solução de PaaS e da criação de um processo estruturado de Integração Contínua e Entrega Contínua (CI/CD).

Apenas com o conjunto dessas iniciativas é que se chega aos benefícios esperados, não apenas na entrega de microsserviços, mas também na entrega de aplicações monolíticas mais eficientes.


## Glossário

### API

É um tipo de serviço de integração que tem como característica a utilização do protocolo HTTP sem a necessidade de qualquer outro protocolo adicional, como é o caso dos web services SOAP.

Podemos desenvolver APIs em qualquer linguagem ou plataforma, desde que ela implemente uma interface HTTP.

### Microsserviços

É uma abordagem arquitetural para o desenvolvimento de aplicações. Nessa abordagem, a aplicação é decomposta em componentes básicos e isolados. Tal isolamento reflete em todo o ciclo de vida da aplicação (desenvolvimento, implantação e operação).

### API Manager

Conjunto de ferramentas responsável pela gestão das APIs da organização. Geralmente é composto por um gateway de proxy e execução de políticas, um portal de publicação, um portal de engajamento para desenvolvedores e uma solução de analytics.

### Open Banking

Movimento impulsionado pelo mercado e por órgãos reguladores que cria uma rede onde as informações financeiras dos clientes e das instituições possam ser compartilhadas entre os integrantes dessa rede, permitindo aos clientes acessar suas informações e serviços financeiros de forma centralizada, bem como migrar suas informações entre as instituições se forma simplificada.

Como visto acima cada conceito existe de forma isolada, e é importante destacar que, apesar dos conceitos estarem relacionados, não existe dependência direta, ou seja:

1. Posso integrar as instituições financeiras para o Open Banking utilizando APIs, que é a maneira mais simples até o momento, porém, essa integração poderia muito bem ser feita por meio de web services SOAP ou mensageria.

2. Estamos utilizando uma solução de API Management para controlar de forma centralizada o consumo das APIs, porém, podemos ter centenas de APIs sendo usadas e reutilizadas sem uma solução de API Management, desde que estejamos dispostos a arcar com os custos de uma gestão descentralizada.

3. Podemos utilizar a abordagem de microsserviços para o desenvolvimento de APIs, aproveitando todas as vantagens dessa abordagem e arcando com suas desvantagens, porém, o desenvolvimento de aplicações monolíticas existirá pois ambas as abordagens são válidas e apresentam pontos positivos e negativos, dependendo do cenário onde são aplicadas.


## Histórico da Revisão

Data       | Versão | Descrição                                     | Autor
-----------|--------|-----------------------------------------------|-----------------
25/01/2021 | 1.0    | Criação do documento                          | César Lima Piau
26/01/2021 | 1.1    | Pequenas correções e inclusão de novos itens. | César Lima Piau
20/09/2021 | 1.2    | Segregação das arquiteturas por framework | Wanderson Daltro


