MicroProfile é um conjunto de especificações para o desenvolvimento de aplicações que permite que desenvolvedores Jakarta EE aproveitarem seus conhecimentos enquanto mudam seu foco de aplicações tradicionais em 3 camadas para microsserviços. As APIs MicroProfile estabelecem as fundações para o desenvolvimento de aplicações baseadas na arquitetura de microsserviços, adotando um subconjunto dos padrões da especificação Jakarta EE e extensões que foram definidas para a implementação de microsserviços.

As especificações de MicroProfile incluem:

*  Config
*  Fault Tolerance
*  Health Check
*  Metrics
*  Open API
*  Rest Client
*  JWT Authentication
*  Open Tracing API
*  CDI
*  JAX-RS
*  JSON Parsing
*  JSON Bind

Essas especificações são abertas, mantidas pela comunidade e cobrem os principais requisitos para o desenvolvimento de microsserviços. A seguir será feito um resumo das principais especificações, as quais podem ser analisadas com detalhe no endereço https://microprofile.io.


#### MicroProfile Config

A especificação MicroProfile Config permite a externalização de configurações da aplicação de forma que um microsserviço obtenhas seus parâmetros de configuração das mais diversas fontes.

As configurações são injetadas na aplicação de forma segura e padronizada, independentemente da fonte da configuração. Os exemplos a seguir demonstra como funciona a injeção de configurações:

```java
// injeção de propriedade booleana
@Inject @ConfigProperty("ONE_CLICK_CHECKOUT_ALLOWED_KEY")
Boolean oneClickCheckoutAllowed;

// injeção de propriedade com valor padrão
@Inject @ConfigProperty("MAX_ITEM_COUNT_KEY", defaultValue="100")
Integer maxItemCount;

// injeção de classe de propriedade
@Inject @ConfigProperty("MINIMUM_AMOUNT_KEY")
Amount minimumAmount;
```

#### MicroProfile Fault Tolerance

A especificação de MicroProfile Fault Tolerance traz mecanismos que facilitam a construção de microsserviços mais resilientes, separando a lógica de negócio da lógica de execução. Ela implementa os principais padrões de resiliência, tais como TimeOut, RetryPolicy, Fallback, Bulkhead e Circuit Breaker.

O exemplo a seguir mostra a implementação de um caso se uso com timeout e fallback, caso o primeiro método não responda dentro do período estipulado, um segundo método é executado imediatamente:

```java
@GET
@Timeout(500)
@Fallback(fallbackMethod = "getBestsellersFallback")
public Response getPersonalRecommendations() throws InterruptedException {
  // retorna recomendações pessoais por meio de uma consulta a um microsserviço de recomendações
  ...
}
public Response getBestsellersFallback() {
  // retorna recomendações genéricas por meio de uma consulta a outro microsserviço
  ...
}
```


#### MicroProfile Health Check

A especificação MicroProfile Health Check ajuda a expor o estado de saúde de um serviço de forma que agentes automatizados possam agir com base nessas informações para reiniciar um serviço parado ou até mesmo colocá-lo em espera até que ele se restabeleça por completo.

Ela implementa os conceitos de vivo e pronto (live e ready) para determinar não apenas se o serviço está no ar, mas se ele está pronto para receber requisições. A especificação foi concebida também com o objetivo de auxiliar as soluções de orquestração de containers a gerir os serviços que rodam nesses ambientes.

O exemplo a seguir mostra como implementar uma simples checagem de readiness para verificar se o pool de conexões está no ar e pronto para receber requisições:

```java
@Readiness
@ApplicationScoped
public class ConnectionPoolCheck implements HealthCheck {
  @Override
  public HealthCheckResponse call() {
    if (isConnectionPoolHealthy()) {
      return HealthCheckResponse(“customer-cp”)
        .up()
        .build();
    } else {
      return HealthCheckResponse(“customer-cp”)
        .down()
        .build();
    }
  }
  // check that there is no connection shortage
  private boolean isConnectionPoolHealthy() { … }
}
```


#### MicroProfile Metrics

A especificação MicroProfile Metrics permite gerar detalhes sobre o comportamento dos serviços em tempos de execução e entregar para agentes de monitoração. Ela também provê uma API Java comum para expor dados de telemetria.

A especificação permite gerar dados para planejamento de capacidade, descoberta proativa de problemas além de ajudar na automação da escalabilidade da aplicação. A especificação suporta vários tipos de dados, tais como contadores, histograma, tempo de resposta etc.

O exemplo a seguir demostra como mensurar a duração da execução de um método:

```java
@POST
@Produces(MediaType.APPLICATION_JSON)
@Timed(absolute = true,
    name = "microprofile.ecommerce.checkout",
    displayName = "check-out time",
    description = "time of check-out process in ns",
    unit = MetricUnits.NANOSECONDS)
public Response checkOut(...) {
  // executa a lógica de checkout
  ...
  return Response.ok()… build();
}
```


#### MicroProfile Open API

A especificação MicroProfile Open API provê um conjunto de interfaces Java e modelos que permitem aos desenvolvedores produzirem de forma nativa documentos no padrão OpenAPI v3.

A especificação OpenAPI, anteriormente conhecida como Swagger, é o padrão de documentação para APIs RESTful mais utilizado pelo mercado.

Os desenvolvedores têm algumas opções de uso da especificação: usar anotações JAX-RS em conjunto com anotações OpenAPI para gerar o documento de forma automatizada, utilizar um documento pré concebido durante a fase de design como entrada para o desenvolvimento da API ou usar um modelo programático para escrever e gerar tanto a estrutura do documento quanto a documentação completa. A especificação também provê formas de mesclar as 3 abordagens para gerar o documento final.

#### MicroProfile Rest Client

A especificação MicroProfile Rest Client simplifica a construção de clientes REST, provendo componentes seguros e consistentes baseados nas APIs JAX-RS para a chamada de serviços RESTful sobre o protocolo HTTP.

Ela permita mapear facilmente um método de uma interface Java para uma requisição a uma API REST usando anotações JAX-RS. O reuso de clientes na aplicação também é facilitado e evita a duplicação de código.

O exemplo a seguir demonstra a criação de uma interface Java com a representação de um serviço REST remoto:

```java
@RegisterRestClient
@Path("/movies")
public interface MovieReviewService {
    @GET
    @Path("/{movieId}/reviews")
    Set<Review> getAllReviews( @PathParam("movieId") String movieId );
    @GET
    @Path("/{movieId}/reviews/{reviewId}")
    Review getReview( @PathParam("movieId") String movieId, @PathParam("reviewId") String reviewId );
    @POST
    @Path("/{movieId}/reviews")
    String submitReview( @PathParam("movieId") String movieId, Review review );
}
```

A partir da criação da interface é possível usá-la para definir a localização do serviço remoto e chamar a operação desejada, conforme exemplo abaixo:

```java
URI apiUri = new URI("http://localhost:9080/movieReviewService");
MovieReviewService reviewSvc = RestClientBuilder.newBuilder()
            .baseUri(apiUri)
            .build(MovieReviewService.class);
Review review = new Review(3 /* stars */, "This was a delightful comedy, but not terribly realistic.");
reviewSvc.submitReview( movieId, review );
```

A interface anotada com @RegisterRestClient pode ser simplesmente injetada em qualquer bean CDI, conforme exemplo abaixo:

```java
@Inject @RestClient
MovieReviewService reviewSvc;
```


#### MicroProfile JWT Authentication

A especificação MicroProfile JWT Authentication provê os mecanismos necessários para implementar a segurança da aplicação baseada nos padrões oAuth 2.0, OpenID Connect e JWT (Json Web Token).

O uso de tokens para a segurança de serviços, principalmente microsserviços baseados em APIs RESTful, garante simplicidade e interoperabilidade na propagação e validação de identidades.

O exemplo a seguir demostra como obter acesso a um token JWT e acessar as informações do mesmo para validação:

```java
@Path("/pizza")
@RequestScoped
public class PizzaEndpoint {
    @Inject
    @Claim("raw_token")
    private String rawToken;
    @Inject
    @Claim("groups")
    private Set<String> groups;

    @GET
    @Path("/{pizzaId}")
    @RolesAllowed({"Admin", "Everyone"})
    public String getPizza() {
        ...
    }
```


#### MicroProfile OpenTracing

A especificação MicroProfile OpenTracing permite que um serviço seja facilmente rastreado em um ambiente distribuído, sem a necessidade de qualquer adição de código na aplicação para essa finalidade e sem que a equipe de desenvolvimento conheça os detalhes da arquitetura de rastreamento (tracing) do ambiente onde a aplicação está sendo executada.

Em um ambiente onde microsserviços são coreografados, é importante que as chamadas nos serviços que compõe uma transação sejam correlacionadas e rastreadas, de forma que seja possível ter uma visão fim a fim.

A implementação permite que a aplicação seja instrumentalizada de tal forma que, para toda mensagem recebida, seja atribuído um correlation ID que é logado e propagado para todas as chamadas subsequentes da transação.


### Implementações de MicroProfile

As implementações de MicroProfile nada mais são do que frameworks para o desenvolvimento e a execução de aplicações Java que são aderentes às especificações de MicroProfile. Essas runtimes, como são conhecidas, são otimizadas para serem leves, rápidas e fáceis de usar, muitas vezes sendo empacotadas junto com a aplicação em um único arquivo .jar (standalone), o que facilita a publicação e a execução em ambiente de nuvem.

Existem diversas implementações de MicroProfile disponíveis para utilização, tanto como open source quanto com licenciamento enterprise. Dentre as implementações que atendem à versão mais recente da especificação (4.0), poderão ser utilizadas duas opções:


#### Open Liberty e Websphere Liberty

Open Liberty e Websphere Liberty são implementações da IBM, sendo as versões open source e enterprise, respectivamente. O Liberty hoje é um produto presente em diversas soluções da IBM, inclusive no CICS.

A escolha se deu pela maturidade do projeto e pela possibilidade de iniciar o desenvolvimento utilizando a versão open source e posteriormente contratar o suporte, caso seja necessário.

Mais informações sobre o Open Liberty e o WebSphere Liberty podem ser encontradas em [https://openliberty.io](https://openliberty.io) e [https://developer.ibm.com/wasdev/websphere-liberty](https://developer.ibm.com/wasdev/websphere-liberty), respectivamente.


#### Quarkus

Quarkus é uma implementação open source, patrocinada pela Red Hat e que se destaca por ser desenvolvida com foco em nuvem e containers e pelo baixo consumo de recursos.

Outro ponto positivo do Quarkus é a possibilidade de criar aplicações compiladas utilizando a GraalVM, dispensando a necessidade de instalação de JVM nos servidores e reduzindo ainda mais o consumo de recursos e o tempo de resposta (BOOT e First Response Time).

Apesar de open source, alguns contratos de suporte de soluções da Red Hat cobrem também o Quarkus.

Mais informações sobre o Quarkus podem ser encontradas em [https://quarkus.io](https://quarkus.io).



### Especificações Obrigatórias

Todas as especificações de MicroProfile trazem recursos importantes para a qualidade dos serviços, no entanto, como a especificação não obriga a implementação de todas, fica a cargo do arquiteto ou desenvolvedor implementar aquelas que ele entender serem importantes para a aplicação.

Algumas tem obrigatoriedade implícita, de acordo com o tipo de aplicação. Por exemplo, em uma API Rest, sempre será obrigatório o uso de JAX-RS e OpenAPI.

No entanto, para tornar a aplicação mais resiliente e flexível para os diversos usos e ambiente de execução, algumas delas devem ser implementadas em todos os projetos.

*  Config: Garanta que os parâmetros da aplicação possam ser gerenciados de forma centralizada e até mesmo externalizadas para plataformas de execução de aplicações.
*  Fault Tolerance: Garanta que as falhas na integração com os diversos serviços acessados pela aplicação seja resiliente a qualquer tipo de indisponibilidade.
*  Health Check: A plena disponibilidade dos serviços, principalmente em um ambiente com dezenas de serviços que se inter-relacionam, depende uma  monitoração constante da saúde da aplicação. A implementação de um endpoint de health check que verifique os principais componentes da aplicação pode ajudar as equipes de monitoração na resolução de incidentes e permitir uma melhor gestão do ambiente.
*  Metrics: O envio de métricas padronizadas aumenta o nível de observabilidade da aplicação, permitindo a monitoração em tempo real e viabilizando a execução de ações tanto reativas quanto proativas em cima dos dados coletados.

## PaaS e Container

Por se tratar de uma tecnologia serverless, ou seja, sem a utilização de servidores de aplicação para gerenciar a implantação, configuração e execução das aplicações, é essencial que tenhamos uma plataforma de seja capaz de efetuar essas tarefas de forma centralizada.

Além disso, muitas das funcionalidades da especificação de MicroProfile foram pensadas para um ambiente baseado em container e é justamente nesse ambiente que elas apresentam melhor desempenho.

Portanto, o desenvolvimento de microsserviços baseados em MicroProfile deve ter como premissa a execução em container num ambiente de PaaS baseado em Kubernetes, capaz de automatizar o provisionamento, a administração e o escalonamento das aplicações.

#### Projetos Quickstart

Para os principais cenários tecnológicos mapeados para uso de microsservios, são disponibilizados templates que possibilitam um início rápido de projetos, assim como servem de referência para os desenvolvedores.

Para projetos Quarkus, os quickstarters se encontram no seguinte link:

[Repositório de projetos quickstarts Quarkus](http://fontes.des.caixa/suart/arquiteturas-de-referencia/quickstarts/quarkus).
 

## Histórico da Revisão

Data       | Versão | Descrição                                     | Autor
-----------|--------|-----------------------------------------------|-----------------
25/01/2021 | 1.0    | Criação do documento                          | César Lima Piau
26/01/2021 | 1.1    | Pequenas correções e inclusão de novos itens. | César Lima Piau
20/09/2021 | 1.2    | Segregação das arquiteturas por framework | Wanderson Daltro
30/09/2021 | 1.2    | Inclusão do link para o repositório de projetos Quickstarter | wanderson Daltro
