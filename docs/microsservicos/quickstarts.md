 

 
[Repositório de projetos Quickstarters Quarkus](http://fontes.des.caixa/suart/arquiteturas-de-referencia/quickstarts/quarkus).

 
[Repositório de projetos Quickstarters Spring](http://fontes.des.caixa/suart/arquiteturas-de-referencia/quickstarts/springboot).


## Histórico da Revisão

Data       | Versão | Descrição                                       | Autor
-----------|--------|-------------------------------------------------|-----------------
30/09/2021 | 1.0    | Inclusão dos links para o repositório de projetos Quickstarters | wanderson Daltro

