### Spring Boot
O Spring Boot é um projeto que visa facilitar a configuração e publicação das aplicações. O Spring Boot é construído em cima do Spring Framework, com isso ele obtém os benefícios de um framework maduro e padrão de mercado, Seu objetivo não é criar novas soluções para problemas que já foram resolvidos, mas sim reaproveitar soluções amplamente utilizadas pelo mercado e aumentar a produtividade do desenvolvedor.
  
### Spring Cloud 
Spring Cloud é basicamente um conjunto de ferramentas que auxiliam equipes de desenvolvimento no processo de contrução de sistemas que sigam padrões comuns de mercado para sistemas distribuídos. 
  
### Arquitetura 
A padronização no processo de desenvolvimento traz uma gama de benefícios como, maior produtividade, curva de apendizagem diminuida, menor custo, etc.
Como forma de uniformizar o desenvolvimento de soluções corporativas desenvolvidas com Spring Boot, foi definido um conjunto mínimo diretrizes para o desenvolvimento de novas soluções.

#### Visão Geral de Implantação

!["Visão geral dos coponentes Microsserviços Springboot"](../img/microsservicos/visao-geral.png)


#### Módulos Spring 

##### Spring Boot Devtools

O Devtools é um conjunto ferramentas adicionais ao Spring Boot que tem a finalidade de aumentar a produtividade e tornar a experiência do desenvolvedor mais fluída. Ele adiciona ao Spring Boot as seguintes capacidades:

  - Padrões de propriedade
  - Reinício Automático da aplicação
  - Live Reload

Sua configuração é automática, bastantando ao desenvolvedor adicionar a seguinte dependência ao projeto

```xml
<dependency>
  <groupId>org.springframework.boot</groupId>
  <artifactId>spring-boot-devtools</artifactId>
  <scope>runtime</scope>
  <optional>true</optional>
</dependency>
```
##### Spring Boot Web

O spring Boot Web configura automaticamente uma gama de funcionalidades necessárias para o desenvolvimento de uma aplicação web ou microsserviço. 
O Spring Boot Web utiliza o Spring MVC como framework, realizando nele todas as configurações necessárias para tornar a aplicação funcional.
Para adicioná-lo é necessário apenas incluir sua dependência no pom.xml


```xml
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-web</artifactId>
</dependency>
```

O Spring Boot Web inclui um servidor embarcado como parte do artefato implantável. o Apache Tomcat é utilizado como servidor embarcado, porém é recomendado substituí-lo pelo servidor Jboss Undertow. Para realizar a substituição deve-se desabilitar o Apache Tomcat, para isso é necessário realizar a exclusão da dependencia transitiva do Apache Tomcat do Spring Boot WEB. Após isso basta apenas incluir o módulo starter do undertow e o mesmo já será cofigurado automaticamente como servlet container da aplicação.


```xml
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-web</artifactId>
    <exclusions>
        <exclusion>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-tomcat</artifactId>
        </exclusion>
    </exclusions>
</dependency>
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-undertow</artifactId>
</dependency>
```

##### Spring Boot Actuator
O Spring Boot Actuator expõe informações operacionais sobre o sistema em produção como, saúde da aplicação, métricas, despejo de memória e threads, variáveis de ambiente, etc. Ele usa endpoints HTTP ou beans JMX para permitir interações.

Para habilitar o Spring Boot Actuator,  é necessário adicionar sua dependência ao pom.xml do projeto.

```xml
<dependency>
	<groupId>org.springframework.boot</groupId>
	<artifactId>spring-boot-starter-actuator</artifactId>
</dependency>
```
Uma vez incluída a dependência, vários endpoints se tornam disponíveis para serem consumidos no sistema.
O Spring Boot Actuator fornece um endpoint de descoberta que retorna links para todos os endpoints disponibilizados pelo Actuator. Esse endpoint  de descoberta é definido pelo path '/actuator'
Através do endpoint '/actuator/healt' são disponibilizados todos os healthchecks. Por padrão o Actuator disponibiliza apenas o healthcheck de estado da aplicação. Para liberar todos os healthchecks, deve-se adicionar ao arquivo application.properties a seguinte propriedade:
```properties
management.endpoint.health.show-details=always
```
Através do endpoint '/actuator/metrics' são disponibilizados todas as metricas disponíveis.
Métricas e healthchecks adicionais podem ser incluídos por meio de dependências que são adicionadas ao projeto, ou podem ser criados pela equipe de desenvolvimento de acordo com a necessidade do projeto. 


&nbsp;



##### Spring Config Properties
O Spring Boot permite que as configurações da aplicação sejam externalizadas podendo ser obtidas de uma variedade de fontes externas, o que inclui, arquivos de propriedades Java, arquivos YAML, variáveis ​​de ambiente e argumentos de linha de comando. Essa característica é extremamente útil, pois nos permite trabalhar com o mesmo código em ambientes diferentes.
 O Spring acessa as propriedades dos arquivos de configuração externa através de injeção de dependência, qualquer chave cadastrada em um arquivo de configuração pode ser injetada em um bean do Spring.
 Existem 3(três) formas de realizar essa injeção, que são:

- Anotação @Value: Permite realizar a injeção de propriedades simples.

- API Environment: Nesse cenário, apenas a classe Environment é injetada nos beans do Spring. O acesso as propriedades externalizadas é feito através dessa classe utilizando-se o método  getProperty. Esse método possui duas versões, na primeira recebe o nome da propriedade a ser recuperada, na segunda, além do nome da propriedade, recebe um valor padrão caso a propriedade não seja encontrada.

- Anotação @ConfigurationProperties: Permite realizar a injeção de objetos complexos. Geralmente quando é necessário injetar várias propriedades, o código pode se tornar confuso e poluído se usarmos muitas anotações @Value, nesses casos, podemos agrupar todas as propriedades necessárias em um POJO e utilizar a anotação @ConfigurationProperties para injetar esse POJO com todas as propriedades já preenchidas do arquivo de configuração externo.

````java
@SpringBootApplication
public class MsSpringApplication {
	
	// Injeção de propriedade simples
	@Value("${jdbc.url}") 
	private String jdbcUrl;
	
	// Injeção da classe Environment para acesso as propriedades
	@Autowired
	private Environment env;

	// Injeção de classe que agrupa várias propriedades através de @ConfigurationProperties
	@Autowired
	private ConfigPropertiesClass config;
	
	public static void main(String[] args) {
		SpringApplication.run(MsSpringApplication.class, args);
	}

}

@ConfigurationProperties(prefix = "mail") 
public class ConfigPropertiesClass  { 
    private String host; 
    private int port; 
    private String from; 
}
````
O arquivo de externalização de configuração para atender a aplicação acima teria as seguintes propriedades.
```properties
jdbc.url=jdbc:h2:mem:a404b31d-7c93-4ab1-9717-f5164a0a1858
mail.host=mail.server
mail.port=587
mail.from=sender@mail
```
&nbsp;

##### Spring Boot  Data JPA

O Spring Data JPA adiciona uma camada sobre o JPA. Isso significa que ele usa todos os recursos definidos pela especificação JPA, especialmente os mapeamentos de entidade e associação, o gerenciamento do ciclo de vida da entidade e os recursos de consulta do JPA. Além disso, ele adiciona seus próprios recursos, como uma implementação do padrão Repository e a criação de consultas de banco de dados a partir de nomes de métodos.

Os Repositories são interfaces Java que permitem que o desenvolvedor defina um contrato de acesso a dados. A estrutura Spring Data JPA pode então inspecionar esse contrato e construir automaticamente a implementação da interface.

É necessário apenas adicionar o artefato spring-boot-starter-data-jpa e seu driver JDBC ao arquivo pom.xml.

```xml
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-data-jpa</artifactId>
</dependency>
```
```properties
spring.datasource.url=jdbc_url
spring.datasource.username=user
spring.datasource.password=pass
spring.jpa.properties.hibernate.dialect= dialect 
spring.jpa.hibernate.ddl-auto=update/drop-create/none

```

##### Spring Cloud Open Feign
O projeto OpenFeign  permite a criação de clientes Rest/SOAP de forma declarativa.
O Spring Cloud OpenFeign fornece  integração do OpenFeign com Spring Boot por meio de autoconfiguração além de adicionar suporte para anotações Spring MVC.
Com ele possível criar clientes Rest através de interfaces que façam uso em conjunto das anotações do OpenFeign e Spring MVC  para indicar quais serviços/endpoints serão chamados. Essas anotações são processadas em runtime para criar proxies gerenciados pelo Spring.
Para habilitar o Spring Cloud OpenFeign alguns passos devem ser seguidos.

A dependência do starter relativo ao OpenFeign deve ser incluída no pom.xml do projeto.
```xml
<dependency>
	<groupId>org.springframework.cloud</groupId>
	<artifactId>spring-cloud-starter-openfeign</artifactId>
</dependency>
```

A aplicação Spring deve ser anotada com @EnableFeignClients. Esse passo irá habilitar a autoconfiguração do framework.
```java
@SpringBootApplication
@EnableFeignClients
public class MsSpringApplication {
	public static void main(String[] args) {
		SpringApplication.run(MsSpringApplication.class, args);
	}
}
```
Com esses passos, o  Spring Cloud OpenFeign já está habilitado com sua configuração padrão. Seguindo o padrão do Spring Boot, qualquer customização das configurações pode ser feita no arquivo application.properties

```properties
feign.client.config.default.connectTimeout: 5000
feign.client.config.default.readTimeout: 5000
feign.client.config.default.loggerLevel: basic
```
A criação do cliente REST se dá através da criação de uma interface que seja anotada com @FeignClient informando o endereço do serviço a ser consumido.
```java
@FeignClient(name = "${feign.name}", url = "${feign.url}")
public interface UsersClient {
	@GetMapping(value = "/users")
	List<Object> findUsers();	
}
```
&nbsp;

##### Spring Cloud Cirtcuit Breaker
O Spring Cloud Circuit Breaker é a biblioteca padrão do Spring para tratar requisitos de resiliência dos microserviços. Algumas implementações são fornecidas sendo a mais utilizada a resilience4J.
O Spring Cloud Circuit Breaker permite a utillização de padrões de implementação que fornecem resiliência aos microserviços,
- Circuit Breaker: Esse padrão permite realizar bloqueios temporários a uma parte do sistema que esteja com falhas.
- Retry: Esse padrão permite repetir execuções que falharem, levando em conta que alguma falhas podem ser transitórias e se resolvem sem intervenção.
- Bulkhead: Esse padrão garante que uma falha em uma parte do sistema não cause o falha no sistema todo. Ele controla o número de chamadas concorrentes que um componente pode ter e isola os recursos, para caso algum apresente falha, os restantes continuem funcionando.
- Rate Limit: Esse padrão limita o volume de requisições a um recurso.


Para habilitar o Spring Cloud Circuit Breaker,  é necessário adicionar sua dependência ao pom.xml do projeto.

```xml
<dependency>
	<groupId>org.springframework.cloud</groupId>
	<artifactId>spring-cloud-starter-circuitbreaker-resilience4j</artifactId>
</dependency>
```

Feito isso, o Spring Cloud Circuit Breaker á está autoconfigurado com as opções padrões disponibilizadas pelo módulo.
Assim como os outros projetos do ecossistema Spring, as customizações de configurações são feitas a partir do arquivo application.properties

```properties	
resilience4j.circuitbreaker.configs.default.registerHealthIndicator=true
resilience4j.circuitbreaker.configs.default.slidingWindowSize= 10
resilience4j.circuitbreaker.configs.default.minimumNumberOfCalls= 5

resilience4j.circuitbreaker.instances.servico.baseConfig=default
resilience4j.circuitbreaker.instances.servico.minimumNumberOfCalls =1

resilience4j.retry.configs.default.maxRetryAttempts= 2
resilience4j.retry.configs.default.waitDuration= 1000
resilience4j.retry.instances.servico.baseConfig=default

resilience4j.timelimiter.configs.default.timeoutDuration =2s
resilience4j.timelimiter.configs.default.cancelRunningFuture=true
resilience4j.timelimiter.instances.servico.baseConfig=default

resilience4j.bulkhead.configs.default.maxConcurrentCalls: 100
resilience4j.bulkhead.instances.servico.baseConfig=default
resilience4j.bulkhead.instances.servico.maxConcurrentCalls: 10

resilience4j.ratelimiter.configs.default.registerHealthIndicator: false
resilience4j.ratelimiter.configs.defaultlimitForPeriod: 10
resilience4j.ratelimiter.configs.defaultlimitRefreshPeriod: 1s
resilience4j.ratelimiter.configs.defaulttimeoutDuration: 0
resilience4j.ratelimiter.instances.servico.baseConfig: default
```
No trecho acima, temos uma exemplo de configuração básicas dos padrão de resiliência disponibilizados pelo Spring Cloud Circuit Breaker. Abaixo, temos um trecho de código Java que utiliza as configurações apresentadas. Assim como outros projetos Spring trabalha faz o processamento das anotações e, tempo de execução.
```java
@CircuitBreaker(name = "servico", fallbackMethod  =  "metodoDeFallback")
@Bulkhead(name = "servico", , type  =  Type.THREADPOOL", fallbackMethod  =  "metodoDeFallback" )
@TimeLimiter(name = "servico", fallbackMethod  =  "metodoDeFallback")
@Retry(name = "servico", fallbackMethod  =  "metodoDeFallback")
@RateLimiter(name = "servico", fallbackMethod  =  "metodoDeFallback")
public String metodoQueVaiFalhar() { 
	throw new Exception("Falha proposital");
}

public String metodoDeFallback() { 
	throw new String("Retorno alternativo");
}
```

&nbsp;
##### Spring Cloud Sleuth
O Spring Cloud Sleuth permite instrumentalizar o rastreamento de requisições do início ao fim. 
Ele utiliza o conceito de  trace-id e span-id, que nada mais são que identificadores que são anexados aos logs que serão usados para identificar o caminho percorrido pela requisição.
O rastreamento cobre tanto o caminho percorrido entre as camadas de um mesmo serviço, quanto a comunicação remota entre serviços distribuídos.
Os logs gerados pelo Sleuth podem ser usados ​​por ferramentas como Zipkin e ELK para armazenamento e análise. 
A análise desse logs por ferramentas adequadas permite analisar rapidamente um fluxo a fim de identificar se o mesmo foi bem-sucedido, se existe latência indesejada em alguma etapa e até mesmo identificar um ponto de exceção no fluxo.
A utilização padrão do Sleuth é feita sem adição alguma de código específico ao serviço, sendo necessário apenas a inclusão de sua dependência no pom.xml do projeto.

```xml
<dependency>
	<groupId>org.springframework.cloud</groupId>
	<artifactId>spring-cloud-starter-sleuth</artifactId>
</dependency>
```

 
#### Bibliotecas de terceiros
##### SpringDoc Open API
SpringDocs Open API é uma biblioteca que ajuda a automatizar a geração de documentação de API em projetos Spring.
O SpringDocs utiliza o swagger que além de ser uma implementação da especificação OpenAPI, fornece um conjunto de ferramentas que vão desde o design da API até os testes.
A utilização do SpringDocs  pode ser feita de duas formas:

- Code First: A geração da documentação é feita a partir do processamento de anotações inseridas no código desenvolvido. Um dos benefícios dessa abordagem é que a documentação por ser gerada a partir do código, sempre está atualizada sem a necessidade de alteração de documentos externos.
- Design First: Todo o design(contrato) da API é criado antes do início da codificação. Essa abordagem tem como benefício a API ser desenhada como um produto final que será entregue a um cliente, já sendo trabalhando desde o início aspectos como verbos HTTP adequados, requisitos de segurança, performance, usabilidade, além de propiciar uma maior facilidade no aprendizado.
Para habilitar o uso do SpringDocs Open API, sua dependência deve ser incluída no pom.mxml do projeto

```xml
<dependency>
	<groupId>org.springdoc</groupId>
	<artifactId>springdoc-openapi-ui</artifactId>
</dependency>
```
Ao incluir a dependência, o projeto terá novos endpoints configurados:
- /swagger-ui/index.html: Interface do swagger para realizar testes no endpoints.
-/v3/api-docs: Documentação OpenAPI em formato json.
-/v3/api-docs.yaml: Documentação OpenAPI em formato yaml.

&nbsp;


#### Padrões, regras e boas práticas

##### Tratamento de Exceções

O Spring fornece alguns mecanismos para tratamento de exceções na aplicação. Desses mecanismos, 2(dois) podem ser adotados nos projetos desenvolvidos pela Caixa, sendo esses:

- ControllerAdvice: funciona como um interceptador de exceções não tratadas nos métodos das controllers.

```java

    @ControllerAdvice
    public class APIExceptionHandler<T> {
        @ExceptionHandler(value = { ServerErrorException.class })
        public ResponseEntity<Response<T>> handleServerErrorException(ServerErrorException exception,  WebRequest request) { 
            Response<T> response = new Response<>();
            response.addErrorMsgToResponse(exception.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
        }
    }
```
- ResponseStatusException: Consiste basicamente em capturar as exceções de negócio e lançar uma exceção ResponseStatusException. O Spring internamente faz as tratativas necessárias para converter essa exceção em uma resposta HTTP.

```java

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Collection<T>> search(P params);
        try {
        return ResponseEntity.ok().body(service.search(params));
        } catch (NaoEncontradoException ex) {
            throw new ResponseStatusException(
            HttpStatus.NOT_FOUND, "Nenhum recurso encontrado com os parâmetros informados", ex);
        }
    }
```

&nbsp;

###### Quando usar cada uma das abordagens? 
O ResponseStatusException deve ser utilizado caso uma exceção precise ser tratada de forma diferente dependendo do cenário em que é lançada. 
Para exceções genéricas e globais, o ControllerAdvice deve ser adotado como mecanismo de tratamento de erros.

###### Injeção por atributo, construtor ou setter
O Spring permite que a injeção de dependências seja feita das seguintes formas:
- Injeção via construtor: quando  a injeção de dependência é feita definição dos construtores das classes.
- Injeção via setter: quando  a injeção de dependência é feita na definição dos  seters das classes.
- Injeção via atributo: quando a injeção de dependência é feita definição dos atributo da classe.

É altamente recomendado usar a injeção nos construtores para dependências obrigatórios (o objeto se torna imutável) e a injeção nos métodos setter para dependências opcionais. Os testes unitários são impactados positivamente quando usadas esses abordagens.

###### Convenção de nomes
Além de facilitar a legibilidade e a compreensão do código, manter o padrão de nomes dos beans do Spring facilita muito quando trabalhamos com AOP ou criamos alguma feature que faça uso de convenção sobre configuração.
É altamente recomendável seguir o padrão dos sufixos: ***Service** para classes de serviço, ***Repository** para classes de repositório, ***Controller** ou***Resource** para os controladores de API.



###### Uso de Optional
Optional é uma classe cujo objetivo é encapsular um objeto a fim de informar se o valor desse objeto está presente ou não.
É altamente recomendável que todas as classes de repository retornem sempre Optional quando o retorno for um objeto único. 

Assim,as classes de serviço devem utilizar os recursos do Optional para tratar possíveis cenários de erro. 
```java

public class Service {
    public T findById(K id) throws NotFoundException {
        return repository.findById(id).orElseThrow(NotFoundException::new);
    }
}
```
Para retornos que possuam múltiplos resultados, deve-se ser utilizadas as interfaces Collection ou List.


#### Projetos Quickstart

Para os principais cenários tecnológicos mapeados para uso de microsservios, são disponibilizados templates que possibilitam um início rápido de projetos, assim como servem de referência para os desenvolvedores.

Para projetos Springboot, os quickstarters se encontram no seguinte link:

[Repositório de projetos quickstarts Spring](http://fontes.des.caixa/suart/arquiteturas-de-referencia/quickstarts/springboot).
 
 
## Histórico da Revisão

Data       | Versão | Descrição                                     | Autor
-----------|--------|-----------------------------------------------|-----------------
23/09/2021 | 1.0    | Criação do documento                          |  Wanderson Daltro
24/09/2021 | 1.1    | Remoção do uso do framework Lombok. | Wanderson Daltro
30/09/2021 | 1.2    | Inclusão do link para o repositório de projeto Quickstarter | wanderson Daltro

