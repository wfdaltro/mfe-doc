# Guia para criação de aplicações Micro Frontend

Micro Frontends estão gradualmente ganhando popularidade como uma maneira de separar aplicativos de front-end em peças de pequeno desempenho e fáceis de manter.

A maior parte da configuração é feita no aplicativo raiz e, além disso, a CLI create-single-spa faz a maior parte do trabalho.


O primeiro passo para iniciar o desenvolvimnto de aplicativos Micro frontends com Single-SPA é a instalação da ferramenta __'create-single-spa'__ , ela deve ser instalada a partir do gerenciador de pacotes do node: 


```sh
npm install [--global|-g] create-single-spa
```

A ferramenta  __'create-single-spa'__ faz a geração automatica deos projetos Single SPA. Ela gerencia as configurações para Webpack, Babel, etc, e de toda a  estrutura de front-end dos projetos.

Existem duas forma de uma aplicação de Micro Frontends ser construida, a partir do carregamento dos Micro Frontends em tempo de compilação e a partir do carregamento dos Micro frontends em tempo de execução.

Como padrão para o desenvovimento no ambito Caixa, deve ser sempre utiizado o carregamento em tempo de execução, que implica na aplicação Root importar através de "mapas de importação" os Micro Frontends a partir de URL's onde os mesmos estejam publicados. Isso permite uma maior independencia na gestão de implantação por parte dos times, permitindo atualização nos Micro Frontends sem a necessidade de republicar a aplicação Root.

Mas o que são mapas de importação?

- Os mapas de importação melhoram a experiência do desenvolvedor de módulo ES no navegador, permitindo que você escreva algo como "import auth from '@cef/auth'" ao invez de precisar usar uma URL abosluta ou relativa para sua instrução de importação.

O recurso de mapas de importação está atualmente ainda não é um  padrão da web e, no momento está implementado no Chrome, porém é utilizado em outros navegadores através de polyfills do SystemJs.

## Criando os projetos com Single SPA

Existem três tipos de projetos que podem ser criados com single-spa:

* single-spa root config: A aplicação root ou aplicação container, é a responsável por gerenciar como os Micro Frontends do tipo application/parcel/utility serão integrados, por fazer o carregamento das aplicações e roteamento entre as aplicações do tipo application. É um tipo de aplicação que não deve utilixar nenhum framework ou biblioteca de UI (Angular, React, Vue, etc) e possui uma configuração padrão e bem definida.
* single-spa application: É uma aplicação SPA com algumas particularidade, como não possuir um arquivo index.html, tendo sua renderização na arvore DOM controlada pela aplicação container e possuir algum código específico do meta framework Single SPA que permite que a mesma seja registrada no aplicativo container.
* in-browser utility module (Styleguide, Utils, Auth, etc): São aplicacões de apoio e compartilhadas entre todos os Micro frontends do tipo application/parcel. Nesse tipo de aplicação deve ficar toda a definição do guia de estilo compartilhado pelas aplicações, assets globais, API's de uso geral, como autenticação, geranciamento de estado(exceto estado de UI, que deve ser evitado ser compartilhado), etc.

Existe um quarto tipo de aplicação chamado Parcel, porém por se tratar de um recurso avançado e de uso bastante controlado não será coberto. 


### Criando o Root Config

A criação de todos os tipos de aplicação se da pelo mesmo comando.


```sh
create-single-spa
```
Uma série de perguntas serão feitas a partir desse momento para determinar as configurações da aplicação, para uma aplicação Root Config, as respostas devem ser as seguintes:

```sh
Directory for new project (.): [Nome do diretorio da aplicação ser criada]
Select type to generate: single-spa root config
Which package manager do you want to use?: npm
Will this project use Typescript? (y/N):    y
Would you like to use single-spa Layout Engine (Y/n): y
Organization name (can use letters, numbers, dash or underscore): cef

```

Após isso, a aplicação será criada, configurada e já estará funcional, porém sem senhum Micro Frontend registrado.


### Criando uma Application

```sh
create-single-spa
```
Uma série de perguntas serão feitas a partir desse momento para determinar as configurações da aplicação, para uma aplicação Application, as respostas devem ser as seguintes:

```sh
Directory for new project:  [Nome do diretorio da aplicação ser criada]
Select type to generate: single-spa application / parcel
Which framework do you want to use?: angular
Project name (can use letters, numbers, dash or underscore):  [Nome da aplicação ser criada]
Would you like to add Angular routing?: Yes
Which stylesheet format would you like to use?: SCSS

```

Após o termino da instalação das depêndencias, a ferramenta perguntará se deseja instalar e executar o pacote single-spa-angular. Esse pacote vai criar as configurações automáticas para o registro de aplicação no Root Config.

```sh
The package single-spa-angular@5.0.2 will be installed and executed.
Would you like to proceed? (Y/n): y
```
Após a instalação do pacote, aceite a configuração para usar o Angular routing e selecione a porta de execução da aplicação.

```sh
Does your application use Angular routing? Yes
What port should your project run on? 4201: 9999
```

A aplicação está criada, porém antes de registrá-la no Root Config é necessário realizar alguns passos de forma manual.

- Execute o comando '''npm i''' para a aplicação pegar as referências da biblioteca single-spa.
- Adicione um __'APP_BASE_HREF'__ e uma rota padrão para o componente criado automaticamente 'EmptyRouteComponent' no app.routing.module.ts para que o Root Config consiga rotear para a aplicação criada de forma consistente.
```typescript
import { APP_BASE_HREF } from "@angular/common";
import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { EmptyRouteComponent } from "./empty-route/empty-route.component";

const routes: Routes = [{ path: "**", component: EmptyRouteComponent }];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [{ provide: APP_BASE_HREF, useValue: "/" }],
})
export class AppRoutingModule {}
```

O single-spa-angular cria um script de execução configurado para a aplicação Single-SPA. A execução da aplicação deve ser dar por esse script:
```sh
npm run serve:single-spa:[nome_aplicacao]
```

### Registrando uma aplicação no Root Config

Para que Micro Frontends possam ser registrados e roteados de forma correta pelo Root Config é necessário habilitar o zone.js que é a biblioteca responsável por fazer o Angular saber quando atualizar o DOM. Para habilitá-lo apenas descomente a seguinte linha no arquivo index.ejs no Root Config.

```html
<script src="https://cdn.jsdelivr.net/npm/zone.js@0.11.3/dist/zone.min.js"></script>
```

Adicione a aplicação no mapa de importação do Root Config

```html
 <script type="systemjs-importmap">
    {
      "imports": {
        "@cef/app-name": "http(s)//[servidor]:[porta]/main.js"
      }
    }
  </script>
```

Sempre deve ser importado o arquivo main.js publicado pelas aplicações.

Adicione o álias da aplicação criado pelo mapa de importação ao bloco de importações do SystemJS

```html
<script>
    System.import('@cef/root-config');
</script>
```
No arquivo do Template Engine, associe a aplicação a uma rota, ou a incluia em algum elemento DOM caso ela deva estar sempre ativa.
Abaixo temos um exemplo do arquivo microfrontend-layout.html

```html
<single-spa-router>
  <application name="@cef/header"></application>

  <div class="container-fluid" >
 
    <route path="/app1">
      <application name="@cef/app1"></application>
    </route>

    <route path="/app2">
    <application name="@cef/app2"></application>
    </route>

    <route path="/app3">
      <application name="@cef/app3"></application>
    </route>

    <route default>
      <div class="jumbotron" >
        <h1 class="display">Microfrontends!</h1>
        <p class="lead">Trecho de código renderizado quando nenhuma das rotas for carregada</p>
      </div>
    </route>

  </div>
</single-spa-router>
```

Ao iniciar ambas aplicações, e acessar a aplicação Root Config, é possível ver os Micro Frontends rodando e navegar entre as aplicações pela barra de endereços.


## Histórico da revisão

Data       | Versão | Descrição                                 | Autor
-----------|--------|-------------------------------------------|-------
02/11/2021 | 1      | Criação do documento. | Wanderson Freitas Daltro



