# Micro frontends

>  **Importante:** Essa arquitetura de referência se encontra em fase de validação e não possui nenhum indicativo de uso geral pelas equipes. 
Havendo entendimento que a mesma se encaixa nas necessidades de seu sistema, a SUART deve ser acionada.
 

## Visão Geral

O conceito de micro aplicações no frontend traz uma abordagem que divide uma
grande aplicação em outras diversas aplicações menores, com times dedicados, permitindo a
realização de entregas de novos recursos e atualizações de forma independente, dando assim
mais autonomia para os desenvolvedores, gerando mais valor e otimizando a entrega do
produto ou serviço atendidos pelo sistema.

!["Monolito X Microfrontends"](../img/frontend/mono-micro.png)



## Requisitos de um micro frontend:

* Deve ser um SPA ou Web Component que é executado isoladamente em um nó DOM arbitrário;
* Não deve instalar bibliotecas globais, fontes ou estilos. Deve herdar os estilos compartilhados e instalar apenas os estilos estritamente necessários para a aplicação;
* Não deve ter nenhum conhecimento sobre a aplicação Root em que é incorporado;
* Deve poder ser instanciado e parametrizado para que possa existir diversas vezes na mesma página, com configurações diferentes;
* A experiência do usuário deve ser totalmente transparente entre as micro aplicações.


## Benefícios dos micro frontends: 
* Agnóstico de tecnologia: cada micro frontend pode ser desenvolvido utilizando 
* Orientado a negócio: o código de cada serviço deve ser isolado e auto-contido, tendo o seu escopo definido baseando-se no negócio.
* Funcionalidades autônomas: cada micro aplicação deve ser capaz de executar as suas responsabilidades de maneira independente.
* Repositórios de código desacoplados e simplificados.
* Deploys independentes.

## Frameworks

Como forma de trazer padronização e , os seguintes frameworks devem ser utilizados para a criação de Micro Frontends:

###  Single SPA

De acordo com a documentação oficial Single-SPA:

O Single-SPA é um framework para reunir vários microfrontends JavaScript em uma única aplicação frontend, tendo em vista auxiliar em atividades tais como:

* Utilizar diferentes frameworks no frontend, como ReactJS, Angular, Vuejs entre outros.
* Implantar os microfrontends de forma independente.
* Melhorar o tempo de carregamento inicial.

Podemos considerá-lo um meta framework  que permite mesclar diversos tipos de framewors/libs Web em uma única aplicação. Por exemplo, conseguimos desenvolver o header de uma página em React, o corpo da página usando Angular e desenvolver o footer com Javascript/HTML puro.

O Single SPA é composto por um único arquivo HTML(EJS) que é a porta de entrada da aplicação. Esse arquivo importa e registra as aplicações microfrontends através de um javascript que segue uma interface padrão(toda aplicação micro frontend disponibiliza esse arquivo). 

O Single SPA então carrega as aplicações micro frontends sob demanda, ou seja, o framework possui uma camada que realiza o lazy load das aplicações.  

O Single SPA possui libs desenvolvidas nos principais frameworks e bibliotecas JavaScript, porém seguindo a [arquitetura de referência de aplicações SPA](./spa.md) somente é permitido a utilização do framework [Angular](http://www.angular.io) para a criação das micro aplicações.

###  Angular

Conforme definido na [arquitetura de referência de aplicações SPA](./spa.md), todas as aplicações Micro Frontend do tipo Application devem ser desenvolvidas utilizando o framework [Angular](http://www.angular.io)


## Estrutura da aplicação

Um aplicativo Single SPA consiste de:

* Root Config: Responsável por renderizar a página HTML da aplicação e carregar e registrar os aplicativos microfrontends Single-SPA. Deve ser criado em Javascript/Typescript puro, se mantndo agnóstico a tecnologias. Cada aplicativo registrado deve possui três informações:
    -  Um nome
    -  Uma função para carregar o código do aplicativo
    -  Uma função que determina quando o aplicativo está ativo / inativo

*  Micro Frontends de Aplicações: Aplicativos SPA ou Web Components customizados com o Single-SPA que devem saber como inicializar, montar e desmontar a si mesmo do DOM. A principal diferença entre um SPA normal e os  Single-SPA é que eles devem ser capazes de coexistir com outros aplicativos, já que não possuem sua própria pagina HTML, coexistindo na página do aplicativo hospedeiro(Root Config).

*  Módulos utilitários: São usados quando precisamos compartilhar uma api em comum com vários microfrontends, por exemplo, ou um design system comum para todas as aplicações, serviços de auteticação, etc. Esse tipo de módulo não é um Micro Frontend, seu único propósito é exportar funcionalidades para uso em outros microfrontends. O Single SPA não suporta a criação de utilitários em Angular, dessa forma esses módulos devem ser escritos em Javascript/Typescript puro.

## Segurança

Os Micro Frontend devem ser aderentes à Arquitetura de Referência de Segurança da mesma forma que as aplicações SPA comums, isso significa que o usuário deve ser autenticado no SISET(Keycloak) para obtenção de um token JWT que deve ser utilizado em todas as requisições.

Porém por se tratar de um tipo especial de aplicação, que é o resultado da cooexistencia de várias aplicações SPA em um mesmo HOST, algumas :

* Todas as aplicações Micro Frontend devem compartilhar o mesmo token.
* Todo o código que trata de autenticação e autorização deve estar em um Micro Frontend utilitário que é compartilhado por todos os Micro Frontends de Aplicação.
* Interceptadores para inclusão do header 'Authorization' e guardas de rotas ainda devem existir nos Micro Frontends.

Como forma padrão para integração com o SSO, deve ser utilizado o [adaptador javascript do keycloak](https://github.com/keycloak/keycloak-documentation/blob/main/securing_apps/topics/oidc/javascript-adapter.adoc), esse adaptador abstrai algumas complexidades da implementação de uma integração com o SSO.


## Design System

No momento , a arquitetura não define um design system padrão para as aplicações SPA, deixando a cargo das equipes de desenvolvimento essa tarefa, porém é recomendada o estudo da viabilidade de uso do [Design System Caixa](http://design.caixa).

Para a construção do design system de e/ou componentes customizados, devem ser utilizadas as seguintes tecnologias:

#### Bootstrap

A parte visual do front-end deve ser criada com Bootstrap, uma biblioteca para criação de front-end web responsivo e mobile-first, que trás como principal benefício a compatibilidade com grande parte dos navegadores, acelerando o desenvolvimento e reduzindo a quantidade de bugs causados pelas diferenças entre os navegadores e suas versões.

#### CSS / SCSS 

Todos os estilos das aplicações devem ser criados utilizando CSS ou preferencialmente SCSS que é uma extensão do CSS padrão adicionam funcionalidades extras ao CSS.


## Bibliotecas Compartilhadas

Por questões de desempenho e compatibilidade entre as aplicações, é crucial que seu aplicativo da web carregue grandes bibliotecas JavaScript apenas uma vez. Por exemplo, todas as dependencias do Angular devem ser carregadas apenas uma vez pela aplicação Root Config e utilizadas por todos os Micro Frontends.

Os Micro Frontends devem carregar apenas bibliotecas que não sejam utilizadas por outras aplicações, e em casos menos comuns, bibliotecas já carregadas pela aplicação Root Config, mas que sejam necessárias em uma versão especifica( nesse caso deve ter o cuidado de não haver confiltos entre as versões de uma mesma biblioteca)



## Histórico da revisão

Data       | Versão | Descrição                                 | Autor
-----------|--------|-------------------------------------------|-------
29/10/2021 | 1      | Criação da Arquitetura de Referência. | Wanderson Freitas Daltro
02/11/2021 | 2      | Inclusão da seção de bibiotecas compartilhadas | Wanderson Freitas Daltro
09/11/2021 | 3      | Inclusão do modelo de autenticação. | Wanderson Freitas Daltro
16/11/2021 | 4      | Revisão e inclusão de aviso de versão em validação . | Wanderson Freitas Daltro



