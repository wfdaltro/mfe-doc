## Arquitetura de Solução
 

A arquitetura apresentada tem como objetivo definir um arcabouço inicial para o desenvolvimento de aplicações SPA, como forma de padronizar a entrega de soluções e tornar homogênio o conhecimento das equipes.

Dessa forma, fica definido que as aplicações SPA devem ser implementadas utilizando o framework Angular e seu ecossistema.

Qualquer necessidade de utilização de outras tecnologias deve ser apresentada e discutida junto a SUART.


## Estrutura da aplicação

!["Estrutura dos módulos de uma aplicação angular"](../img/frontend/visao_componentes_spa.png)

Toda aplicação Angular deve ter duas partes principais:

- A parte Eager: Carregada pelo bundle ``` main.js```. Deve conter o AppModule e o CoreModule.
- A parte Lazy:  Carregada sob demanda como resultado da navegação do usuário para uma rota específica. Todas as funcionalidades da aplicação devem ser definadas como Lazy, assim como o módulo Shared.

### Módulos

#### Core

O módulo Core deve conter serviços que necessitam de apenas uma instância. Autenticação e Interceptors são exemplos de componentes que terão apenas uma instância para a aplicação e que serão utilizados por praticamente todos os módulos.

#### Feature

Os módulos  features (lazy load) visam diminuir o tempo de inicialização da aplicação. Com o lazy load, o aplicativo não precisa carregar tudo de uma só vez. Ele só vai carregar o que o usuário precisar quando ocorrer a navegação para sua rota.

Para módulos Features que tiverem mais de um componente,  sub-pastas para a cada componentedevem ser criadas para que nenhuma pasta tenha mais de 6 arquivos conforme a [recomendação](https://angular.io/guide/styleguide#flat) do style guide do Angular.

 
#### Shared

O shared é onde todos os componentes compartilhados, pipes, filters e services devem ir. O shared pode ser importado em qualquer módulo. 
Ele deve ser independente do restante do aplicativo, portanto, não deve ter nenhuma referências a outros módulos.


### Organização de Pastas

Como forma de padronizar as aplicações e melhorar a manutenibilidade/legibilidade do código, a seguinte organização de pastas é preferida.

```bash
|-- app.module.ts
|-- app-routing.module.ts
|-- core
    |-- [+] auth
    |-- [+] guards
    |-- [+] http
    |-- [+] interceptors
    |-- [+] services
    |-- [+] layout
|-- features
    |-- feature1
        |-- feature1-routing.module.ts
        |-- feature1.component.html
        |-- feature1.component.scss
        |-- feature1.component.ts
        |-- feature1.component.spec.ts
        |-- feature1.service.ts
        |-- feature1.module.ts
    |-- feature2 
        |-- [+] components
        |-- [+] models
        |-- feature2-routing.module.ts
        |-- feature2.service.ts
        |-- feature2.module.ts
|-- shared
    |-- [+] components
    |-- [+] models
    |-- [+] directives
    |-- [+] pipes
|-- app
    |-- app.component.html
    |-- app.component.scss
    |-- app.component.ts
    |-- app.component.spec.ts

```


## Segurança

As aplicações SPA devem ser aderentes à Arquitetura de Referência de Segurança, isso significa que o usuário deve ser autenticado no SISET(Keycloak) para obtenção de um token JWT que deve ser utilizado em todas as requisições.  

Como forma padrão para integração com o SSO, deve ser utilizada a biblioteca [keycloak-angular](https://www.npmjs.com/package/keycloak-angular), essa biblioteca abstrai algumas complexidades do [adaptador padrão do keycloak para javascript](https://www.keycloak.org/docs/latest/securing_apps/index.html#_javascript_adapter), fornecendo implementações já prontas para alguma necessidades, como:

* Um interceptador que adiciona o heder de autorização em todas as solicitações Http.
* Uma Implementação de AuthGuard para verificação de autorização e autenticação pelas rotas.
* Um Wrapper(Keycloak Service) que facilita a consumo das funcionalidades do adaptador Keycloak pelo Angular.

Toda a configuração necessária para uso da biblioteca é descrita no [site do projeto](https://www.npmjs.com/package/keycloak-angular), porém no Quickstart disponibilizado, é entregue toda a estrutura para uso da biblioteca, sendo necessário apenas incluir no arquivo ````environment.ts``` as configurações para conexão com o servidor SSO, como mostrado abaixo:

```typescript
export const environment = {
  ssoConfig: {
    url: 'http://server:port/auth',
    realm: 'realm',
    clientId: 'client-id',
  }
};

```



## Design System

No momento , a arquitetura não define um design system padrão para as aplicações SPA, deixando a cargo das equipes de desenvolvimento essa tarefa, porém é recomendada o estudo da viabilidade de uso do [Design System Caixa](http://design.caixa)
Para a construção do design system de e/ou componentes customizados, devem ser utilizadas as seguintes tecnologias:

#### Bootstrap

A parte visual do front-end deve ser criada com Bootstrap, uma biblioteca para criação de front-end web responsivo e mobile-first, que trás como principal benefício a compatibilidade com grande parte dos navegadores, acelerando o desenvolvimento e reduzindo a quantidade de bugs causados pelas diferenças entre os navegadores e suas versões.

#### CSS / SCSS 

Todos os estilos das aplicações devem ser criados utilizando CSS ou preferencialmente SCSS que é uma extensão do CSS padrão adicionam funcionalidades extras ao CSS.


## Backend for Frontend

Por padrão as API REST para uso de múltiplos clientes tendem a ser genéricas e não fazer suposições sobre como os dados serão usados. Quando um canal precisa acessar recursos de vários serviços acaba sendo penalizado por um número excessivo de requisições HTTP que precisam ser tratadas e combinadas na camada do cliente.

Não é factível que cada serviço exponha múltiplos serviços com pequenas diferenças entre si para se adequar às necessidades de diferentes clientes.

Para contornar essas questões surgiu o padrão "Back-end for Front-end", também chamado de BFF. Nesse padrão, uma aplicação back-end atua como complemento do front-end e é encarregada de expor um endpoint exclusivo e altamente especializado, realizando as requisições para os múltiplos serviços e combinando os payloads em formato otimizado para consumo daquele canal específico.

Devido ao alto grau de acoplamento desse back-end com a camada de front-end do canal, as vezes diz-se que ele "faz parte da camada view".


## Quickstart

Um projeto de Quickstart contendo todo o código boilerplate assim como implementações padrões para algumas necessidades como autenticação, encontra-se disponível no seguinte repositório.

[Quickstart Single Page Application](http://fontes.caixa/suart)


## Referências

A estrutura é inspirada na seguinte fonte:

[Application structure and NgModules - Angular coding style guide](https://angular.io/guide/styleguide#application-structure-and-ngmodules)

## Histórico da revisão

Data       | Versão | Descrição                                 | Autor
-----------|--------|-------------------------------------------|-------
29/10/2021 | 1      | Criação da Arquitetura de Referência. | Wanderson Freitas Daltro