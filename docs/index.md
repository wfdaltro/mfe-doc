# Introdução

As Arquiteturas de Referência (ARF) padronizam as tecnologias e padrões
de projeto para as aplicações da Caixa.

## Navegação

À esquerda estão listadas todas as arquiteturas de referência publicadas
atualmente pela SUART, sempre em suas últimas versões. Ao acessar cada
uma, uma lista com os tópicos definidos nela estará disponível à direita
para facilitar a navegação.

## As definições das ARFs não me atende. O que fazer?

As arquiteturas de referência buscam mapear os cenários mais comuns, bem
como os casos especiais encontrados ao longo do tempo. Caso esteja
diante de um cenário não contemplado, procure o grupo de arquitetos da
sua comunidade para que uma solução seja definida e documentada para uso
futuro.
