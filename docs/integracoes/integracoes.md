# Integrações


## Modalidades de Integração

![Diagrama que representa os cenários de integração.](../img/integracoes/formas_integracao.svg)


### API REST

A integração síncrona entre aplicativos de plataforma baixa deverá ocorrer por meio de APIs RESTful, cujos padrões e boas práticas para o desenvolvimento, bem como processo de governança em vigor estão disponíveis em [https://portalapi.caixa/](https://portalapi.caixa/).

_**Observação:** Quando houver indicação de reuso ou a necessidade de exposição externa das APIs, elas deverão ser publicadas no API Manager._

_**Importante:** A necessidade de publicação no API Manager nos demais casos deverão ser avaliadas pela área detentora do mandato de Governança de APIs._

Definições de como desenvolver APIs REST para aplicações CICS estão descritas no item [Desenvolvimento de APIs a partir na Plataforma Alta](#desenvolvimento-de-apis-a-partir-na-plataforma-alta).


### Barramento (IIB ou Broker)

O uso do barramento (IBM Integration Bus/Broker) permite o desenvolvimento de serviços de diversos tipos (APIs REST, Web Services SOAP, MQ etc.).

Seu uso é indicado para os casos onde o serviço a ser desenvolvido necessita de orquestração de dois ou mais sistemas, transformação de mensagens e a utilização múltiplos protocolos.

_**Importante:** Atualmente, o uso do barramento está restrito à orquestração de sistemas na plataforma alta._


### CICS-to-CICS

Chamadas CICS-to-CICS ocorrem na plataforma alta e devem e são usadas nos casos em que transações de sistema executado em um CICS TS necessitam executar transações de outro CICS TS.

_**Observação:** Com o objetivo de reduzir o acoplamento entre as aplicações, a comunicação CICS-to-CICS será realizada, preferencialmente, por `START`._

_**Importante:** Deve ser evitado o desenvolvimento, na plataforma alta, de "sistemas portais" cuja única função seja orquestrar transações de outros sistemas de negócio._


### MQ (Message Queue)

O uso do IBM MQ deve ocorrer somente para serviços que necessitam de comunicação assíncrona, como por exemplo, em serviços de log e monitoração (mão única) ou aplicações orientadas a eventos.

_**Importante:** Deve ser evitado o uso do MQ em comunicações "pseudosíncronas", onde a aplicação cliente posta uma mensagem em uma fila de requisição (`PUT`) e fica esperando a resposta em outra fila (`GET WAIT`)._


### ETL

Para os processos que envolvam extração, transformação e carga de dados de forma automatizada, a ferramenta Informatica PowerCenter deverá ser utilizada.


### Integração _Batch_ (Troca de Arquivos)

![Diagrama mostrando troca de arquivos via compartilhamento NFS entre Plataforma Alta e Plataforma Intermediária. Arquivos da Internet são trocados via B2B (Plataformas Alta e Distribuída) ou SISDU (somente Plataforma Distribuída), dependendo do caso.](../img/integracoes/integracao_arquivos.svg)


#### Entre Sistemas Corporativos

A troca de arquivos entre sistemas ocorre para processamento massivo de informações, quando não há a possibilidade da integração online.

Entre Sistemas de Plataforma Alta, quando na mesma partição, essa troca ocorre diretamente entre os sistemas.

Entre Sistemas de Plataforma Alta em diferentes partições ou entre Sistemas de Plataforma Alta e Baixa, a troca ocorre por meio de NFS (Network File System).

_**Importante:** O NFS não fornece_ lock _para arquivos. Para integrações envolvendo file watch, o arquivo deve ser copiado para o NFS com um nome intermediário e renomeado após a transferência._


#### Entre Sistema Corporativo e Sistema Externo

Trocas de arquivos com clientes/parceiros externos serão realizados com a utilização do B2B.

_**Observação:** O B2B pode ser integrado diretamente com a aplicação dos clientes/parceiros ou com alguma aplicação Caixa que faça a interface, tal como o Internet Banking._


## Desenvolvimento de APIs a partir na Plataforma Alta

![Diagrama das formas de exposição de API REST para aplicação CICS.](../img/integracoes/integracao_java_cics.svg)

Para publicar APIs REST de aplicações CICS deve-se usar um dos cenários descritos a seguir.

_**Observação:** Estes cenários são alternativas ao uso do barramento para o desenvolvimento de APIs a partir da plataforma alta em casos onde não existe a necessidade de orquestração de dois ou mais sistemas._

_**Importante:** Nos dois cenários, a camada desenvolvida em Java e a aplicação Cobol/CICS devem estar, preferencialmente, na mesma sigla._


### Cenário 1: CICS Liberty

Nesse cenário, o desenvolvimento de serviços padrão RESTful ocorrerá a partir do CICS, utilizando aplicação Java desenvolvida com os frameworks JEE/JakartaEE 8 ou MicroProfile e executadas no servidor de aplicação CICS Liberty.

A aplicação Java executar transações CICS por meio de link, utilizando as bibliotecas disponibilizadas pela IBM para converter objetos do Java em `CONTAINER` e vice-versa ao produzir a resposta.

_**Importante:** A validação do token de usuário e autorização poderão ser realizados junto ao SISET tanto pela camada Java quanto pelo CICS._


### Cenário 2: JBoss ou MicroProfile

Esse cenário requer que as regras de negócio estejam na Plataforma Alta.

As APIs são desenvolvidas a partir de uma aplicação Java desenvolvida com os frameworks JEE/JakartaEE 8 e executadas no servidor de aplicação JBoss.

Alternativamente, a aplicação Java pode ser desenvolvida utilizando uma das especificações de MicroProfile homologadas para deploy no ambiente de contêineres.

A aplicação Java se comunica com a aplicação CICS por meio Web Services desenvolvidos por meio do CICS Web Support ou IBM MQ.

_**Importante:** As interfaces disponibilizadas na camada do CICS são de uso exclusivo da camada Java que proverá o serviço e não devem ser reutilizadas por outras aplicações._

_**Observação:** Em ambos os cenários, a autorização dos serviços deve ser implementada na camada Java, atualmente por meio do protocolo oAuth 2, e a comunicação entre a camada Java e o CICS será realizada por usuário de serviço._

## Referências

 * TE111 – Padrões Arquiteturais para o Desenvolvimento de Aplicativos
 * TE197 – Segurança para o Desenvolvimento e Manutenção de Sistemas
 * TE202 – Serviços de Diretórios de Usuários para Sistemas e Aplicativos
 * TE232 – Diretrizes de Segurança para Utilização de API
 * [Portal API](https://portalapi.caixa/)
 * [Portal de Desenvolvedores](https://desenvolvedores.caixa)
 * [PPDS - Projeto Padrão para Desenvolvimento de Software](http://ppds.caixa)


## Glossário

Termo       | Definição
------------|-----------------------------------------------------------
API         | _Application Program Interface_ – Serviços de um aplicativo, sistema, produto disponíveis para programas que funcionam sob esse componente
API Manager | Solução que fornece uma camada de gestão para APIs. Na CAIXA, a ferramenta utilizada é o CA API Management.
B2B         | Gateway para integração entre diferentes ambientes, com opções de caixa de correio global para distribuição geográfica e replicação de dados/arquivos em tempo real.
Barramento  | Solução para integração e orquestração de diferentes serviços, podendo utilizar diferentes protocolos. Na CAIXA, a ferramenta utilizada é o IBM Integration BUS.
CICS        | _Customer Information Control System_ – Software de processamento de transações
MQ          | Protocolo de comunicação assíncrono baseado em mensageria.
REST        | _Representational State Transfer_ é a abstração da arquitetura WWW, que objetiva a definição de características fundamentais para construção de aplicações Web no contexto das melhores práticas.
SISET       | Sistema de Segurança Tecnológica – tem como objetivo implementar autenticação única dos usuários internos/externos com múltiplo fator de autenticação


## Histórico da Revisão

Data       | Versão | Descrição                       | Autor
-----------|--------|---------------------------------|---------
06/06/2020 | 1.0    | Publicação.                     | SUART06
29/01/2021 | 1.1    | Retirada do SISDU do Glossário. | SUART10
