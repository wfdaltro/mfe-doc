# Web

Aqui estão definidos os padrões e tecnologias a serem utilizados para o desenvolvimento de front-end Web para a Caixa.

_**Importante:** Essa arquitetura de referência não se aplica a_ hot sites _de campanhas nem ao [site da Caixa](http://www.caixa.gov.br/Paginas/home-caixa.aspx)._


## Arquitetura de Referência

![Diagrama da arquitetura de referência para web.](../img/web/arquitetura.svg)

A camada cliente (font-end) web para aplicações corporativas, empresariais e funcionais deve seguir o modelo _single-page application_ (SPA), ou seja, composta por um _website_ único cujo conteúdo é atualizado dinamicamente com novos dados recebidos do servidor.

As requisições de novos dados devem utilizar o protocolo [HTTPS](https://en.wikipedia.org/wiki/HTTPS) para garantir que a comunicação entre cliente e servidor seja privada, que as partes são quem afirmam ser e que os dados não foram corrompidos durante a transmissão. A criptografia da comunicação deve utilizar o protocolo TLS 1.2 ([RFC 5246](https://tools.ietf.org/html/rfc5246)) ou 1.3 ([RFC 8846](https://tools.ietf.org/html/rfc8446)) uma vez que as versões 1.0 e 1.1 foram descontinuadas nos principais navegadores no começo de 2020.

A comunicação entre cliente e servidor deve ser RESTful, ou seja, aderir ao padrão [REST (Representational State Transfer)](https://en.wikipedia.org/wiki/Representational_state_transfer), cujas características estão resumidas a seguir e cujo padrão a ser adotado na Caixa está detalhes no [Portal de APIs](https://portalapi.caixa/).

*  *Arquitetura Cliente/servidor:* Isso significa que *cliente e servidor precisam ser capazes de evoluir separadamente*, sem qualquer dependência entre eles. O único pré-requisito para o cliente interagir com o servidor é conhecer as [URIs](https://en.wikipedia.org/wiki/Uniform_Resource_Identifier) dos recursos.

*  *_Statelessness_:* O servidor não deve guardar qualquer contexto entre requisições. Toda requisição deve _stateless_ (conter todos os dados necessários para que o servidor possa respondê-la) e o estado da sessão é mantido no cliente.

*  *_Cacheable_:* Todos os recursos que puderem ser guardados em [cache](https://en.wikipedia.org/wiki/Cache_(computing)) devem se declarar como tal. Isso objetiva melhorar a performance no lado do cliente e otimizar a capacidade do servidor por causa da redução de requisições que precisam ser respondidas.

    Um recurso informa que pode ser guardado em cache por meio da diretiva `Cache-Control` do cabeçalho HTTP. Nesse caso o recurso pode ser guardado tanto no cache da camada servidor (Apache, Nginx etc.), no cache da camada cliente (navegador web) ou no cache de qualquer intermediário.

*  *Sistema em camadas:* O cliente não deve ser capaz de determinar se está conectado diretamente com o servidor final ou com um intermediário. Servidores intermediários podem adicionar funcionalidades e recursos, como por exemplo, balanceamento de carga, cache compartilhado (Data Grid) ou segurança (SISET). Concluindo, múltiplos servidores podem ser acionados para gerar a resposta da requisição.

*  *Código sob demanda (opcional):* O servidor pode estender ou alterar a funcionalidade do cliente ao transferir código executável como, por exemplo, um applet Java ou script Javascript.

*  *Interface uniforme:* Todos os recursos devem ser acessíveis seguindo o formato de API padronizado. Essa característica desacopla cliente e servidor, permitindo que evoluam separadamente. As 4 restrições dessa característica são:

   *  *Identificação do recurso na requisição:* Isso significa que cada recurso deve ter uma única URI lógica. Por exemplo, um contrato de empréstimo consignado pode ter a URI abaixo, identificando claramente que tipo de recurso está sendo manipulado.

        http://api.caixa/emprestimo/consignado/{numero_do_contrato}

   *  *Manipulação de recurso por meio de representação:* Quando o cliente tem uma representação [JSON](https://www.json.org/json-pt.html) do recurso, tem informação suficiente para modificar ou excluir o recurso.

   *  *Mensagem auto-descritiva:* Cada mensagem deve ter informação que descreva como processá-la, por exemplo, usando a informação [media type](https://en.wikipedia.org/wiki/Media_type) para processar o conteúdo corretamente.

   *  *Hypermedia as the engine of application state:* A representação do recurso deve conter links apontando para ações e outros recursos relacionados a ele, de forma que o cliente não precise guardar informações _"hard coded"_ sobre a estrutura e dinâmica do sistema com que está interagindo.


### Padrões tecnológicos

![Visão lógica da arquitetura para front-end web.](../img/web/logica.svg)


#### Navegadores

As aplicações corporativas devem ser compatíveis com os navegadores Google Chrome (últimas 2 versões) e Mozilla Firefox ESR (última versão).

As aplicações destinadas a clientes e parceiros devem ser compatíveis com os navegadores relacionados pelo gestor de negócio. Essa definição deve levar em conta o perfil do público usuário da aplicação. Por exemplo, uma aplicação web pode ser compatível com uma versão antiga do navegador web do Android caso o público alvo seja de baixa renda e a acesse prioritariamente pelo telefone celular. Outro exemplo: um parceiro pode solicitar a compatibilidade com uma versão mais antiga do Firefox, Chrome ou outro navegador.

Em caso de indefinição do gestor de negócio, recomenda-se que a aplicação destinada a públicos externos seja compatível com os navegadores suportados pelo Bootstrap 4.


#### HTML5

O HTML5 agrupa a nova versão da linguagem HTML, com novos elementos, atributos e comportamentos, e um conjunto de tecnologias que permitem construir sites e aplicativos web mais diversos e poderosos.

Na camada semântica, novos elementos permitem descrever mais precisamente o conteúdo, como, por exemplo `<section>`, `<article>`, `<nav>`, `<header>`, `<footer>` e `<aside>`. Há também os elementos `<video>` e `<audio>` para inserir e manipular esse tipo de conteúdo multimídia. Finalmente, uma API mais potente de validação de valores inseridos em formulários está disponível.

Novos recursos de conectividade incluem Web Sockets, a capacidade de o cliente "escutar" eventos enviados do servidor e WebRTC para comunicação em tempo real.

A capacidade de persistir dados localmente no cliente e operar off-line foram aprimoradas, assim como acesso a vários dispositivos e recursos locais, como câmera, aceleração de hardware, placa de vídeo, geolocalização, orientação do dispositivo e capturar eventos de toque.


#### Bootstrap 4

A parte visual do front-end deve ser criada com [Bootstrap](https://getbootstrap.com/), uma biblioteca para criação de front-end web responsivo e _mobile-first_, que trás como principal benefício a compatibilidade com grande parte dos navegadores, acelerando o desenvolvimento e reduzindo a quantidade de bugs causados pelas diferenças entre os navegadores e suas versões.

Os navegadores com os quais o Bootstrap 4 é compatível são:

*  Chrome >= 45
*  Firefox >= 38
*  Edge >= 12
*  Explorer >= 10
*  iOS >= 9
*  Safari >= 9
*  Android >= 4.4
*  Opera >= 30

_**Observação:** Para o Firefox, também é suportada a última versão [Extended Support Release (ESR)](https://support.mozilla.org/pt-BR/kb/ciclo-de-lancamento-do-firefox-esr), usada pela Caixa internamente._


#### Angular 9.x ou superior

A parte dinâmica do front-end web deve ser implementada com o framework de código aberto [Angular](https://angular.io/), desenvolvido pelo Google com colaboração da Microsoft.

O framework favorece o desenvolvimento modular da aplicação cliente, no qual cada módulo é composto por um conjunto de componentes dedicado a um domínio de negócio, workflow ou um conjunto de capacidades relacionadas.

Cada componente define uma classe que contém dados e lógica da aplicação, e é associado a um _template_ HTML que define uma _view_ para ser mostrada no navegador. O _template_ combina HTML com marcação do Angular e é renderizado no cliente antes de o DOM ser atualizado.

Finalmente, serviços são classes que encapsulam funcionalidade e dados que não são específicos de uma _view_ e que podem ser compartilhados por vários componentes.

![Diagrama dos elementos que compõem o Angular](../img/web/arquitetura_angular.png)

_**Importante:** Não utilizar o [AngularJS](https://angularjs.org/), a primeira versão do_ framework _que é incompatível com as versões mais recentes._


#### TypeScript

O desenvolvimento com o framework Angular usa a linguagem de programação [TypeScript](https://www.typescriptlang.org/), desenvolvida pela Microsoft, que adiciona tipos, interfaces, classes abstratas e outros recursos que não existem no Javascript. Esse recursos permitem que o compilador identifique erros em tempo de compilação, ao contrário do Javascript, no qual a maior parte dos erros só é observada em tempo de execução.

_**Importante:** Deve-se usar exclusivamente o TypeScript instalado e configurado pelo Angular para evitar incompatibilidades._


## Segurança

Os front-ends web, bem como as APIs REST com as quais interagem, deve ser aderentes à Arquitetura de Referência de Segurança. De forma simplificada, isso significa que o usuário deve ser autenticado no SISET para obtenção de um _token_ [JWT](https://jwt.io/) que deve ser utilizado em todas as requisições.


## Back-end for Front-end (BFF)

Por padrão as API REST para uso de múltiplos clientes tendem a ser genéricas e não fazer suposições sobre como os dados serão usados. Quando um canal precisa acessar recursos de vários serviços acaba sendo penalizado por um número excessivo de requisições HTTP que precisam ser tratadas e combinadas na camada do cliente.

Não é factível que cada serviço exponha múltiplos serviços com pequenas diferenças entre si para se adequar às necessidades de diferentes clientes.

Para contornar essas questões surgiu o padrão "Back-end for Front-end", também chamado de BFF. Nesse padrão, uma aplicação back-end atua como complemento do front-end e é encarregada de expor um _endpoint_ exclusivo e altamente especializado, realizando as requisições para os múltiplos serviços e combinando os _payloads_ em formato otimizado para consumo daquele canal específico.

Devido ao alto grau de acoplamento desse back-end com a camada de front-end do canal, as vezes diz-se que ele "faz parte da camada _view_".

![Diagrama do padrão BFF (retirado do artigo https://philcalcado.com/2015/09/18/the_back_end_for_front_end_pattern_bff.html).](../img/web/sc-bff-2.png)

_**Observação:** A descrição desse padrão geralmente não discorre sobre questões de segurança, mas uma vantagem observada é que as_ API Keys _do canal ficam guardadas no BFF ao invés de expostas no front-end._


## Glossário

Termo       | Definição
------------|-----------------------------------------------------------
API         | A API é um conjunto de definições e protocolos usado no desenvolvimento e na integração de software de aplicações. API é um acrônimo em inglês que significa interface de programação de aplicações.
JSON        | Acrônimo para _JavaScript Object Notation_), é um formato leve para troca de dados, de fácil leitura para seres humanos e de fácil interpretação e geração para máquinas. Está baseado em um subconjunto da linguagem de programação JavaScript, Padrão ECMA-262 3ª Edição de Dezembro/1999. Para mais informações consulte [https://www.json.org/json-pt.html](https://www.json.org/json-pt.html).
JWT         | Acrônimo para _JSON Web Token_, é um padrão aberto ([RFC 7519](https://tools.ietf.org/html/rfc7519)) que define um meio compacto e auto-contido para transmissão segura de informação entre entidades como um objeto JSON. Essa informação é confiável e pode ser verificada porque é assinada digitalmente. JWT pode ser assinado usando um segredo (com o algoritmo HMAC) ou um par de chaves pública/privada usando RSA ou ECDSA. Para mais informações consulte [https://jwt.io/](https://jwt.io/).
HTTPS       | Acrônimo para _Hyper Text Transfer Protocol Secure, é uma implementação do protocolo HTTP sobre uma camada adicional de segurança que utiliza o protocolo SSL/TLS. Essa camada adicional permite que os dados sejam transmitidos por meio de uma conexão cifrada e que se verifique a autenticidade do servidor e do cliente por meio de certificados digitais.
Oauth 2     | É um protocolo padrão de mercado para autorização, que provê fluxos de autorização específicos para aplicações web, desktop, mobile e internet das coisas. Para mais informações consulte [https://oauth.net/2/](https://oauth.net/2/).
REST        | Do inglês _Representational State Transfer_, se refere ao conjunto de características de design de arquitetura de software que adicionam eficiência, confiabilidade e escalabilidade a sistemas distribuídos. Um sistemas é considerado _RESTful_ quando adere a essas características.
SSO         | Acrônimo para _Single Sign On_, provê aos usuários uma experiência de autenticação fluida, na qual, uma vez que o usuário se autentica numa aplicação, não precisa fornecer credenciais novamente ao entra numa outra aplicação desde que ambas confiem no mesmo provedor de identidade. Na Caixa o provedor de identidade corporativo é o SISET.
Stateless   | Se refere a qualquer protocolo no qual o servidor não guarda qualquer dado de sessão (estado) entre requisições.
Web Sockets | É um protocolo que permite a conexão TCP persistente entre servidor e cliente de forma que eles possam trocar informações a qualquer momento. Através deles o servidor pode enviar dados para o cliente sem a necessidade de uma requisição anterior do cliente, permitindo atualização de conteúdo dinâmica.


## Histórico da revisão

Data       | Versão | Descrição                                 | Autor
-----------|--------|-------------------------------------------|-------
08/04/2016 | 1      | Criação da Arquitetura de Referência Web. | Antônio Vítor Pinho Silva
31/03/2020 | 2      | Conversão para AsciiDoc, atualização do modelo de segurança para uso do SISET e atualizações de versões das tecnologias. | SUART06
