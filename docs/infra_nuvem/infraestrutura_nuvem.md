# Infraestrutura de Nuvem

O objetivo deste documento é definir a arquitetura referência de infraestrutura para nuvem utilizada na CAIXA, bem como, padrões e tecnologias a serem utilizados. O documento também deve servir como um guia para direcionar a tomada de decisões sobre quando utilizar cada arquétipo arquitetural e avaliar a viabilidade de novos componentes conforme as estratégias de negócios e premissas estabelecidas pela área responsável - SUART.

![Diagrama da arquitetura de referência de infraestrutura](../img/infraestrutura/Imagem1.png)

## Arquitetura de Processador

1. A CAIXA utiliza a arquitetura X86 no ambiente on-premises, motivo pelo qual será replicada a mesma estratégia para nuvem.

     1. A manutenção em nuvem da arquitetura utilizado no ambiente on-premisses facilita a migração sem a necessidade de reescrita da aplicação. Sendo necessário apenas as adaptações para o adequado funcionamento em nuvem.

     2. Com isso, para reduzir a possibilidade de lock-in com a impossibilidade de migração de aplicações para o ambiente on-premises não será permitida a utilização da arquitetura ARM.

     3. No ambiente em nuvem será possível a utilização de processadores tanto do fabricante Intel quanto da AMD

2. **Modelos de máquinas disponíveis em Nuvem**

    1.     Para padronizar as aplicações, funções e políticas de uso, haverá blueprints habilitados com os seguintes modelos de máquinas em nuvem:

         •	Máquina virtual com 2vCPU e 4GB de memória RAM

         •	Máquina virtual com 2vCPU e 8GB de memória RAM

         •	Máquina virtual com 4vCPU e 8GB de memória RAM

         •	Máquina virtual com 4vCPU e 16GB de memória RAM

         •	Máquina virtual com 4vCPU e 32GB de memória RAM

         •	Máquina virtual com 8vCPU e 16GB de memória RAM

         •	Máquina virtual com 8vCPU e 32GB de memória RAM

         •	Máquina virtual com 16vCPU e 32GB de memória RAM

         •	*  Máquina virtual com 16vCPU e 64GB de memória RAM

         •	*  Máquina virtual com 32vCPU e 128GB de memória RAM

         •	*  Máquina virtual com 64vCPU e 512GB de memória RAM 

         -     Máquinas com configuração diversa dos padrões estabelecidos no item 2, deverão ser aprovadas pela SUART na reunião inicial de arquitetura.

3. Visão de Arquitetura

![Diagrama da arquitetura de referência de infraestrutura](../img/infraestrutura/Imagem2.png)

4. Redundância de máquinas virtuais (local, zona, global)

    1. O pool de máquinas virtuais deve possuir alta disponibilidade para os workloads em nuvem, para isso deverão ser criadas dentro de um mesmo dimensionamento duas ou mais VMs

    2. A redundância pode ser dentro de uma mesma zona de disponibilidade, em zonas de disponibilidade distintas, dentro de uma mesma região ou em regiões de disponibilidade distintas a depender da criticidade da aplicação.

        1. A definição dos locais de implantação ocorrerá na reunião inicial de arquitetura de cada aplicação a ser migrada/criada em nuvem.

        2. Por padrão a redundância será pelo menos dentro de uma mesma zona de disponibilidade. 

        ![Diagrama da arquitetura de referência de infraestrutura](../img/infraestrutura/Imagem3.png)

## Sistema Operacional

1. As máquinas virtuais poderão ser criadas com sistema operacional Windows,  Linux e MacOs nas seguintes distribuições e versões:

     - Linux CentOS 7 ou superior;

     - Linux Debian 8 ou superior;

     - Linux Red Hat Enterprise 7 ou superior;

     - Linux Suse 11 ou superior;

     - Microsoft Windows Server 2012 ou superior;

     - MacOS Mojave 10.14 ou superior.

2. É desejável que as máquinas virtuais contenham a capacidade de crescimento automático em função da demanda (autoscaling).

    1. A utilização do recurso de autoscaling será decidida na reunião inicial de arquitetura

    2. A utilização de licenças do ambiente on premises em nuvem (byol) será definida na reunião inicial de arquitetura.

        1. A utilização de byol será permitida apenas em casos excepcionais a depender da previsibilidade de uso em contrato para evitar o pagamento de multas oriundas do uso em ambiente diverso daquele previamente contratado.
    
    3. A definição do sistema operacional utilizado ocorrerá na reunião inicial de arquitetura de cada aplicação a ser migrada/criada em nuvem.

        1. O sistema operacional padrão é o Linux RedHat Enterprise.

## Banco de Dados

1. A definição sobre a escolha dos SGBDs será feita na reunião inicial de arquitetura.

2. Poderão ser utilizados os seguintes SGBDs:

    - PostgreSql, SQL Server, Oracle ou DB2.

    1. A SUART13 – Arquitetura de Dados é a equipe responsável pela definição do SGBDs a ser utilizado.

3. Os serviços de banco de dados devem:

     1. Permitir a criação de banco de dados gerenciado (PaaS), seguindo o modelo de responsabilidade de serviços em nuvem na impossibilidade de utilização dos modelos IaaS;

     2. Permitir a escolha do tipo de disco que suportará o banco de dados ou oferecer o melhor disco disponível baseado no requisito da aplicação;

     3. Implementar recursos de segurança relacionados ao controle de acesso;

     4. Implementar recursos de detecção de falhas e recuperação dos recursos computacionais e aplicações; 

     5. Permitir o monitoramento do banco de dados. 

     6. Ter a capacidade de configurar recurso de alta disponibilidade.

     7. Permitir atribuir o tipo de recurso computacional que suportará o banco de dados;

     8. Oferecer serviço de log de autenticação;

     9.	Ter possibilidade de registrar modificações DML, DDL e DCL

     10. Os sistemas de gerenciamento de banco de dados devem estar devidamente licenciados (edição Standard ou superior) e aptos para uso.

     11. Os SGBDs deverão possuir no mínimo as seguintes características:

         - Padrão SQL 2016; 

         - Capacidade para atendimento à GDPR;

         - Capacidade para atendimento à LGPD;

         - Drivers ODBC versão 4 para windows 10 e superior;

         - Drivers JDBC tipo 4 para Java 8 e superior;

         - OLTP (online transaction processing);

     12. A SUART13 definirá as ferramentas de monitoramento a serem utilizadas em nuvem para cada aplicação.

        ![Diagrama da arquitetura de referência de infraestrutura](../img/infraestrutura/Imagem4.png)

## Armazenamento (em breve)     

## Backup

 1. Será realizado o backup de todas as aplicações, dados e scripts de configuração que estiverem disponíveis em nuvem, o que inclui as imagens das máquinas virtuais importadas previamente para o provedor de nuvem, cópias dos dados armazenados em dispositivos de armazenamento em nuvem e cópias dos bancos de dados que fazem parte da arquitetura das aplicações da CAIXA provisionadas em nuvem.

 2. As imagens das máquinas para backup e importação para o ambiente on-premises são aquelas que foram previamente migradas do seu ambiente on-premises para o ambiente de nuvem.

 3. A forma de backup de cada aplicação será definida na reunião inicial de arquitetura.

 4. Em conformidade com a IN5 GSI/PR, apenas poderá ocorrer backup em região diversa, caso essa esteja também em território nacional.

     1. Caso as aplicações não possuam informações sensíveis e o provedor não possua mais de uma região em território brasileiro, o backup poderá acontecer dentro da mesma região conforme preconiza a IN5 GSI/PR.

     2. As aplicações que possuam informações sensíveis não poderão possuir backup em nuvem.

5. O Link dedicado entre os datacenters da CAIXA e entre os provedores de nuvem poderá ser um túnel expresso ou link do tipo VPN, baseado na definição da SUART07 – Rede de Telecomunicações.

6. Arquitetura de referência.

![Diagrama da arquitetura de referência de infraestrutura](../img/infraestrutura/Imagem5.png)

## Devops

1.  O objetivo do Devops é servir de apoio técnico na automação de entregas de versão de aplicações em todos os ambientes.

2. A arquitetura de Devops se destina às equipes de desenvolvimento e operações. A GECEQ é a unidade responsável por definir a prioridade de cada solicitação de automação Devops. Ela define a prioridade, a equipe da coordenação do sistema e os técnicos envolvidos.

3. Os sistemas elencados pela GECEQ terão a implantação da Integração e das entregas contínuas. Sendo migrados os ecossistemas das soluções para a esteira corporativa de DEVOPs.

4. Premissas da Esteira DEVOPs CAIXA

    1. Identificar as necessidades

        1. Repositório DEVE ser GIT, preferencialmente fontes.caixa.

        2. Cada REPO DEVE ter contemplar apenas 1(uma) Build Pipeline Ex.: frontend, backend em REPO separados.

        3. DEVE ter repositório dedicado para itens de configuração.

        4. Confirmar se as dependências em suas respectivas versões estão disponíveis no Nexus (binarios.caixa).

        5. Mapear as configurações e as variáveis de ambiente para configurações por dos diversos ambientes (DES|TQS|HMP|PRD).

        6. Mapeamento das portas e conexões que fazem parte do ecossistema da aplicação.

        7. O sistema DEVE ter pelo menos 1(um) build no Nexus (binarios.caixa).

        8. Criar Grupos de AD para devidos permissionamentos nos perfis dos times que atuarão no ADS.

        9. DEVE-se ter o nome da comunidade ao qual o aplicativo está vinculado para futura criação de grupos e times.

        10. Identificar os parâmetros mínimos necessários para aprovação no Quality Gate do SonarQube.

        11. Ter domínio do DAS ou qualquer documento que represente a arquitetura a da aplicação/sistema.

        12. OBS.:A equipe do SAI pode ser acionado para ajustar as necessidades identificadas.

    2. Mapear atividades para as configurações nas pipelines no perfil de desenvolvedor

        1. Configurações dos repositórios de fontes

            1. Adicionar as configurações de SSL para utilização do git

        2. Realizar o clone e respectivamente o push, caso o repositório não tenha sido previamente migrado;

        3. Quando necessário, separar os repositórios por unidades de empacotamento necessárias, por exemplo, frontend e backend;

        4. Entrar no gerenciamento de repositórios, acessar o repositório, definir a branch default (Set as default branch);

        5. Havendo mais de um repositório de fontes, realizar o procedimento;

        6. Usar como referência o repositório de sample-* da respectiva linguagem;

        7. Crie a pasta. s2i/bin e crie os arquivos "run", "assemble". Você pode encontrar os modelos dos arquivos no repositório "Sample-Node" (Wiki)

        8. Abra o arquivo "run" e verifique se ele está apontando para o "app.js" corretamente (Wiki).

        9. Ajustes nos arquivos de configuração conforme respectivas tecnologias;

        10. Ajustar ENVIRONMENTS (Variables Group) nos demais arquivos de configurações conforme linguagens para o processo de build, esta atividade apoia e é apoiada pela atividade 1.5. (SAI);

        11. Angular e node: environments/*

        12.	Java: jmx_prometheus.yaml

    3. Configurações da Build Pipeline

        1. Identificar a Build Pipeline criada pelo SAI.

        2. Testar Build Pipeline para validar o processo de configuração.

        3. Fazer agendamento (queue) da build.

        4. Validar a execução da build, até sua finalização gerando pacote publicável

            1. Caso de insucesso, realizar os ajustes necessários e repetir o processo.

            2. Caso de sucesso, o pacote é gerado no NEXUS, com todas as tarefas executadas, incluindo as de garantia da qualidade;

        5. Ajustes nos repositórios de configurações

        6. Identificar o repositório de configurações criado pelo SAI.

        7. Ajustar ENVIRONMENTS (Variables Group) nos demais arquivos de configurações conforme linguagens para o processo de release, esta atividade apoia e é apoiada pela atividade 1.5. (SAI).

        8. Angular: sample-nginx.conf.

        9. Java: standalone.conf.

    4. Incluir ou configurar os Variable Groups.

        1. Solicitar ao SAI a clonagem e edição dos variables group conforme a linguagem e stages.

        2. Incluir e/ou validar as variáveis.

    5. Solicitações das liberações de portas e firewall no portal.infra.

        1. OBS.: Caso haja a necessidade de novos tipos de repositórios, deve-se ratificar com a GECEQ04

    ![Diagrama da arquitetura de referência de infraestrutura](../img/infraestrutura/Imagem6.png)

5. Catálogo de ferramentas

     **Ferramentas**

     **Automação e Teste**

     Katalon;

     R. Dev. Teste for Z System;

     R. Funcional Testes;

     Cucumber;

     C. Based T. Automation;

     JMock;

     TestNG;

     Hiperstation;

     Selenium;

     JUnit;

     Appium;

     Capybara;

     Livre;

     **Geração de Massa**

     File-Aid;

     Livre;

     **Teste não funcional**

     Azure;

     R. Performance Tester;

     R. Test Workbench;

     Qa-load;

     JMeter;

     **Análise de Código**

     CA Introscope;

     Livre;

     PMD;

     Abend-Aid;

     Quality control for Cobol;

     Xpediter;

     Quality Control for DB2;

     Sonarlint;

     Sonar;

     Teste de API

     Postman;

     SoapUI;

     **Gestão de Teste**

     R. Quality Manager;

     TesteLink;

     Mantis;
