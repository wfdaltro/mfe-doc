
### 1 - DLP – Data Loss Prevention

O processo de DLP – Data Loss Prevention (Prevenção à perda de dados) tem por objetivo reduzir o risco de vazamento, roubo e compartilhamento inadequado de dados corporativos por meio da identificação, monitoração e aplicação de políticas nos pontos de saída, como email, portas USB, capturas de tela, impressão, compartilhamento de arquivos, entre outros.

As políticas de DLP são previamente definidas pela CAIXA, em alinhamento às normas internas, regulamentações externas e grau de confidencialidade dos dados, podendo haver o controle da saída de determinadas informações.

**Situação atual**: Processo implementado por meio de politicas aplicadas nos sistemas operacionais e ativos de redes e com soluções da Proteção de Informações da McAffee e da Microsoft.

**Principais gaps**: Quantidade limitada de licenças (Microsoft).

Defasagem tecnológica (McAfee).

Cliente de conformidade das máquinas com limitações de expansão de politicas (Microsoft SCCM).

**Situação alvo**: Cobertura de todos os perfis de usuários da CAIXA e unificação das ferramentas e políticas.

### 2 - DRM - Digital Rights Management

O DRM-Digital Rights Management permite a proteção persistente de arquivos e e-mails por meio da aplicação de políticas que usam mecanismos de criptografia, identidade e autorização. Isso permite que a proteção dos dados corporativos esteja ativa mesmo fora do perímetro organizacional.

**Situação atual**: Em fase de estudos para planejamento da implementação das ferramentas de RMS (Azure Rights Management) da Microsoft.

**Principais gaps**: Quantidade limitada de licenças (Microsoft).

**Situação alvo**: Implementação e cobertura de todos os perfis de usuários da CAIXA.

 