
### 1 - Ofuscação de código

Solução usada na esteira de desenvolvimento e que é responsável por
transformar o código fonte de artefatos não compilados distribuídos para
a camada de frontend das aplicações.

A distribuições de código, a exemplo de bibliotecas javascript, não
ofuscadas possibilita uma fácil depuração do código por pessoas que têm
objetivo de interferir em tais códigos buscando acessos indevidos ao
ambiente de TI da Caixa.

A ofuscação do código consiste em "embaralhar" o código de forma que o
navegador possa executá-lo, porém a análise visual do código torna-se
extremamente complexa.

**Situação atual**: Não temos solução de ofuscação.

**Situação alvo**: solução de mercado adquirida e em uso pelas equipes
de desenvolvimento. Encontra-se em andamento uma prospecção, prevista para
concluir em out/2021. 

### 2 - Análise de código

Solução usada pelas equipes de desenvolvimento e de operação de
segurança, com o objetivo de identificar vulnerabilidades nos códigos
das aplicações e assim poder corrigi-los ainda na fase de
desenvolvimento, portanto, antes da implantação em produção, momento em
que tais vulnerabilidades poderiam ser exploradas.

A análise das aplicações pode ser realizada na modalidade SAST (Static
Application Security Testing) ou DAST (Dynamic Application Security
Testing).

Na análise SAST, ou análise estática, a solução examina o próprio código
fonte; enquanto na análise DAST, ou dinâmica, a análise é feita sobre a
aplicação em execução.

As duas análises se complementam na busca possíveis vulnerabilidades
existentes no código da aplicação.

**Situação atual**: Solução Fortify, da Microfocus, adquirida em 2019,
atendendo os requisitos atuais.

**Principais gaps**: Performance.

> Suporte da solução acaba em dez/2021.

**Situação alvo**: Manutenção da solução atual.

 