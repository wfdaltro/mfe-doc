

O conjunto de soluções de proteção à rede engloba:

-   Firewall/Application Firewall,

-   IDS/IPS (Intrusion Detection/Prevention System)

-   CASB (Cloud Access Security Broker)

-   Anti DDoS

-   Proxy\*

As soluções trabalham integradas, proporcionando:

-   Detecção e bloqueio de acessos maliciosos

-   Detecção e bloqueio de ataques de negação de serviço

-   Geração de eventos para o [correlacionador de
    eventos](#análise-de-vulnerabilidade)

-   Acesso seguro a sistemas e informações disponíveis na nuvem

-   Proteção dos usuários externos ao acessarem serviços na Internet

### 1 - Firewall

A solução de Firewall tem por objetivo garantir que somente as conexões
permitidas sejam estabelecidas com o ambiente de TI Caixa. A partir de
regras definidas na solução é possível definir as origens e destinos
autorizados, considerando-se endereços IPs e protocolos. O firewall de
próxima geração (Next Generation Firewall) agrega ainda funcionalidades
de combate a vulnerabilidade, intrusão e malware.

**Situação atual**: Firewall tradicional.

**Principais gaps**: Defasagem tecnológica,

Capacidade de atendimento

**Situação alvo**: Aquisição de Next Generation Firewall com capacidade
compatível com a nova realidade de serviços da Caixa.

### 2 -  IDS/IPS

Solução que atua na detecção e prevenção contra diversos ataques de
intrusão. O IPS atua de forma complementar ao Firewall, podendo ser
embarcado no firewall ou ser um componente autônomo. Assim como o
firewall, o IPS também possui uma evolução denominada Next Generation
IPS, que agrega funcionalidades avançadas, não encontradas no IPS
convencional.

**Situação atual**: Existe IPS, porém apenas em uma entrada.

**Principais gaps**: Equipamento defasado tecnologicamente

> Nem todos os segmentos de rede estão protegidos

**Situação alvo**: Next Generation IPS dedicado e dimensionado para a
realidade de negócio da Caixa.

### 3 -  Proxy

A solução de proxy atua como um intermediário entre a rede interna e a
Internet, possibilitando a conversão dos endereços internos em endereços
reconhecidos na Internet, além de funcionalidades como: filtro de
páginas web, baseado na política corporativa de acesso à Internet;
antimalware para impedir que o acesso à internet cause uma infecção na
rede.

**Situação atual**: Solução em produção defasada tecnologicamente. Nova
solução adquirida em abr/2021 e em processo de validação.

**Principais gaps**: Defasagem tecnológica.

**Situação alvo**: Serviço de proxy com filtro de conteúdo e
antimalware.

### 4 - CASB

Cloud Access Security Broker (CASB) é uma solução que fica entre a infraestrutura interna e a nuvem, e tem como objetivos principais apresentar visibilidade do tráfego em trânsito para fora da organização como também do tráfego para aplicações hospedadas na nuvem.

**Situação atual**:Não há.

**Principais gaps**:Falta de Controle de utilização, Segurança dos dados e Proteção contra ameaças.

**Situação alvo**:Solução on premise dimensionada para atender todos serviços em nuvem.

### 6 - Anti DDoS

O Anti DDoS combate ataques de negação de serviço. A solução atual tanto
on premise quanto em nuvem, visando combater ataques menos potente com
recursos on premise e redirecionando o tráfego para a nuvem quando o
ataque é superior à capacidade do link. A solução também é capaz de
deter ataques automatizados não baseados em volumetria.

**Situação atual**: Não há

**Situação alvo**: Solução híbrida, contemplando recursos on premise, em
nuvem e baseados em operadoras de telecom.

### 7 - WAF

O Web Application Firewall combate ataques que ocorrem na camada de
aplicação, os quais não são tratados pelo Firewall de rede. O WAF
permite a inspeção dos pacotes de rede no nível da camada de aplicação,
possibilitando "ver" chamadas específicas que buscam burlar controles
nas aplicações.

**Situação atual**: Não há.

**Situação alvo**: Solução on premise dimensionada para atender todos os
segmentos de rede.

### 8 - NPB -- Network Packet Broker

O NPB (Network Packet Broker) tem como objetivo fazer a "ponte" entre as soluções de segurança e os diversos segmentos de rede. Nesse papel, o NPB atua como broker, de forma que é possível compartilhar um mesmo ativo de segurança entre diferentes segmentos, promovendo uma redução na quantidade de ativos necessários e, consequentemente, uma redução no custo total com soluções de segurança. A adoção de um NPB também proporciona uma melhor gestão, seja pela menor quantidade de dispositivos, seja porque possibilita uma rápida e imediata reconfiguração da arquitetura.


**Situação atual**: Não existe na CAIXA.

**Situação alvo**: Solução será adquirida e implantada.

### 9 - Network Security Policy Configuration

Uma solução de Network Security Policy Configuration permite um
gerenciamento centralizado e fácil das configurações dos ativos de
segurança, em especial dos firewalls.

**Situação atual**: Não existe na CAIXA.

**Situação alvo**: Solução será adquirida e implantada.

### 10 - Acesso Remoto

Uma solução de Acesso Remoto permite o acesso dos colaboradores, a
partir da Internet, à rede Interna da CAIXA, com segurança, facilidade e
bom desempenho.

**Situação atual**: Usadas soluções de VPN e VDI

**Principais gaps**: O acesso via VPN não tem apresentado uma boa performance. 

**Situação alvo**: Solução de acesso remoto, independente de VPN, que
garanta um acesso seguro, boa experiência para o usuário e boa
performance.
 