
### 1 - Correlação de eventos -- SIEM -- Gerenciamento de informações e de eventos de segurança

Os componentes da arquitetura de segurança listados abaixo geram
informações passíveis de consumo pela solução de correlação de eventos:

-	Aruba Clear Pass
-	Cisco ASA
-	Logstash estações de trabalho
-	McAfee EPO DLP
-	MS DHCP
-	BIND DNS
-	Mainframe
-	Microsoft Exchange
-	SSO
-	Cisco ISE
-	Domain Controller
-	Microsoft TMG
-	VMware Horizon – VDI
-	LDAP


A solução é capaz de coletar eventos dessas fontes, mesmo que as tecnologias empregadas sejam diferentes, gerando alertas com base no conjunto de regras definidas pela Caixa.

Os alertas gerados são consumidos pela GECMI no processo de segurança cibernética.

**Situação atual**: Solução LogRhythm, adquirida em 2018, atendendo os requisitos atuais.

**Situação alvo**: Avaliar a possibilidade de utilização de solução em nuvem.

### 2 - Monitoração de Bancos de Dados

A solução de monitoração a Bancos de Dados garante a rastreabilidade dos comandos executados por usuários DBA’s. A monitoração consiste em identificar quando um comando SQL é executado na base de dados. 

Controle de monitoramento/revisão tempestiva dos logs relacionados a execução e comandos de alteração de dados nos bancos de dados. Ainda é possível realizar o bloqueio de um comando SQL a partir de uma política definida na própria ferramenta (IBM Guardium). 

A solução IBM Guardium permite atuar de modo a adotar medidas de segurança técnicas e administrativas aptas a proteger os dados estruturados quando da execução de comandos SQL nas bases de dados, evitando situações acidentais ou ilícitas de destruição, perda, alteração indevida ou qualquer forma de tratamento inadequado dos dados. 

Fortalece a eficiência do tratamento de incidentes de segurança, elevando a capacidade de gestão das bases de dados e permitindo a resposta mais rápida às ameaças. 

Centraliza os dados de auditoria das bases de dados, permitindo integração com as demais ferramentas de segurança contratadas pela CAIXA. 

É importante salientar que a solução não visa a gestão de acesso as bases de dados. 

**Situação atual**: Solução IBM Guardium que monitora as bases de dados dos sistemas considerados críticos do ambiente corporativo da CAIXA. Solução customizada (Graylog) para atender SGDB’s legados (Caché e IDMS).

**Principais gaps**: Broadcom IDMS e Intersystem Caché.

**Situação alvo**: Controle sobre as execuções nas bases de dados dos sistemas corporativos, atendimento aos requisitos legais e mitigação de riscos no tratamento indevido dos dados, bem como elevar a atuação da cibersegurança nas bases de dados.

### 3 - Análise de Vulnerabilidade

A identificação de vulnerabilidades é realizada por toda a equipe de segurança tecnológica e direcionada as equipes gestoras ou detentoras dos ativos para avaliação e mitigação do risco apresentado de forma mais breve possível.

Essa identificação, pode ocorrer de forma proativa através da varredura de ambientes, ou realização de Pentests, ou ainda ser resultado da atividade cotidiana e até mesmo de colaboração interna ou externa.

Uma vez identificada, a vulnerabilidade deve ser direcionada para tratamento pelo responsável do ativo envolvido de forma tempestiva, conjuntamente a classificação inerente ao risco de exploração.

Vale salientar que a visualização da informação nas ferramentas é pública, portanto, as vulnerabilidades identificadas não devem ser descritas em detalhes nas respectivas demandas de tratamento via GSC e RTC, impedindo assim o comprometimento de informações referente às falhas de segurança nos ativos da CAIXA.

3.1 Análise DAST (Dynamic Application Security Testing)

Complementa a análise SAST (Static Application Security Testing), usada
no processo de desenvolvimento, buscando identificar vulnerabilidades em
aplicações.

**Situação atual**: Soluções atendem aos requisitos atuais.

**Situação alvo**: Avaliar novas ferramentas que auxilie na gestão da vulnerabilidade de endpoints e da infraestrutura corporativa.

3.2 Teste de Intrusão/penetração

Solução usada para "forçar" o acesso ao ambiente de TI Caixa, simulando
a ação de atacantes. Assim como a solução de análise de código, a
solução de intrusão visa identificar vulnerabilidades existentes em
ambiente e aplicações.

A solução atua "disparando" diversos tipos de ataques similares àqueles
originados em ferramentas maliciosas normalmente usadas por pessoas mal
intencionadas que buscam obter acesso indevido a sistemas de TI. O nível
de força do ataque pode ser definido na solução, podendo ir desde uma
simples inspeção até uma tentativa real de invasão.

**Situação atual**: Soluções atendem aos requisitos atuais.

**Situação alvo**: Avaliar novas ferramentas que acelerem os testes de segurança em aplicações.

 