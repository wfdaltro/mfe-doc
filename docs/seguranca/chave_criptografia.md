### 1 - Cofre de senhas

Todas as credenciais de serviços são armazenadas em solução de cofre de
senhas, cuja gestão operacional é da GECMI.

São armazenadas em cofre:

-   Senhas de usuários nativos (root, admin, administrator, etc).

-   Senhas de usuários de serviço -- são considerados apenas os usuários
    de serviço que possuem senha.

-   Secrets de aplicações geradas pelo Authorization Server Caixa ou de
    terceiros .

O cofre de senhas é uma solução tolerante a falhas, com controle de
acesso que garante a absoluta inviolabilidade das credenciais,
implementando os pilares da confidencialidade, integridade e
disponibilidade.

**Situação atual**: Finalizada a aquisição de uma solução de PAM (Privileged Access Management), que atenderá as necessidades do processo de gestão de credenciais de serviço. O pregão foi finalizado em jul/2021. O vencedor do processo foi a NCT com a solução Beyond Trust.


**Situação alvo**: Implantação e uso contínuo da nova solução.

### 2 - HSM e Vault

As chaves criptográficas são armazenadas e processadas em solução de HSM 
(Hardware Security Module), garantindo a sua confidencialidade e 
integridade e também tolerância a falhas.

É responsável por operações criptográficas, assinaturas digitais,
geração e armazenamento de chaves.

Em ambiente de nuvem, as soluções de Vault podem entregar o serviço tanto em
hardware quanto em software.

-   Na Azure, há o Azure Keyvault (https://azure.microsoft.com/pt-br/services/key-vault/#product-overview).
-   No AWS, há o CloudHSM (https://aws.amazon.com/pt/cloudhsm/)
-   Na GCP, há o Cloud HSM (https://cloud.google.com/kms/docs/hsm)

Normalmente, esses serviços de HSM em nuvem usam um equipamento de um fabricante
de HSM, a exemplo do Azure Keyvault, que usa um HSM da Thales.

**Situação atual**: Na plataforma distribuída é usada solução da Dínamo,
porém sem redundância e com baixo throughput.

Na plataforma mainframe é usado o ICSF (Integrated Cryptographic Service
Facility) juntamente com os hardwares de criptografia providos pela
plataforma.

![Visão Geral](../img/seguranca/image4.png)

**Principais gaps na plataforma distribuída**:

-   Garantia de disponibilidade.

-   A taxa de operação não atende as expectativas de crescimento,
    principalmente com a chegada do PIX.

**Situação alvo**: O contrato com a Dínamo foi renovado através de aditivo e, atualmente, estamos recebendo os novos equipamentos para disponibilização em ambiente produtivo e tratar a questão da redundância e baixo throughput.

### 3 - Chaves em keystore

O keystore é um repositório de certificados de segurança, utilizado para
o armazenamento de chaves, onde essas chaves são identificadas através
de um alias.

O arquivo keystore é criado dentro de um servidor de aplicação com JBOSS
e instalado em diretório definido pelo usuário, esse arquivo deve ser
utilizado na geração de novos arquivos dentro do repositório.

**Situação atual**: É usado na Caixa nas seguintes situações:

-   Geração de certificados para aplicação;

-   Certificados para criptografia ponta a ponta de servidores;

-   Criação de VAULT (utilizado a partir do JBOSS 4.1 até JBOSS7) ou
    ELYTRON (a partir do JBOSS 7.1) para a criptografia de senhas de
    usuários de serviço e chaves do SSO.

**Principais gaps**:

-   Não existe procedimento padronizado na Caixa;

-   Muitas aplicações não usam versões do JBOSS compatíveis para
    utilização de keystore (o que implica em vulnerabilidades dessas
    aplicações, por manterem dados sensíveis sem criptografia).

-   Em muitos casos é utilizado a senha padrão (fornecedor) para a
    geração do keystore.

**Situação alvo**: O uso do keystore está publicado na TE079, de acordo com as melhores práticas de segurança, mas seu uso, atualmente, ocorre como exceção, mediante a autorização da GECMI.
