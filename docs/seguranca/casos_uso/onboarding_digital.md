O Onboarding digital é um processo do [IAM Caixa](../onboarding_auth.md). pelo qual é
criada uma Identidade Digital para um novo cliente Caixa, permitindo,
assim, seu acesso aos sistemas e serviços providos pela empresa.

Considera-se aqui "Cliente", tanto clientes de produtos bancários quanto
dos demais produtos disponíveis no portfolio da Caixa, incluindo aí
loterias e serviços sociais.

O Onboarding Digital pressupõe um reconhecimento com o mínimo de
segurança de que a pessoa alvo do Onboarding é realmente quem ela diz
ser.

Para se ter uma garantia mínima de que a pessoa é quem ela diz ser,
pode-se lançar mão de canais físicos ou canais totalmente digitais,
sendo que esse último apresenta um desafio significativamente maior com
relação ao primeiro.

Ao falarmos da autenticação em canais físicos, estamos falando
basicamente de:

-   Validação documental de forma presencial em um ponto de atendimento
    da Caixa;

-   Validação de cartão com chip na rede de ATMs da Caixa. Salienta-se
    que essa alternativa somente se aplica a clientes correntistas.

Para autenticação em meios exclusivamente digitais temos as
alternativas:

-   Background check (base de informações previamente conhecidas);

-   Quiz (informações previamente conhecidas);

-   Validação biométrica (base previamente conhecida e autenticada);

-   Dados de dispositivo (informações prévias daquele dispositivo).

Observa-se que nesse caso sempre é necessário o conhecimento prévio de
informações sobre a pessoa, o que torna o desafio ainda maior.

No processo de Onboarding digital, o principal componente da arquitetura
de referência de segurança é o IAM, que atua como um orquestrador dos
diversos serviços usados no Onboarding, conforme indicado no fluxo
abaixo:

![](../../img/seguranca/image7.png)

No processo de Onboarding deve-se buscar o registro de um segundo fator
de autenticação, complementar à senha, e que será usado nos próximos
acessos do usuário.

Quando o Onboarding ocorrer em dispositivo móvel, a identificação e
registro do dispositivo pode e devem ser usados como segundo fator,
usando-se o registro do par "Identificador do Usuário -- Identificador
do dispositivo".

A validação dessa relação nos próximos acessos é de vital importância
para a segurança do acesso.

Uma vez realizado o Onboarding, que resulta na criação da Identidade
Digital, essa identidade autenticada a cada acesso por meio dos fatores
de autenticação registrados, a exemplo de:

Senha + certificado digital

Senha + biometria

Senha + token por SMS

Senha + token por email

Senha + token por push

O próprio identificador do dispositivo, sendo já conhecido, pode ser
usado como fator adicional de autenticação. Dessa forma, caso o usuário
esteja acessando a partir de um dispositivo ainda não associado à sua
Identidade, poderá se lançar mão de alguns dos mesmos serviços usados no
processo de Onboarding para revalidar a Identidade do usuário.

Nas figuras abaixo encontramos a definição de como usar um serviço
externo de autenticação de selfie, como segundo fator, nos fluxos de
Onboarding e de autenticação.

![](../../img/seguranca/image8.jpg)
![](../../img/seguranca/image9.png)
![](../../img/seguranca/image10.png)