O novo modelo solução de ATM, adquirido no mercado, deve:

a)  Prover toda a segurança intrínseca, ou seja, a garantia de
    inviolabilidade entre suas camadas de front e de backend, impedindo
    violações de qualquer natureza no frontend e garantindo a
    comunicação segura entre essas duas camadas.

b)  Integrar-se com a arquitetura de TI da CAIXA, conforme padrões aqui
    definidos.

O escopo desta arquitetura se restringe ao ponto destacado no item "b",
acima, e considera duas possibilidades:

a)  O backend da solução de ATM centralizado e, portanto, fora do
    terminal físico.

![](../../img/seguranca/image20.png)

Nesse caso, o backend é responsável por armazenar o "secret", que é
utilizado na comunicação com o Authorization Server no processo de
validação dos diversos tipos de credenciais: senha de acesso aos
aplicativos, biometria, Identificação positiva, senha do cartão, OTP,
etc.

b)  O backend da solução dentro do terminal físico, junto do frontend

![](../../img/seguranca/image21.png)

> Aqui, o "secret" deve ser obtido do SIMTC, somente após a validação do
> terminal. Nesse caso, o secret não deve ser armazenado, ficando
> disponível apenas em memória protegida contra inspeção de código em
> execução.
>
> Em ambos os modelos ("a" e "b"), deverão ser adotadas medidas de
> segurança, tais como:

a)  Validação da integridade da aplicação;

b)  Autorização do terminal;

c)  Proteção da origem (IP), impedindo terminais espúrios se conectarem
    de origens (IPs) não autorizados;

d)  Associação entre IP e número de terminal, que deve ser checada
    durante as transações;

e)  Outras proteções já observadas na solução atual.

> No caso da autenticação da Identificação Positiva Silábica (SIRAN),
> caberá ao SIORQ obter o "ID" do mapa junto ao SIRAN, e repasse dessa
> informação para a solução do ATM, de forma que, quando for submetida a
> resposta do usuário, essa siga juntamente com o ID do mapa,
> possibilitando que o SIRAN faça a validação das respostas.
>
> Como arquitetura de transição, enquanto o Authorization Server não for
> capaz de validar um determinado tipo de credencial, o SIORQ poderá
> orquestrar diretamente a autenticação na base correspondente, por
> exemplo, Identificação Positiva Silábica, no SIRAN, e biometria, no
> SIABM.
