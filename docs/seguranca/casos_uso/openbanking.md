Para o OpenBanking, fizemos a realização do desenho da arquitetura de
consentimento. A arquitetura de consentimento se tornou um caso
específico pelo fato de não poder usar o modelo padrão de consentimento
que estávamos aplicando ao SSO.

Para este projeto, é necessário que o consentimento dado seja atribuído
por prazo definido pelo usuário e temos que prever a possibilidade de
ser realizada uma multiautorização, em que uma pendência é gerada a
partir de uma autorização inicial, para que outros entes também
autorizem a transação, sendo que só desta forma ela será efetivada.

O consentimento será tratado então pelo SIAPD, sistema que já estava
sendo construído prevendo mecanismos de consentimento para atender à
LGPD, e que se adequará às demandas específicas do OpenBanking.


![](../../img/seguranca/image11.png)
![](../../img/seguranca/image12.png)
![](../../img/seguranca/image13.png)
![](../../img/seguranca/image14.png)
![](../../img/seguranca/image15.png)