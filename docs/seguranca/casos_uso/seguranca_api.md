O acesso a APIs deve ser protegido com Access Token padrão JWT, gerado
pelo Authorization Server, conforme protocolo Openid Connect/OAuth 2.0.

Como regra geral, sempre que possível, deve-se propagar o token de
acesso do usuário até a última milha, ou seja, o recurso protegido,
fazendo com que o Access Token passe por toda a arquitetura, fim-a-fim.

A despeito dessa boa prática, sabemos que em determinadas situações é
impossível, ou inviável, obter-se um token de usuário final, a exemplo
de comunicação entre aplicações. Por outro lado, sabe-se também que há
diferentes níveis de criticidade das APIs que são acessadas, que são de
conhecimento dos respectivos gestores.

Portanto, nas situações em que houver impossibilidade ou inviabilidade
técnica, ou mesmo de oportunidade de negócio, cabe ao gestor da API
avaliar risco e permitir, ou não, o acesso às suas APIs com tokens de
serviço (tokens de aplicação), nos quais não há identificação de usuário
final. Nesse caso, a API deverá ser protegida com role, por ela
definida, seguindo o padrão SSS_XXXXXX, onde SSS é a sigla do sistema e
o XXXXX é o nome da role. Adicionalmente, a critério do gestor, poderá
ser implementado também um controle por "ClientID" (aplicação), de forma
que o controle seja dado mediante duas autorizações, sendo uma no
"Authorization Server" (role) e outra na própria API (tabela interna de
controle de aplicações autorizadas).

Dessa forma, cabe à API identificar os diferentes cenários e tratá-los
de acordo:

a\) Token de usuário interno - preferred_username contem usuário interno
("matrícula").

b\) Token de usuário externo -- preferred_username contem CPF.

c\) Token de usuário conveniado -- preferred_username contem CPF e token
possui a claim resource_access com pelo menos uma role com o prefixo
"CONV\_".

d\) Token de serviço -- preferred_username contém a identificação da
aplicação que gerou o token.

Em qualquer situação, a trilha de auditoria deve responder no mínimo as
seguintes questões:

a\) QUEM: usuário que provocou a chamada (usuário final ou da aplicação,
no caso de token de serviço)

b\) O QUÊ: qual a operação realizada

c\) QUANDO: data/hora

d\) ONDE: IP

No caso de informações de negócio, a exemplo de conta, CPF, contrato,
etc, cabe ao gestor da aplicação avaliar a necessidade de inclusão de
tais dados na trilha de auditoria, tendo em mente sempre que a trilha é
um elemento fundamental na análise de incidentes de segurança e que,
portanto, deve registrar todos os elementos necessários a tal análise.

### 3.1 Consumo de serviços hospedados no mainframe

![](../../img/seguranca/image16.png)

Conforme definição da SUART constante na arquitetura de referência de
integrações
(<http://arquiteturati.caixa/arquitetura_referencia/integracoes/>), a
exposição de APIs no mainframe deve usar um dos cenários: 1-Liberty ou
2- JBoss ou MicroProfile.

Em qualquer dos cenários acima as aplicações da baixa plataforma devem
buscar propagar o Access Token até o ponto mais distante possível dentro
da arquitetura.

Para serviços expostos no Liberty, é possível propagar o Access Token
até o servidor Liberty, dentro do mainframe.

Em qualquer situação, onde houver a "tradução" entre a chamada REST,
protegida por um Access Token, e uma chamada CICS, deverá ocorrer os
seguintes controles:

1)  Controle de permissão da origem, que permita definir se a aplicação
    pode chamar a API em questão. O controle deve obter da chamada:

-   Aplicação chamadora (claim "azp" do Access Token)

-   Usuário (claim "preferred_username" do Access Token)

-   Data e hora do token (claim "iat" do Access Token)

-   Roles (claim "realm_access" do Access Token)

-   Serviço chamado (da requisição)

A camada de tradução deve, então, verificar se a lista de "roles" contém
a role exigida pela API a ser chamada.

Opcionalmente, a camada de controle, ou a própria API, poderá
implementar controle por "clientID" (aplicação), permitindo o acesso
somente de clientIDs previamente cadastrados em seu próprio controle.

Em seguida, a camada de tradução deve chamar a transação CICS com o
usuário de serviço correspondente à API a ser chamada.

Esse usuário de serviço (e sua respectiva senha) deve ser de
conhecimento exclusivo da camada de tradução.

A Matriz de Acesso das aplicações chamadoras deve definir os acessos
para os usuários finais, enquanto a Matriz de Acesso das APIs deve
definir os acessos dos usuários de serviço.

Dessa forma, não é necessário que cada aplicação tenha seu próprio
usuário de serviço.

No caso de tokens de usuários internos, ou seja, aqueles que contêm a
identificação de um usuário interno ("matrícula"), não é necessário
propagar o usuário final para o mainframe, uma vez que a autorização
será dada para o usuário de serviço da API que foi chamada.

2)  Registro de trilha de auditoria que permita estabelecer a relação
    entre requisição REST e chamada CICS, de forma que seja possível
    rastrear a chamada de ponta a ponta. Neste caso, deve-se registrar
    pelo menos:

-   Data/hora da requisição

-   IP da origem da transação

-   Id do token

-   Usuário (claim \"preferred_username")

-   Aplicação chamadora (claim "azp" do token)

-   Usuário de serviço usado para chamar a transação CICS

-   Transação chamada

-   Role usada na autorização (qual a role da claim "realm access" que
    autorizou a aplicação a chamar essa transação)

Adicionalmente, a aplicação chamadora de APIs pode registrar, em sua
própria trilha de auditoria, o tokenId, juntamente com as demais
informações da transação.

A API, por sua vez, quando receber o parâmetro tokenId, passado pela
camada de tradução, deve registrá-lo em sua trilha de auditoria,
juntamente com os dados da transação.

A presença do tokenId nas duas "pontas" (aplicação e API) permitirá o
rastreamento fim-a-fim da transação, sem que seja necessário o registro
nas camadas de tradução.

### 3.2 Consumo de APIs a partir de programas COBOL no mainframe

![](../../img/seguranca/image17.png)

Caso a aplicação mainframe esteja rodando no Liberty, o consumo da API
na plataforma aberta se dará por meio de chamada REST protegida com
token de acesso obtido no Authorizarion Server.

Neste caso, pode-se repassar um access token já recebido de outra
chamada, desde que seja a mesma transação, ou pode ser um token de
"serviço" obtido por meio de uma autenticação "client credentials",
conforme definido no protocolo Openid Connect/OAuth 2.0.

Por outro lado, se a aplicação mainframe não estiver rodando no Liberty,
a chamada deve ser feita para uma camada de tradução (middleware) na
plataforma aberta. Essa camada de tradução será responsável por manter
os seguintes controles:

1)  Autorização de quem pode chamar (aplicação mainframe).

A comunicação entre o programa COBOL e o middleware deverá ser feita de
forma segura, impedindo que usuários e/ou aplicações não autorizadas
enviem os dados em nome da aplicação legítima. Isso poderá ser feito por
assinatura da mensagem ou pelo controle de acesso na fila MQ, quando
esse recurso for utilizado.

O middleware deverá manter controle sobre quais aplicações podem
executar cada uma de suas APIs.

2)  Trilha de auditoria, que deve registrar, entre outras coisas:

-   Data/hora da requisição

-   Id do token usado para chamar a API na plataforma aberta.

-   Usuário (caso tenha um CPF envolvido na chamada)

-   Aplicação chamadora (aplicação que fez a chamada)

-   Id da transação (chave que identifique a transação no mainframe\*)

> \* A aplicação no mainframe deve registrar em sua trilha de auditoria
> uma chave (exemplo NSU) que identifique a transação, possibilitando a
> rastreabilidade fim-a-fim.
