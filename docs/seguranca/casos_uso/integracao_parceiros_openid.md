Para a integração com parceiros há dois modelos distintos, um que
envolve o redirect da conexão para a continuidade da transação no
ambiente do parceiro, e outro sem redirect, que envolve apenas a chamada
a uma API do parceiro.

Para proteger o tráfego de informações com o parceiro, deverá ser usado
MTLS(autenticação mútua), modelo em que tanto cliente quanto o servidor
são autenticados por certificado digital.

### 1 - Redirect de conexões para o parceiro

Neste modelo, a aplicação deverá estar integrada com a autenticação
usando o SSO, para que seja gerado um token de usuário no fluxo de
Authorization Code.

Após a obtenção do token de usuário, o mesmo deve ser propagado para o
parceiro, que usará os cookies da sessão para identificar o
Authorization Server e validar o token recebido como válido.

Com o acesso concedido após a validação, o sistema do parceiro deve
autorizar o usuário a efetuar as operações necessárias para a conclusão
do serviço a ser prestado.

Após obter o resultado o usuário deve ser redirecionado de volta para a
aplicação da CAIXA, que fará uma nova validação do token para
restabelecer o acesso à página em que o usuário estava anteriormente.

![](../../img/seguranca/image18.png) 

### 2 - Integração com parceiros sem redirect

Neste modelo, o parceiro deverá disponibilizar uma API para ser acionada
a partir dos processos dos sistemas internos da CAIXA, sendo que o
próprio sistema da CAIXA exibirá as funcionalidades para o cliente
selecionar.

Será feita uma chamada nessa API autenticada com access token
requisitado no sistema do parceiro, utilizando o fluxo de Client
Credentials. Para tal, além da API publicada, o parceiro terá que ter
uma estrutura baseada em autenticação OAuth para geração do token.

O cliente, neste caso, não interage em momento algum de forma direta com
o sistema do parceiro.

![](../../img/seguranca/image19.png) 