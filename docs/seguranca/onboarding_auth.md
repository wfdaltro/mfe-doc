
### 1 -  IAM

O IAM (Identity and Access Management) Caixa é responsável por gerenciar
usuários e dispositivos nos acessos aos sistemas e serviços da Caixa,
estando entre suas responsabilidades:

**Na Intranet** (acesso dos colaboradores aos sistemas internos):

-   Sincronismo com as bases autoritativas (Funcionários próprios e terceiros): SISRH, SISCP, SGA, SGE, SAP.

-   Gerenciamento do ciclo de vida dos usuários: Cadastramento,  alterações e descadastramento.

-   Sincronismo de senha entre as diversas bases de autenticação: AD,  LDAP, OpenLDAP, RACF, etc.

-   Automação do processo de concessão de acessos

-   Manutenção do Compliance dos acessos

-   Provimento de adapters compatíveis com protocolo OpenID
    Connect/oAuth 2.0

**Na Internet** (acesso de clientes e parceiros):

-   Manutenção do cadastro

-   Autenticação e autorização

-   Gestão de dispositivos

-   Integração dos serviços usados no Onboarding digital

-   Implementação do módulo de convênio, responsável pelo vínculo entre  CPF e CNPJ no acesso a sistemas de convênio.

-   Implementação do IDP no protocolo OpenID Connect/oAuth 2.0 (geração e validação de tokens JWT)

-   Provimento de adapters compatíveis com protocolo OpenID Connect/oAuth 2.0

O IAM gera logs capazes de consumo pelo [Correlacionador de Eventos](./soc_csirt.md#análise-de-vulnerabilidade).

O IAM disponibiliza diferentes fatores de autenticação (MFA) a serem usados em adição ou substituição à senha, a exemplo de:

-   OTP

-   Certificado digital

-   Token por SMS

-   Token por email

-   Biometria

![](../img/seguranca/image5.png)

**Situação atual**: Atualmente são usadas as soluções:

-   IdentityIQ (SailPoint) para o gerenciamento de Identidades na
    Intranet,

-   RedhatSSO para o gerenciamento de acesso com protocolo OpenId
    connect/oAuth 2 na Intranet e Internet

-   RedhatSSO para o gerenciamento de dispositivos na Internet

-   RedhatSSO para o gerenciamento de acesso dos conveniados

-   RedhatSSO na integração dos serviços de Onboarding digital.

-   Soluções desenvolvidas internamente: SISGR, SIUSR, SINAV, SIASE,
    SIPER, SISUR, SISSG, SAP/IAM

**Principais gaps**: Existência de múltiplas soluções de IAM, com
consequência direta na qualidade do atendimento aos clientes.

**Situação alvo**: Por questão de performance, encontra-se em andamento
a substituição da solução RedhatSSO pela solução B2C. No momento as
equipes técnicas buscam viabilizar, na nova solução, os serviços
disponíveis na solução atual. Busca-se também a unificação da solução de
IAM eliminando-se as soluções internas.

#### 1.1 Authorization Server

A CAIXA adota o protocolo Openid Connect/OAuth 2.0 para prover
autorização a aplicações e serviços. O protocolo está intrinsicamente
ligado ao processo de autenticação, no qual, quando bem sucedido, é
gerado um Access Token (Token de Acesso) padrão JWT (Java Web Token),
contendo os dados do acesso, entre os quais destacamos:

-   Identificação do Usuário

-   Aplicação

-   Data/hora

-   Endereço de acesso

-   Roles

-   Escopos

O Access Token é, então, usado pela aplicação para acesso a APIs ou
outras aplicações.

Atualmente é usada a solução Redhat SSO no papel de Authorization
Server, existindo diversos "Realms", que são domínios ou agrupamento de
bases e configurações, tais como cadastro de aplicações e de usuários,
fluxos de autenticação, configuração de federações, entre outras:

-   Internet (login.caixa.gov.br) -- hospedado no ambiente onpremise e
    responsável por autenticar e autorizar os acessos de clientes nas
    aplicações de Internet, exceto: apps CaixaTem, DPVAT e Internet
    Banking (nos acessos com biometria).

-   Internet(login2.caixa.gov.br) -- hospedado na nuvem da Microsoft
    (Azure) e responsável por autenticar e autorizar os acessos de
    clientes aos apps: CaixaTem e DPVAT.

-   R_inter_siper (loginx.caixa.gov.br) -- hospedado no ambiente
    onpremise e responsável por autenticar e autorizar os acessos de
    clientes ao app Internet Banking (SIMOB) exclusivamente quando o
    acesso é feito com a opção de biometria.

-   Intranet (login.prd.caixa) -- hospedado no ambiente on premise e
    responsável por autenticar e autorizar usuários internos
    (colaboradores) nas aplicações internas.

-   Serviço (servico.prd.caixa) - hospedado no ambiente on premise e
    responsável por autenticar e autorizar aplicações e APIs, por meio
    do fluxo de "Client Credentials".

As aplicações gerenciadas pelo Authorizarion Server são identificadas
por seu "ClientID", cujo padrão é: cli-ttt-sss, onde:

-   cli: prefixo

-   ttt: tipo de aplicação (web, mobile(mob) ou seriço(ser))

-   sss: sigla da aplicação. Caso a sigla seja "dep" e o clientID
    contenha uma quarta parte, essa última parte indica o nome da
    aplicação departamental. Exemplo: cli-web-dep-portalx.

### 2 - Serviço de diretório

O serviço de diretório, ou base de autenticação, é responsável por
manter a base de usuários, senhas e grupos usadas no processo de
autenticação e autorização a sistemas internos, usados pelos
colaboradores Caixa.

O serviço de diretório é mantido pelo IAM Interno a partir do cruzamento
de informações das bases autoritativas e das matrizes de acesso
definidas pelos gestores.

As matrizes de acesso definem a relação entre:

-   Funcionalidade e perfis

-   Perfis e tipos de usuários

![](../img/seguranca/image6.png) 

Mantém árvores de usuário e grupos capazes de armazenar e prover os
seguintes atributos:

-   Username (Userid interno no formato x999999 ou CPF para usuários
    externos)

-   Nome completo

-   Email

-   Lista de grupos aos quais o usuário tem acesso

-   Atributos adicionais dependendo da solução (interna ou externa)

    -   Externos: Data de nascimento, CEP, Telefone

    -   Internos: atributos obtidos das bases autoritativas (SISRH,
        SISCP, SGA, SGE, SAP), principalmente: CPF, unidade de lotação,
        tipo de usuário, cargo e função.

### 3 - Onboarding

O processo de Onboarding digital deve autenticar a pessoa, garantindo
que ela é realmente quem diz ser. Para isso, podem ser usados diversos
serviços e/ou soluções.

#### 3.1 Serviço de validação documental e de biometria

-   No processo de Onboarding, a solução deve:

-   Capturar imagem de documento de identificação

-   Validar a integridade do documento

-   Capturar imagem da face do usuário (selfie)

-   Comparar a imagem da face capturada (selfie) com a imagem da face
    constante no documento de identificação

-   Armazenar o template criptográfico da selfie

O template da selfie é usado no processo de login, quando:

-   Novamente será capturada a selfie

-   Calculado o template sobre a nova selfie

-   Comparados os templates para determinar se a pessoa que está logando
    é a mesma que fez o onboarding

#### 3.2 Validação de background

Esse serviço é capaz de validar dados fornecidos pelo usuário, tais como
cep, email e número de telefone e verificar o grau de associação desses
dados com o CPF do usuário.
 

 **Situação atual**: As soluções atualmente utilizadas no onboarding digital dos clientas não foram contratadas para este fim, portanto, a expxeriência do cliente possui bastante atrito.


**Situação alvo**: Solução de oboarding digital e face match em prospecção.
