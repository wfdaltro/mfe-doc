
A segurança do frontend engloba os acessos por diversos canais:

-   Aplicativos próprios, tanto em celulares quanto em computadores;

-   Aplicativos/plataformas de terceiros, a exemplo de whatsapp;

-   Aplicativos/serviços de parceiros.

Os aplicativos próprios, para dispositivos móveis, são protegidos pelas
soluções de Gerenciador Mobile.

Os aplicativos próprios, independentemente da plataforma, são protegidos
pela solução de Segurança de Aplicativos Móveis e Web (Proteção do
Client).

Os aplicativos próprios se integram ao [IAM Caixa](onboarding_auth.md).

Os aplicativos próprios, quando embarcados em plataformas de terceiros,
a exemplo do whatsapp, se integram ao [IAM Caixa](onboarding_auth.md) e ao [APIManager](#_2.6__API) quando necessário acessar serviços Caixa.

Os aplicativos/serviços de terceiros se integram ao [IAMCaixa](onboarding_auth.md) e ao [API Manager](#_2.6__API) quando necessário
acessar serviços Caixa.

São características da solução de Gerenciador Mobile:

-   Versionamento

-   Controle de autenticidade

-   Controle de sessão

-   Geração de ID de dispositivo

São características da solução de Proteção do Client:

-   Proteção contra trojans

-   Proteção contra páginas falsas

-   Proteção contra URLs falsas

-   Geração de ID de dispositivo

-   Geração de eventos

-   Proteção contra root/jailbreak em dispositivos móveis

**Situação atual**:

-   Gerenciador Mobile: Mobile First, da IBM.

-   Proteção do Client: Topaz (Antigo Warsaw), da Diebold.

**Principais gaps**: Autenticidade e geração de ID imutável não são
garantidas no Mobile First.

**Situação alvo**: Soluções trabalhando em conjunto, fornecendo as
funcionalidades atuais, garantindo a autenticidade e a imutabilidade dos
IDs.

### 1 - Antivirus

O Antivirus é responsável por combater softwares maliciosos cujo
objetivo é atacar os ativos de TI visando obter acessos indevidos aos
computadores ligados em rede. O antivírus atua tanto com bases de
"assinaturas" de vírus quanto por heurística, observando o comportamento
de arquivos encontrados no computador, ou em uso, procurando identificar
neles comportamentos abusivos.

**Situação atual**: Solução da McAffee

**Principais gaps**: Não há.

### 2 - Segurança de endpoints de clientes 

O acesso de clientes a aplicações web e mobile deve ser protegido contra
ataques de:

-   Clonagem

-   Roubo de informações

-   Acesso remoto

A solução trabalha nas seguintes frentes:

-   Identificação do dispositivo

> A solução consegue identificar de forma unívoca cada dispositivo que
> acessa as aplicações. A identificação garante que dois dispositivos
> não possuem o mesmo ID e que um mesmo dispositivo gera sempre o mesmo
> ID.

-   Proteção contra trojans

A solução é especializada no combate a programas maliciosos do tipo
"trojan" cujo principal objetivo é fraude financeira. Diferentemente dos
vírus, os trojans buscam acesso aos computadores com o único objetivo de
roubar dados dos usuários e assim realizar transações financeiras
fraudulentas. Alguns trojans são identificados pelas soluções de
antivírus, entretanto a maior parte só é devidamente tratada pelas
soluções especializadas nesse tipo de ataque.

-   Geração de eventos de segurança

A solução é capaz de gerar eventos de segurança que ajudem no combate a
fraudes.

**Situação atual**: Solução OFD da Topaz

**Principais gaps**: Necessidade de instalação de agente.

**Situação alvo**: Solução que garanta a segurança dos aplicativos,
tanto web quanto mobile, sem necessariamente possuir agente instalado.

 