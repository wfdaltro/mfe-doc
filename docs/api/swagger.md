# Swagger API

Modelo de Arquivo Swagger em conformidade com os padrões API RESTfull CAIXA .

<a href="http://arquiteturati.caixa/arquitetura_referencia/api/swagger.json" download="swagger.json">Swagger</a>
## Mais Informações

[Portal Arquitetura de API](https://caixa.sharepoint.com/sites/5141/SitePages/GovernancaAPI.aspx).


## Histórico da Revisão

Data       | Versão | Descrição                                       | Autor
-----------|--------|-------------------------------------------------|-----------------
29/09/2021 | 1.0    | Inclusão do arquétipo de Open API | Elvis Elias


