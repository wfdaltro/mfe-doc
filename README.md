# Arquiteturas de Referência

Esse projeto documenta as Arquiteturas de Referência (ARF) utilizando a
linguagem de marcação Markdown e o gerador de sites estáticos
[MkDocs](https://www.mkdocs.org/) com o tema
[Material for MkDocs](https://squidfunk.github.io/mkdocs-material/).


## Preparando o ambiente

Para editar uma arquitetura de referência é necessário ter algumas
ferramentas instaladas na estação de trabalho e realizar algumas
configurações iniciais. Elas só precisam ser realizada 1 vez.

Abra chamado de instalação no https://servicos.caixa/ para as
ferramentas abaixo:

*  Python
*  Git
*  Visual Studio Code

Certifique-se que o chamado está sendo aberto para o "Visual Studio
**Code**" e não para o "Visual Studio", que é outro produto apesar de
ter o nome tão parecido.

Após ter todas as ferramentas instaladas, é preciso "clonar" o
repositório, que na verdade é o termo usado pelo Git para copiar todo o
conteúdo do repositório remoto
(http://fontes.caixa/suart/arquiteturas-de-referencia/arquiteturas-de-referencia)
para a sua estação de trabalho.

> **Importante!**
>
> Não clone um repositório para dentro do OneDrive. Isso pode causar
> conflitos e resultar em perda de trabalho.
>
> Os passos a seguir descrevem como fazer para evitar o problema.

Os comandos a seguir devem ser executados no Powershell para clonar o
repositório na sua estação de trabalho:

```powershell
New-Item -Type Directory -Path $home\dev
cd $home\dev
git clone http://fontes.des.caixa/suart/arquiteturas-de-referencia/arquiteturas-de-referencia.git
cd arquiteturas-de-referencia
```

Uma vez clonado o repositório do projeto, instale as dependências.
Considerando que o Python foi instalado corretamente, siga os passos
abaixo conforme o sistema operacional.

No Powershell digite os comandos abaixo:

```powershell
python -m venv venv
.\venv\Scripts\Activate.ps1
python -m pip install --upgrade -r .\requirements.txt
```

> **Atenção!**
>
> Se você teve erros durante a instalação das dependências, execute os
> comandos a seguir no Powershell para corrigir:
>
> ```powershell
> New-Item -ItemType Directory $env:APPDATA\pip
> New-Item -ItemType File $env:APPDATA\pip\pip.ini
> Get-Content .\pip.ini | Out-File -FilePath $env:APPDATA\pip\pip.ini
> ```
>
> Essa configuração adiciona o binário.caixa (Nexus) como repositório de
> onde devem ser baixadas as dependências do projeto.
>
> Após realizar essa configuração, execute o comando `python -m pip install --upgrade -r .\requirements.txt` novamente.

O processo de instalação das dependências pode demorar um pouco e, uma
vez terminado, podemos começar a editar as arquiteturas de referência no
Visual Studio Code.


## Editando uma arquitetura de referência

Para editar as arquiteturas de referência, recomendamos usar o Visual
Studio Code. Todas as instruções abaixo consideram que você está usando
esse editor.

Abra o Visual Studio Code clicando 2x no ícone dele. No
menu Arquivo (File), use a opção "Abrir Pasta..." (Open Folder...),
navegue até o diretório onde clonou o repositório e clique no botão
"Selecionar pasta" no canto inferior direito.

> **Dica**
>
> Se tiver usado as instruções acima para clonar o repositório, ele está
> no diretório `%userprofile%\dev\arquiteturas-de-referencia`. Esse
> caminho pode ser colado na barra do explorador de arquivos que se abre
> quando escolhemos a opção "Abrir Pasta..." no Visual Studio Code.

> **Importante!**
>
> Crie uma "branch" para editar a arquitetura de referência. Isso quer
> dizer que suas alterações estarão isoladas das demais, permitindo que
> várias pessoas trabalhem no mesmo repositório sem conflitos.
>
> Para criar uma branch, use o menu Ver, opção "Paleta de Comandos..."
> e digite "git: branch" sem aspas. A primeira opção é o comando
> desejado. Selecione, digite um nome para sua branch e pressione Enter.
>
> **O nome da sua branch deve aparecer no canto inferior esquerdo da
> janela do Visual Studio Code.**

No explorador de arquivos do Visual Studio Code, clique no arquivo da
arquitetura de referência que deseja editar ou crie um novo. Os arquivos
estão no subdiretório `/docs` e têm o nome da arquitetura de referência
escrito em minúsculas, sem acentos nem caracteres especiais e com
espaços substituídos por hífen ou underscore. Todos têm a extensão `.md`
que indica ser um arquivo Markdown.

As imagens devem ser colocadas em `./docs/img/[nome da arf]` para manter
o repositório organizado.

Para pré-visualizar o resultado final, abra o terminal integrado (menu
Ver, opção Terminal) e digite o comando

      mkdocs serve

Se ocorrer um erro, verifique que o ambiente virtual está ativo. Para
ativá-lo use o comando

      .\venv\Scripts\activate

e execute o comando `mkdocs serve` novamente. Esse comando vai criar uma
prévia do site das arquiteturas de referência que pode ser acessado no
navegador pelo link http://127.0.0.1:8000.

Toda vez que alterar um arquivo e gravar as alterações, elas serão
atualizadas automaticamente no navegador.

Uma vez que estiver satisfeito com as alterações. Vá no menu Ver, opção
SCM. Adicione os arquivos desejados passando o mouse sobre o nome deles
e clicando no "+" que aparece ao lado. Após adicionar os arquivos
alterados, digite uma mensagem curta descrevendo a alteração na caixa de
texto acima e clique no "check" mais acima para fazer um commit.

Para enviar suas alterações para o servidor, abra a Paleta de Comando e
digite "git sync" e escolha a primeira opção que aparece. Nesse
momento pode ser que se abra uma janela solicitando usuário e senha do
http://fontes.caixa.

Informa a SUART10 para publicação da arquitetura no site. Em caso de
dúvidas, procurar a SUART10.
